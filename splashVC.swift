//
//  splashVC.swift
//  HealthyPlus
//
//  Created by apple on 7/12/18.
//  Copyright © 2018 Kirollos. All rights reserved.
//

import UIKit
import Gifu
import Alamofire

class splashVC: UIViewController {
    
    var link = " "
    
    @IBOutlet weak var gifView: GIFImageView!
    @IBOutlet weak var ad_image: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        get_ad()
        gifView.animate(withGIFNamed: "splash", loopCount: 5)
        perform(#selector(launchingApp), with: nil, afterDelay: 4)
    }
    @objc func launchingApp(){
        if sharedHandler.isLogged(){
            if sharedHandler.getUserType() == 1{
                pushHome()
            }else{
                selectPatientType()
            }
        }else{
            selectUserType()
        }
    }
    @IBAction func openAdAction(_ sender: Any) {
        guard let url = URL(string: link) else {
            return //be safe
        }
        if #available(iOS 10.0, *) {
            UIApplication.shared.open(url, options: [:], completionHandler: nil)
        } else {
            UIApplication.shared.openURL(url)
        }
    }
    
    func get_ad(){
        let param = [
            "method": "get_ad"
        ]
        Alamofire.request(urls.healthyPlusURL, method: .post, parameters: param, encoding: JSONEncoding(options: []), headers: nil).responseJSON { (response) in
            switch response.result {
                
            case .success:
                if let value = response.result.value as? NSDictionary {
                    if value["state"] as? Int == 101{
                        if let arr = value["result"] as? NSArray{
                            if let object = arr[0] as? Dictionary <String, Any>{
                                if let image = object["value"] as? String{
                                    self.setImage(url: urls.ad_link_Image + image, Image: self.ad_image)
                                }
                            }
                            if let object = arr[2] as? Dictionary <String, Any>{
                                if let link = object["value"] as? String{
                                    self.link = link
                                }
                            }
                        }
                    }
                }
            case .failure:
                break
            }
        }
    }
}
