//
//  apiManager.swift
//  HealthyPlus
//
//  Created by apple on 10/16/18.
//  Copyright © 2018 Kirollos. All rights reserved.
//

import Foundation
import Alamofire

class APIManager {
    
    //MARK: - Singlton
    class var sharedInstance : APIManager {
        struct Singlton {
            static let instance = APIManager()
        }
        
        return Singlton.instance
    }
    
    
    // MARK: - general request
    func getRequest(_ url : String ,completionHandler :@escaping (DataResponse<Any>) -> Void) {
        _ = Alamofire.request(url,method: .get, parameters: nil, encoding: JSONEncoding.default, headers: nil).responseJSON { response in
            completionHandler(response)
        }
    }
    
    func postRequest(_ url : String ,Parameters : [String: Any],completionHandler :@escaping (DataResponse<Any>) -> Void) {
        _ = Alamofire.request(url,method: .post, parameters: Parameters, encoding: JSONEncoding.default, headers: nil).responseJSON { response in
            completionHandler(response)
        }
    }
}
