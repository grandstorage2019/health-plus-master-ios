/* 
Copyright (c) 2018 Swift Models Generated from JSON powered by http://www.json4swift.com

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

For support, please feel free to contact me at https://www.linkedin.com/in/syedabsar

*/

import Foundation
struct Clinic_address : Codable {
	let id : Int?
	let doctor_id : String?
	let clinic_name_en : String?
	let clinic_name_ar : String?
	let clinic_number : String?
	let area_id : Int?
	let lat : String?
	let lang : String?
	let building_number_ar : String?
	let building_number_en : String?
	let street_name_ar : String?
	let street_name_en : String?
	let floor_ar : String?
	let floor_en : String?
	let apartment_en : String?
	let apartment_ar : String?
	let landmark_en : String?
	let landmark_ar : String?
	let block_en : String?
	let block_ar : String?
	let flat_number : String?
	let governorate_name : String?
	let governorate_id : Int?
	let country_id : Int?
	let countries_name : String?
	let area_name : String?

	enum CodingKeys: String, CodingKey {

		case id = "id"
		case doctor_id = "doctor_id"
		case clinic_name_en = "clinic_name_en"
		case clinic_name_ar = "clinic_name_ar"
		case clinic_number = "clinic_number"
		case area_id = "area_id"
		case lat = "lat"
		case lang = "lang"
		case building_number_ar = "building_number_ar"
		case building_number_en = "building_number_en"
		case street_name_ar = "street_name_ar"
		case street_name_en = "street_name_en"
		case floor_ar = "floor_ar"
		case floor_en = "floor_en"
		case apartment_en = "apartment_en"
		case apartment_ar = "apartment_ar"
		case landmark_en = "landmark_en"
		case landmark_ar = "landmark_ar"
		case block_en = "block_en"
		case block_ar = "block_ar"
		case flat_number = "flat_number"
		case governorate_name = "governorate_name"
		case governorate_id = "governorate_id"
		case country_id = "country_id"
		case countries_name = "countries_name"
		case area_name = "area_name"
	}

	init(from decoder: Decoder) throws {
		let values = try decoder.container(keyedBy: CodingKeys.self)
		id = try values.decodeIfPresent(Int.self, forKey: .id)
		doctor_id = try values.decodeIfPresent(String.self, forKey: .doctor_id)
		clinic_name_en = try values.decodeIfPresent(String.self, forKey: .clinic_name_en)
		clinic_name_ar = try values.decodeIfPresent(String.self, forKey: .clinic_name_ar)
		clinic_number = try values.decodeIfPresent(String.self, forKey: .clinic_number)
		area_id = try values.decodeIfPresent(Int.self, forKey: .area_id)
		lat = try values.decodeIfPresent(String.self, forKey: .lat)
		lang = try values.decodeIfPresent(String.self, forKey: .lang)
		building_number_ar = try values.decodeIfPresent(String.self, forKey: .building_number_ar)
		building_number_en = try values.decodeIfPresent(String.self, forKey: .building_number_en)
		street_name_ar = try values.decodeIfPresent(String.self, forKey: .street_name_ar)
		street_name_en = try values.decodeIfPresent(String.self, forKey: .street_name_en)
		floor_ar = try values.decodeIfPresent(String.self, forKey: .floor_ar)
		floor_en = try values.decodeIfPresent(String.self, forKey: .floor_en)
		apartment_en = try values.decodeIfPresent(String.self, forKey: .apartment_en)
		apartment_ar = try values.decodeIfPresent(String.self, forKey: .apartment_ar)
		landmark_en = try values.decodeIfPresent(String.self, forKey: .landmark_en)
		landmark_ar = try values.decodeIfPresent(String.self, forKey: .landmark_ar)
		block_en = try values.decodeIfPresent(String.self, forKey: .block_en)
		block_ar = try values.decodeIfPresent(String.self, forKey: .block_ar)
		flat_number = try values.decodeIfPresent(String.self, forKey: .flat_number)
		governorate_name = try values.decodeIfPresent(String.self, forKey: .governorate_name)
		governorate_id = try values.decodeIfPresent(Int.self, forKey: .governorate_id)
		country_id = try values.decodeIfPresent(Int.self, forKey: .country_id)
		countries_name = try values.decodeIfPresent(String.self, forKey: .countries_name)
		area_name = try values.decodeIfPresent(String.self, forKey: .area_name)
	}

}