//
//  healthyFavourites.swift
//  HealthyPlus
//
//  Created by apple on 7/17/18.
//  Copyright © 2018 Kirollos. All rights reserved.
//

import Foundation

class healthyFavourite{
    
    var id = 0
    var doctor_id = 0
    var profile_image = " "
    var is_favourite = false
    var rate = 0
    var views = 0
    var raters = 0
    var fullName = " "
    var clinic_name = " "
    var about = " "
    var price = 0
    var waitingTime = " "
    var Location = " "
    var available_from = " "
    
    var is_life_time = " "
    var spec_id = " "
    var area_name = " "
    var flat_number = " "
    var clinic_number = " "
    var type = " "
    var specializations = " "
    var main_specialization_name = " "
    var level = " "
    init(id: Int, doctor_id: Int, profile_image: String, is_favourite: Bool, rate: Int, views: Int, raters: Int, fullName: String, clinic_name: String, about: String, price: Int, waitingTime: String, Location: String, available_from: String, is_life_time: String, spec_id: String, area_name: String, flat_number: String, clinic_number: String, type: String, specializations: String, main_specialization_name: String, level: String) {
        self.id = id
        self.doctor_id = doctor_id
        self.profile_image = profile_image
        self.is_favourite = is_favourite
        self.rate = rate
        self.views = views
        self.raters = raters
        self.fullName = fullName
        self.clinic_name = clinic_name
        self.about = about
        self.price = price
        self.waitingTime = waitingTime
        self.Location = Location
        self.available_from = available_from
        self.is_life_time = is_life_time
        self.spec_id = spec_id
        self.area_name = area_name
        self.flat_number = flat_number
        self.clinic_number = clinic_number
        self.type = type
        self.specializations = specializations
        self.main_specialization_name = main_specialization_name
        self.level = level 
    }
}
