//
//  advertiseModel.swift
//  HealthyPlus
//
//  Created by apple on 7/15/18.
//  Copyright © 2018 Kirollos. All rights reserved.
//

import Foundation

class advertiseModel {
    var image = " "
    var name = " "
    var link = " "
    var is_promited = false
    
    init(image: String, name: String, link: String, is_promited: Bool) {
        self.image = image
        self.name = name
        self.link = link
        self.is_promited = is_promited
    }
}
