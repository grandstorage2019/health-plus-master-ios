//
//  reservations.swift
//  HealthyPlus
//
//  Created by apple on 7/15/18.
//  Copyright © 2018 Kirollos. All rights reserved.
//

import Foundation

class reservation{
    var reservation_id = 0
    var dateOrServiceName = " "
    var Title = " "
    var subTitle = " "
    var logo = " "
    var mobile = " "
    var address = " "
    var lat = " "
    var lang = " "
    
    
    
    init(resID: Int, dateOrServiceName: String, Title: String, subTitle: String, logo: String, mobile: String, address: String, lat: String, lang: String) {
        self.reservation_id = resID
        self.dateOrServiceName = dateOrServiceName
        self.Title = Title
        self.subTitle = subTitle
        self.logo = logo
        self.mobile = mobile
        self.address = address
        self.lat = lat
        self.lang = lang
    }
}
