//
//  appointment.swift
//  HealthyPlus
//
//  Created by apple on 8/12/18.
//  Copyright © 2018 Kirollos. All rights reserved.
//

import Foundation

//"day": "2018-07-31",
//"day_col": "Tue",
//"date": "2018-07-31",
//"have_reservations": true,
//"num_of_reservations": 2

class appointment{
    var day = " "
    var dayName = " "
    var date = " "
    var haveReservation = false
    var numOfReservation = 0
    
    init(Day: String, dayName: String, date: String, haveReservation: Bool, numOfReservation: Int) {
        self.day = Day
        self.dayName = dayName
        self.date = date
        self.haveReservation = haveReservation
        self.numOfReservation = numOfReservation
    }
}
