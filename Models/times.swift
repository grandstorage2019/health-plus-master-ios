//
//  times.swift
//  HealthyPlus
//
//  Created by apple on 7/18/18.
//  Copyright © 2018 Kirollos. All rights reserved.
//

import Foundation

class Times{
    
    var day = " "
    var from_time = " "
    var to_time = " "
    var waiting_time = " "
    var date = " "
    var time_type = " "
    
    init(day: String, from_time: String, to_time: String, waiting_time: String, date: String, time_type: String) {
        self.day = day
        self.from_time = from_time
        self.to_time = to_time
        self.waiting_time = waiting_time
        self.date = date
        self.time_type = time_type
    }
}
