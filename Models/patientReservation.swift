//
//  patientReservation.swift
//  HealthyPlus
//
//  Created by apple on 8/13/18.
//  Copyright © 2018 Kirollos. All rights reserved.
//

import Foundation

class patientReservation{
    
    var time = " "
    var name = " "
    var gender = " "
    var id = " "
    var phone = " "
    
    init(time: String, name: String, gender: String, id: String, phone: String) {
        self.time = time
        self.name = name
        self.gender = gender
        self.id = id
        self.phone = phone
    }
}
