//
//  PatientModel.swift
//  HealthyPlus
//
//  Created by apple on 8/8/18.
//  Copyright © 2018 Kirollos. All rights reserved.
//

import Foundation

class Patient{
    var id = 0
    var added_by_doctor_id = 0
    var country_id = 0
    var gender = " "
    var phone = " "
    var name = " "
    var email = " "
    var image = " "
    var notes = " "
    var insurance_id = " "
    var insurance_company = " "
    
    init(id: Int, doctorID: Int, countryId: Int, gender: String, phone: String, name: String, email: String, picture: String, notes: String, insuranceId: String, insuranceCompany: String) {
        self.id = id
        self.added_by_doctor_id = doctorID
        self.country_id = countryId
        self.gender = gender
        self.phone = phone
        self.name = name
        self.email = email
        self.image = picture
        self.notes = notes
        self.insurance_id = insuranceId
        self.insurance_company = insuranceCompany
    }
}
