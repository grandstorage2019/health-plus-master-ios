//
//  servicesChecked.swift
//  HealthyPlus
//
//  Created by Donna on 8/26/18.
//  Copyright © 2018 Kirollos. All rights reserved.
//

import Foundation
//servicesChecked(id: id, name: name, check: check)

class servicesChecked{
    
    var id = 0
    var name = " "
    var check = false
    
    init(id: Int, name: String, check: Bool) {
        self.id = id
        self.name = name
        self.check = check
    }
    
}
