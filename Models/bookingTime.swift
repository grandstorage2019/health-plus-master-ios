//
//  bookingTime.swift
//  HealthyPlus
//
//  Created by apple on 7/23/18.
//  Copyright © 2018 Kirollos. All rights reserved.
//

import Foundation

class bookingTime{
    var time = " "
    var isBooked = false
    
    init(time: String, isBooked: Bool) {
        self.time = time
        self.isBooked = isBooked
    }
}
