//
//  notifications.swift
//  HealthyPlus
//
//  Created by apple on 7/16/18.
//  Copyright © 2018 Kirollos. All rights reserved.
//

import Foundation

class notification{
    var message = " "
    var image = " "
    var created_at = " "
    
    init(msg: String, image: String, created: String) {
        self.message = msg
        self.image = image
        self.created_at = created
    }
}
