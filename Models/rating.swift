//
//  rating.swift
//  HealthyPlus
//
//  Created by Donna on 7/20/18.
//  Copyright © 2018 Kirollos. All rights reserved.
//

import Foundation

class rating{
    var Name = " "
    var Comment = " "
    var Stars = 0
    
    init(name: String, com: String, star: Int) {
        self.Name = name
        self.Comment = com
        self.Stars = star
    }
}
