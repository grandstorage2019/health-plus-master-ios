//
//  servicesModel.swift
//  HealthyPlus
//
//  Created by Donna on 7/21/18.
//  Copyright © 2018 Kirollos. All rights reserved.
//

import Foundation

class servicesModel{
    var id = 0
    var name  = " "
    var logo = " "
    var subServices = Array<subServices>()
    
    init(id: Int, Name: String, Logo: String, SubServ: [subServices]) {
        self.id = id
        self.name = Name
        self.logo = Logo
        self.subServices = SubServ
    }
}
class subServices{
    
    var id = 0
    var image = " "
    var quantity = " "
    var price = 0
    var Name = " "
    
    init(id: Int, image: String, Quantity: String, Price: Int, name: String) {
        self.id = id
        self.image = image
        self.quantity = Quantity
        self.price = Price
        self.Name = name
    }
}

