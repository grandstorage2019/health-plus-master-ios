//
//  messages.swift
//  HealthyPlus
//
//  Created by apple on 8/16/18.
//  Copyright © 2018 Kirollos. All rights reserved.
//

import Foundation

class messages{
    var doctor_id = 0
    var patient_id = 0
    var message = " "
    var who = " "
    var name = " "
    var image = " "
    
    init(doctor_id: Int, patient_id: Int, message: String, who: String, name: String, image: String) {
        self.doctor_id   = doctor_id
        self.patient_id  = patient_id
        self.message     = message
        self.who         = who
        self.name        = name
        self.image       = image
    }
}
