//
//  careServices.swift
//  HealthyPlus
//
//  Created by apple on 8/14/18.
//  Copyright © 2018 Kirollos. All rights reserved.
//

import Foundation

class careServices{
    var id = 0
    var name = " "
    var logo = " "
    var sub_services = Array<subCareServices>()
    
    init(id: Int, name: String, logo: String, subServices: [subCareServices]) {
        self.id = id
        self.name = name
        self.logo = logo
        self.sub_services = subServices
    }
}
class subCareServices{
    var id       = 0
    var image    = " "
    var quantity = " "
    var price    = " "
    var name     = " "
    var unit     = " "
    
    init(id: Int, image: String, quantity: String, price: String, name: String, unit: String) {
        self.id       = id
        self.image    = image
        self.quantity = quantity
        self.price    = price
        self.name     = name
        self.unit     = unit
    }
}
