//
//  plans.swift
//  HealthyPlus
//
//  Created by apple on 8/16/18.
//  Copyright © 2018 Kirollos. All rights reserved.
//

import Foundation

class plans{
    var id = " "
    var name = " "
    var details = " "
    
    init(id: String, name: String, details:String) {
        self.id = id
        self.name = name
        self.details = details
    }
}
