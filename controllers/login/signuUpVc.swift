//
//  signuUpVc.swift
//  HealthyPlus
//
//  Created by apple on 8/1/18.
//  Copyright © 2018 Kirollos. All rights reserved.
//

import UIKit
import Alamofire
import Lottie

class signuUpVc: UIViewController {
    
    var countryID = 0
    var birthDate = " "
    var gender = " "
    
    var isUpdate   = false
    var isRemember = false
    
    var arrRegion = Array<services>()
    var arrGender = ["male".localized, "female".localized]
    var arrGenderSelected  = ["male", "female"]
    
    var birthPicker = UIDatePicker()
    var genderPicker = UIPickerView()
    var countryPicker = UIPickerView()
    
    var lott: LOTAnimationView = LOTAnimationView(name: "Boat_Loader")
    
    @IBOutlet weak var view1: UIView!
    @IBOutlet weak var view2: UIView!
    @IBOutlet weak var view3: UIView!
    @IBOutlet weak var view4: UIView!
    @IBOutlet weak var view5: UIView!
    @IBOutlet weak var view6: UIView!
    @IBOutlet weak var view7: UIView!
    @IBOutlet weak var view8: UIView!
    
    @IBOutlet weak var titleLbl: UILabel!
    @IBOutlet weak var haveAccLbl: UILabel!
    @IBOutlet weak var rememberLbl: UILabel!
    
    @IBOutlet weak var mobTF: UITextField!
    @IBOutlet weak var emailTF: UITextField!
    @IBOutlet weak var genderTF: UITextField!
    @IBOutlet weak var countryTF: UITextField!
    @IBOutlet weak var userNameTF: UITextField!
    @IBOutlet weak var passwrodTF: UITextField!
    @IBOutlet weak var birthDateTF: UITextField!
    @IBOutlet weak var confirmPasswordTF: UITextField!
    
    @IBOutlet weak var skipBtn: UIButton!
    @IBOutlet weak var signupOutlet: UIButton!
    @IBOutlet weak var signInOutlet: UIButton!
    @IBOutlet weak var rememberMeOutlet: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        isUpdatee()
        
        customView(custom: view1)
        customView(custom: view2)
        customView(custom: view3)
        customView(custom: view4)
        customView(custom: view5)
        customView(custom: view6)
        customView(custom: view7)
        customView(custom: view8)
        
        view1.layer.cornerRadius = 25
        view2.layer.cornerRadius = 25
        view3.layer.cornerRadius = 25
        view4.layer.cornerRadius = 25
        view5.layer.cornerRadius = 25
        view6.layer.cornerRadius = 25
        view7.layer.cornerRadius = 25
        view8.layer.cornerRadius = 25
        
        
        countryPicker.delegate   = self
        countryPicker.dataSource = self
        
        genderPicker.delegate    = self
        genderPicker.dataSource  = self
        
        genderTF.inputView = genderPicker
        countryTF.inputView = countryPicker
        birthDateTF.inputView = birthPicker
        
        showDatePicker()
        specializationData(url: urls.healthyPlusURL)
        
        setBtnGradient(btn: signupOutlet, color: colors.appColor, gradColor: colors.gradColor)
        signupOutlet.layer.cornerRadius = 25
    }
    
    func isUpdatee(){
        if isUpdate{
            skipBtn.alpha          = 1
            haveAccLbl.alpha       = 0
            rememberLbl.alpha      = 0
            signInOutlet.alpha     = 0
            rememberMeOutlet.alpha = 0
            
            titleLbl.text = "Edit Profile".localized
            signupOutlet.setTitle("Update".localized, for: .normal)
            
            
            mobTF.text       = sharedHandler.getPhone()
            emailTF.text     = sharedHandler.getEmail()
            genderTF.text    = sharedHandler.getGender()
            userNameTF.text  = sharedHandler.getUserName()
            birthDateTF.text = sharedHandler.getbirthDate()
            
            countryID        = sharedHandler.getCountryID()
        }
    }
    
    @IBAction func rememberMeAction(_ sender: UIButton) {
        if sender.tag == 0{
            sender.tag = 1
            isRemember = true
            rememberMeOutlet.setImage(#imageLiteral(resourceName: "circleSelected"), for: .normal)
        }else{
            sender.tag = 0
            isRemember = false
            rememberMeOutlet.setImage(#imageLiteral(resourceName: "circle"), for: .normal)
        }
    }
    
    @IBAction func signUpAction(_ sender: Any) {
        if sharedHandler.getUserType() == 2{
            if sharedHandler.isValidAction(textFeilds: [mobTF, emailTF, genderTF, countryTF, userNameTF, birthDateTF]){
                if passwrodTF.text == confirmPasswordTF.text{
                    if isUpdate{
                        Update()
                    }else{
                        register()
                    }
                }else{
                    showAttentionMessage(msg: "Password didn't Match")
                }
            }else{
                showAttentionMessage(msg: "Please, Enter All Fields")
            }
            
        }else{
        }
    }
    
    @IBAction func signInAction(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    func register(){
        let param = [
            "method": "register",
            "username": "\(userNameTF.text!)",
            "email": "\(emailTF.text!)",
            "password": "\(passwrodTF.text!)",
            "phone": "\(mobTF.text!)",
            "gender": "\(gender)",
            "birth_day": "\(birthDate)",
            "google_id": "\(sharedHandler.googleID())",
            "device_id": "\(sharedHandler.getDeviceID())",
            "country_id": "\(countryID)"
        ]
        cycleLogin(url: urls.healthyPlusURL, param: param, isRemember: isRemember, isUpdate: false, isNewDoc: false)
    }
    
    func Update(){
        let param = [
            "method": "update",
            "id": "\(sharedHandler.getUserID())",
            "username": "\(userNameTF.text!)",
            "email": "\(emailTF.text!)",
            "password": "\(passwrodTF.text!)",
            "phone": "\(mobTF.text!)",
            "gender": "\(gender)",
            "birth_day": "\(birthDate)",
            "google_id": "\(sharedHandler.googleID())",
            "device_id": "\(sharedHandler.getDeviceID())",
            "country_id": "\(countryID)"
        ]
        cycleLogin(url: urls.healthyPlusURL, param: param, isRemember: isRemember, isUpdate: true, isNewDoc: false)
    }
    
    func specializationData(url: String){
        lott = playAnimation(vc: self)

        let param = [
            "method": "countries",
            "language": "\(sharedHandler.getLanguage())",
        ]
        Alamofire.request(url, method: .post, parameters: param, encoding: JSONEncoding(options: []), headers: nil).responseJSON { (response) in
            switch response.result {
                
            case .success:
                if let value = response.result.value as? NSDictionary {
                    if value["state"] as? Int == 101{
                        if let arr = value["countries"] as? NSArray{
                            for data in arr{
                                
                                var id = 0
                                var name = " "
                                
                                if let object = data as? Dictionary <String, Any>{
                                    if let value = object["id"] as? Int{
                                        id = value
                                    }
                                    if let value = object["name"] as? String{
                                        name = value
                                    }
                                }
                                self.arrRegion.append(services(id: id, name: name))
                            }
                            self.countryPicker.reloadAllComponents()
                        }
                    }
                    else{
                        if let status = value["state"] as? Int{
                            self.otherStatus(Number: status)
                        }
                    }
                    self.stopAnimation(Lot: self.lott)
                }
            case .failure:
                self.errorInConnection()
                self.stopAnimation(Lot: self.lott)
            }
        }
    }
    
}
extension signuUpVc: UIPickerViewDelegate, UIPickerViewDataSource{
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        if pickerView == genderPicker{
            return arrGender.count
        }else{
            return arrRegion.count
        }
    }
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        if pickerView == genderPicker{
            return arrGender[row]
        }else{
            return arrRegion[row].name
        }
    }
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        if pickerView == genderPicker{
            genderTF.text  = arrGender[row]
            gender = arrGenderSelected[row]
        }else{
            if arrRegion.count > 0{
                countryID      = arrRegion[row].id
                countryTF.text = arrRegion[row].name
            }else{
                self.specializationData(url: urls.healthyPlusURL)
            }
        }
    }
    func showDatePicker(){
        birthPicker.datePickerMode = .date
        
        let toolbar = UIToolbar();
        toolbar.sizeToFit()
        
        //done button & cancel button
        let doneButton = UIBarButtonItem(title: "Done".localized, style: .done, target: self, action: #selector(donedatePicker))
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: "Cancel".localized, style: .done, target: self, action: #selector(cancelDatePicker))
        toolbar.setItems([doneButton,spaceButton,cancelButton], animated: false)
        
        birthDateTF.inputAccessoryView = toolbar
        
    }
    @objc func donedatePicker(){
        //For date formate
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd"
        birthDateTF.text = formatter.string(from: birthPicker.date)
        birthDate = birthDateTF.text!
        self.view.endEditing(true)
    }
    @objc func cancelDatePicker(){
        self.view.endEditing(true)
    }
}
