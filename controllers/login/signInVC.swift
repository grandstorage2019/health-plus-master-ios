//
//  signInVC.swift
//  HealthyPlus
//
//  Created by apple on 8/1/18.
//  Copyright © 2018 Kirollos. All rights reserved.
//

import UIKit

class signInVC: UIViewController {
    
    var isRemember = false
    
    @IBOutlet weak var mailView: UIView!
    @IBOutlet weak var passView: UIView!
    
    @IBOutlet weak var emailTF: UITextField!
    @IBOutlet weak var passwordTF: UITextField!
    
    @IBOutlet weak var rememberMeBtn: UIButton!
    @IBOutlet weak var signInBtnOutlet: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        customView(custom: mailView)
        customView(custom: passView)
        
        mailView.layer.cornerRadius = 25
        passView.layer.cornerRadius = 25
        
        
        setBtnGradient(btn: signInBtnOutlet, color: colors.appColor, gradColor: colors.gradColor)
        signInBtnOutlet.layer.cornerRadius = 25
    }
    @IBAction func isRemeberAction(_ sender: UIButton) {
        if sender.tag == 0{
            sender.tag = 1
            isRemember = true
            rememberMeBtn.setImage(#imageLiteral(resourceName: "circleSelected"), for: .normal)
        }else{
            sender.tag = 0
            isRemember = false
            rememberMeBtn.setImage(#imageLiteral(resourceName: "circle"), for: .normal)
        }
    }
    @IBAction func forgetPasswordAction(_ sender: Any) {
        let vc = UIStoryboard.init(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "forgetPasswordVC") as! forgetPasswordVC
        present(vc, animated: true, completion: nil)
    }
    @IBAction func signInAction(_ sender: Any) {
        if sharedHandler.isValidAction(textFeilds: [emailTF, passwordTF]){
            if sharedHandler.getUserType() == 1{
                login(url: urls.filterInsurance)
            }else{
                login(url: urls.healthyPlusURL)
            }
        }else{
            showAttentionMessage(msg: "Please Enter Email and Password")
        }
    }
    @IBAction func signUpAction(_ sender: Any) {
        if sharedHandler.getUserType() == 2{
            let vc = UIStoryboard.init(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "signuUpVc") as! signuUpVc
            present(vc, animated: true, completion: nil)
        }else{
            let vc = UIStoryboard.init(name: "doctor", bundle: nil).instantiateViewController(withIdentifier: "docSignUpVc") as! docSignUpVc
            present(vc, animated: true, completion: nil)
        }
    }
    @IBAction func skipAction(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    func login(url: String){
        let param = [
            "method": "login",
            "email": "\(emailTF.text!)",
            "password": "\(passwordTF.text!)",
            "google_id": "\(sharedHandler.googleID())",
            "device_id": "\(sharedHandler.getDeviceID())"
        ]
        cycleLogin(url: url, param: param, isRemember: isRemember, isUpdate: false, isNewDoc: false)
    }
}
