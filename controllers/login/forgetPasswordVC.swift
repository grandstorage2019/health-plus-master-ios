//
//  forgetPasswordVC.swift
//  HealthyPlus
//
//  Created by apple on 8/2/18.
//  Copyright © 2018 Kirollos. All rights reserved.

import UIKit
import Alamofire
import Lottie
class forgetPasswordVC: UIViewController {
    
    
    var lott: LOTAnimationView = LOTAnimationView(name: "Boat_Loader")
    
    @IBOutlet weak var mailView: UIView!
    @IBOutlet weak var mailTF: UITextField!
    @IBOutlet weak var resetbtn: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        customView(custom: mailView)
        setBtnGradient(btn: resetbtn, color: colors.appColor, gradColor: colors.gradColor)
        
        mailView.layer.cornerRadius = 25
        resetbtn.layer.cornerRadius = 25
    }
    func randomCode() -> NSString {
        
        let letters : NSString = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"
        let randomString : NSMutableString = NSMutableString(capacity: 4)
        
        for _ in 0...3{
            let length = UInt32 (letters.length)
            let rand = arc4random_uniform(length)
            randomString.appendFormat("%C", letters.character(at: Int(rand)))
        }
        
        return randomString
    }
    @IBAction func resetAction(_ sender: Any) {
        if sharedHandler.isValidAction(textFeilds: [mailTF]){
            requestPassword()
        }
    }
    @IBAction func signInAction(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    func requestPassword(){
        lott = playAnimation(vc: self)

        let code = randomCode() as String
        let param = [
            "method": "send_code",
            "email": "\(mailTF.text!)",
            "code": "\(code)"
        ]
        Alamofire.request(urls.healthyPlusURL, method: .post, parameters: param, encoding: JSONEncoding(options: []), headers: nil).responseJSON { (response) in
            switch response.result {
                
            case .success:
                if let value = response.result.value as? NSDictionary {
                    if value["state"] as? Int == 101{
                        let vc = UIStoryboard.init(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "resetPasswordVC") as! resetPasswordVC
                        vc.Code = code
                        vc.mail = self.mailTF.text!
                        self.present(vc, animated: true, completion: nil)
                    }
                    else{
                        if let status = value["state"] as? Int{
                            self.otherStatus(Number: status)
                        }
                    }
                    self.stopAnimation(Lot: self.lott)
                }
            case .failure:
                self.errorInConnection()
                self.stopAnimation(Lot: self.lott)
            }
        }
    }
}
