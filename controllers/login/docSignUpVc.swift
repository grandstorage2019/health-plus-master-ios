//  docSignUpVc.swift
//  HealthyPlus
//
//  Created by apple on 8/5/18.
//  Copyright © 2018 Kirollos. All rights reserved.
//

import UIKit
import Alamofire
import Lottie
class docSignUpVc: UIViewController {
    
    var specialID = 0
    var countryID = 0
    var birthDate = " "
    var gender = " "
    
    var isUpdate   = false
    var isRemember = false
    
    var arrRegion = Array<services>()
    var arrGender = ["male".localized, "female".localized]
    var arrGenderSelected  = ["male", "female"]
    
    var arrSpecialization = Array<services>()
    
    var birthPicker = UIDatePicker()
    var genderPicker = UIPickerView()
    var countryPicker = UIPickerView()
    var specializationPicker = UIPickerView()
    
    var lott: LOTAnimationView = LOTAnimationView(name: "Boat_Loader")
    
    @IBOutlet weak var view1: UIView!
    @IBOutlet weak var view2: UIView!
    @IBOutlet weak var view3: UIView!
    @IBOutlet weak var view4: UIView!
    @IBOutlet weak var view5: UIView!
    @IBOutlet weak var view6: UIView!
    @IBOutlet weak var view7: UIView!
    @IBOutlet weak var view8: UIView!
    @IBOutlet weak var view9: UIView!
    @IBOutlet weak var view10: UIView!
    
    @IBOutlet weak var haveLbl: UILabel!
    @IBOutlet weak var titleLbl: UILabel!
    @IBOutlet weak var remmberLbl: UILabel!
    
    @IBOutlet weak var fNameTf: UITextField!
    @IBOutlet weak var lNameTF: UITextField!
    @IBOutlet weak var emailTF: UITextField!
    @IBOutlet weak var mobileTF: UITextField!
    @IBOutlet weak var genderTF: UITextField!
    @IBOutlet weak var countryTF: UITextField!
    @IBOutlet weak var passwordTF: UITextField!
    @IBOutlet weak var birthDateTF: UITextField!
    @IBOutlet weak var confPasswordTF: UITextField!
    @IBOutlet weak var specializationTF: UITextField!
    
    @IBOutlet weak var skipBtn: UIButton!
    @IBOutlet weak var signInBtn: UIButton!
    @IBOutlet weak var rememberBtn: UIButton!
    @IBOutlet weak var signUpOutlet: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        isUpdatee()
        
        customView(custom: view1)
        customView(custom: view2)
        customView(custom: view3)
        customView(custom: view4)
        customView(custom: view5)
        customView(custom: view6)
        customView(custom: view7)
        customView(custom: view8)
        customView(custom: view9)
        customView(custom: view10)
        
        view1.layer.cornerRadius = 25
        view2.layer.cornerRadius = 25
        view3.layer.cornerRadius = 25
        view4.layer.cornerRadius = 25
        view5.layer.cornerRadius = 25
        view6.layer.cornerRadius = 25
        view7.layer.cornerRadius = 25
        view8.layer.cornerRadius = 25
        view9.layer.cornerRadius = 25
        view10.layer.cornerRadius = 25
        
        countryPicker.delegate   = self
        countryPicker.dataSource = self
        
        genderPicker.delegate    = self
        genderPicker.dataSource  = self
        
        specializationPicker.delegate    = self
        specializationPicker.dataSource  = self
        
        genderTF.inputView = genderPicker
        countryTF.inputView = countryPicker
        specializationTF.inputView = specializationPicker
        birthDateTF.inputView = birthPicker
        mobileTF.keyboardType = .decimalPad
        
        showDatePicker()
        specializationData(url: urls.filterInsurance)
        countriesData(url: urls.healthyPlusURL)
        
        setBtnGradient(btn: signUpOutlet, color: colors.appColor, gradColor: colors.gradColor)
        signUpOutlet.layer.cornerRadius = 25
    }
    func isUpdatee(){
        if isUpdate{
            skipBtn.alpha              = 1
            haveLbl.alpha              = 0
            signInBtn.alpha            = 0
            remmberLbl.alpha           = 0
            rememberBtn.alpha          = 0
            countryTF.isEnabled        = false
            specializationTF.isEnabled = false
            mobileTF.isEnabled         = false
            genderTF.isEnabled         = false
            fNameTf.isEnabled          = false
            lNameTF.isEnabled          = false
            birthDateTF.isEnabled      = false
            
            titleLbl.text = "Edit Profile".localized
            signUpOutlet.setTitle("Update".localized, for: .normal)
            
            
            mobileTF.text       = sharedHandler.getPhone()
            emailTF.text        = sharedHandler.getEmail()
            genderTF.text       = sharedHandler.getGender()
            fNameTf.text        = sharedHandler.getUserName()
            lNameTF.text        = sharedHandler.getEnLName()
            birthDateTF.text    = sharedHandler.getbirthDate()
            countryID           = sharedHandler.getCountryID()
            
            mobileTF.textColor    = .gray
            genderTF.textColor    = .gray
            fNameTf.textColor     = .gray
            lNameTF.textColor     = .gray
            birthDateTF.textColor = .gray
            countryTF.textColor   = .gray
            countryTF.textColor        = .gray
            specializationTF.textColor = .gray
        }
    }
    func countriesData(url: String){
        lott = playAnimation(vc: self)

        let param = [
            "method": "countries",
            "language": "\(sharedHandler.getLanguage())",
        ]
        Alamofire.request(url, method: .post, parameters: param, encoding: JSONEncoding(options: []), headers: nil).responseJSON { (response) in
            switch response.result {
                
            case .success:
                if let value = response.result.value as? NSDictionary {
                    if value["state"] as? Int == 101{
                        if let arr = value["countries"] as? NSArray{
                            for data in arr{
                                
                                var id = 0
                                var name = " "
                                
                                if let object = data as? Dictionary <String, Any>{
                                    if let value = object["id"] as? Int{
                                        id = value
                                        
                                    }
                                    if let value = object["name"] as? String{
                                        name = value
                                    }
                                }
                                self.arrRegion.append(services(id: id, name: name))
                            }
                            self.countryPicker.reloadAllComponents()
                        }
                    }
                    else{
                        if let status = value["state"] as? Int{
                            self.otherStatus(Number: status)
                        }
                    }
                    self.stopAnimation(Lot: self.lott)
                }
            case .failure:
                self.errorInConnection()
                self.stopAnimation(Lot: self.lott)
            }
        }
    }
    func specializationData(url: String){

        let param = [
            "method": "specializations",
            "language": "\(sharedHandler.getLanguage())",
        ]
        Alamofire.request(url, method: .post, parameters: param, encoding: JSONEncoding(options: []), headers: nil).responseJSON { (response) in
            switch response.result {
                
            case .success:
                if let value = response.result.value as? NSDictionary {
                    if value["state"] as? Int == 101{
                        if let arr = value["specializations"] as? NSArray{
                            for data in arr{
                                
                                var id = 0
                                var name = " "
                                
                                if let object = data as? Dictionary <String, Any>{
                                    if let value = object["id"] as? Int{
                                        id = value
                                    }
                                    if let value = object["name"] as? String{
                                        name = value
                                        print(sharedHandler.specialization())
                                        if sharedHandler.specialization() == id{
                                            self.specializationTF.placeholder = value
                                        }
                                    }
                                }
                                self.arrSpecialization.append(services(id: id, name: name))
                            }
                            self.specializationPicker.reloadAllComponents()
                        }
                        
                    }
                    else{
                        if let status = value["state"] as? Int{
                            self.otherStatus(Number: status)
                        }
                    }
                    self.stopAnimation(Lot: self.lott)
                }
            case .failure:
                self.errorInConnection()
                self.stopAnimation(Lot: self.lott)
            }
        }
    }
    func Update(){
        let param = [
            "method"   : "update",
            "id"       : "\(sharedHandler.getUserID())",
            "email"    : "\(emailTF.text!)",
            "password" : "\(passwordTF.text!)",
            "birth_day": "\(birthDateTF.text!)",
            "google_id": "\(sharedHandler.googleID())",
            "device_id": "\(sharedHandler.getDeviceID())"
        ]
        print(param)
        cycleLogin(url: urls.filterInsurance, param: param, isRemember: isRemember, isUpdate: true, isNewDoc: false)
    }
    func register(){
        let param = [
            "method": "register",
            "fname_en": "\(fNameTf.text!)",
            "lname_en": "\(lNameTF.text!)",
            "email": "\(emailTF.text!)",
            "password": "\(passwordTF.text!)",
            "phone": "\(mobileTF.text!)",
            "gender": "\(gender)",
            "birth_day": "\(birthDateTF.text!)",
            "google_id": "\(sharedHandler.googleID())",
            "device_id": "\(sharedHandler.getDeviceID())",
            "country_id": "\(countryID)",
            "specialization_id": "\(specialID)"
        ]
        print(param)
        cycleLogin(url: urls.filterInsurance, param: param, isRemember: isRemember, isUpdate: false, isNewDoc: true)
    }
    @IBAction func rememberMeAction(_ sender: UIButton) {
        if sender.tag == 0{
            sender.tag = 1
            isRemember = true
            rememberBtn.setImage(#imageLiteral(resourceName: "circleSelected"), for: .normal)
        }else{
            sender.tag = 0
            isRemember = false
            rememberBtn.setImage(#imageLiteral(resourceName: "circle"), for: .normal)
        }
    }
    @IBAction func signUpAction(_ sender: Any) {
        if sharedHandler.getUserType() == 1{
            if sharedHandler.isValidAction(textFeilds: [emailTF, passwordTF]){
                if passwordTF.text == confPasswordTF.text{
                    if isUpdate{
                        Update()
                    }else{
                        if sharedHandler.isValidAction(textFeilds: [countryTF, specializationTF, mobileTF, genderTF, fNameTf, lNameTF, birthDateTF]){
                            register()
                        }else{
                            showAttentionMessage(msg: "Please, Enter All Fields")
                        }
                    }
                }else{
                    showAttentionMessage(msg: "Password didn't Match")
                }
            }else{
                showAttentionMessage(msg: "Please, Enter All Fields")
            }
            
        }else{
        }
    }
    @IBAction func signInAction(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
}
extension docSignUpVc: UIPickerViewDelegate, UIPickerViewDataSource{
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        if pickerView == genderPicker{
            return arrGender.count
        }else if pickerView == countryPicker{
            return arrRegion.count
        }else{
            return arrSpecialization.count
        }
    }
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        if pickerView == genderPicker{
            return arrGender[row]
        }else if pickerView == countryPicker{
            return arrRegion[row].name
        }else{
            return arrSpecialization[row].name
        }
    }
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        if pickerView == genderPicker{
            genderTF.text  = arrGender[row]
            gender = arrGenderSelected[row]
        }else if pickerView == countryPicker{
            if arrRegion.count > 0{
                countryID      = arrRegion[row].id
                countryTF.text = arrRegion[row].name
            }else{
                countriesData(url: urls.healthyPlusURL)
            }
        }else{
            if arrSpecialization.count > 0{
                specialID = arrSpecialization[row].id
                specializationTF.text = arrSpecialization[row].name
            }
        }
    }
    func showDatePicker(){
        birthPicker.datePickerMode = .date
        
        let toolbar = UIToolbar();
        toolbar.sizeToFit()
        
        //done button & cancel button
        let doneButton = UIBarButtonItem(title: "Done".localized, style: .done, target: self, action: #selector(donedatePicker))
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: "Cancel".localized, style: .done, target: self, action: #selector(cancelDatePicker))
        toolbar.setItems([doneButton,spaceButton,cancelButton], animated: false)
        
        birthDateTF.inputAccessoryView = toolbar
        
    }
    @objc func donedatePicker(){
        //For date formate
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd"
        birthDateTF.text = formatter.string(from: birthPicker.date)
        birthDate = birthDateTF.text!
        self.view.endEditing(true)
    }
    @objc func cancelDatePicker(){
        self.view.endEditing(true)
    }
}
