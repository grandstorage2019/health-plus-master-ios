//
//  resetPasswordVC.swift
//  HealthyPlus
//
//  Created by apple on 8/2/18.
//  Copyright © 2018 Kirollos. All rights reserved.
//

import UIKit
import Alamofire
import Lottie
class resetPasswordVC: UIViewController {
    
    var Code = " "
    var mail = " "
    
    var lott: LOTAnimationView = LOTAnimationView(name: "Boat_Loader")
    
    @IBOutlet weak var view1: UIView!
    @IBOutlet weak var view2: UIView!
    @IBOutlet weak var view3: UIView!
    
    @IBOutlet weak var codeTF: UITextField!
    @IBOutlet weak var passwordTF: UITextField!
    @IBOutlet weak var confPasswordTF: UITextField!
    
    @IBOutlet weak var resetPassword: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        customView(custom: view1)
        customView(custom: view2)
        customView(custom: view3)
        
        view1.layer.cornerRadius = 25
        view2.layer.cornerRadius = 25
        view3.layer.cornerRadius = 25
        
        setBtnGradient(btn: resetPassword, color: colors.appColor, gradColor: colors.gradColor)
        resetPassword.layer.cornerRadius = 25
    }
    
    func resetPasswordRequest(){
        lott = playAnimation(vc: self)

        let param = [
            "method": "update_password",
            "password": "\(passwordTF.text!)",
            "email": "\(mail)"
        ]
        Alamofire.request(urls.healthyPlusURL, method: .post, parameters: param, encoding: JSONEncoding(options: []), headers: nil).responseJSON { (response) in
            switch response.result {
                
            case .success:
                if let value = response.result.value as? NSDictionary {
                    if value["state"] as? Int == 101{
                        let vc = UIStoryboard.init(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "signInVC") as! signInVC
                        self.present(vc, animated: true, completion: nil)
                    }
                    else{
                        if let status = value["state"] as? Int{
                            self.otherStatus(Number: status)
                        }
                    }
                    self.stopAnimation(Lot: self.lott)
                }
            case .failure:
                self.errorInConnection()
                self.stopAnimation(Lot: self.lott)
            }
        }
    }
    @IBAction func skipAction(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    @IBAction func resetPasswordAction(_ sender: Any) {
        if Code == codeTF.text{
            if passwordTF.text == confPasswordTF.text{
                if sharedHandler.isValidAction(textFeilds: [passwordTF, confPasswordTF]){
                    resetPasswordRequest()
                }
            }else{
                self.showAttentionMessage(msg: "Passowrds didn't Match")
            }
        }else{
             self.showAttentionMessage(msg: "Code is Error")
        }
    }
}
