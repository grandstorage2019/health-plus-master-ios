//
//  messagesVC.swift
//  HealthyPlus
//
//  Created by apple on 8/16/18.
//  Copyright © 2018 Kirollos. All rights reserved.
//

import UIKit
import Alamofire
import Lottie

class messagesVC: UIViewController {
    
    var method = " "
    var url = " "
    var parameterID = " "
    var arrMessage = Array<messages>()
    var isDoctor = true
    
    var lott: LOTAnimationView = LOTAnimationView(name: "Boat_Loader")
    
    @IBOutlet weak var messagesTbl: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        messagesTbl.delegate = self
        messagesTbl.dataSource = self
        
        setNavigationTitle(Title: "Messages")
    }
    override func viewDidAppear(_ animated: Bool) {
        loadMeassages()
    }
    override func viewDidDisappear(_ animated: Bool) {
        arrMessage.removeAll()
        messagesTbl.reloadData()
        messagesTbl.alpha = 0
    }
    func loadMeassages(){
        lott = playAnimation(vc: self)
        let param = [
            "method": "\(method)",
            "language":"\(sharedHandler.getLanguage())",
            "\(parameterID)":"\(sharedHandler.getUserID())"
        ]
        Alamofire.request(url, method: .post, parameters: param, encoding: JSONEncoding(options: []), headers: nil).responseJSON { (response) in
            switch response.result {
                
            case .success:
                if let value = response.result.value as? NSDictionary {
                    if value["state"] as? Int == 101{
                        if let arr = value["\(self.method)"] as? NSArray{
                            
                            for data in arr{
                                var doctor_id = 0
                                var patient_id = 0
                                var message = " "
                                var who = " "
                                var name = " "
                                var image = " "
                                var url = " "
                                
                                if let object = data as? Dictionary <String, Any>{
                                    if let value = object["doctor_id"] as? Int{
                                        doctor_id = value
                                    }
                                    if let value = object["patient_id"] as? Int{
                                        patient_id = value
                                    }
                                    if let value = object["massage"] as? String{
                                        message = value
                                    }
                                    if let value = object["who"] as? String{
                                        who = value
                                        if who == "patient"{
                                            url = urls.patientImage
                                        }else{
                                            url = urls.profileImage
                                        }
                                    }
                                    if let valuee = object["name"] as? String{
                                        name = valuee
                                    }
                                    if let valuee = object["fname"] as? String{
                                        name = valuee
                                    }
                                    if let valuee = object["lname"] as? String{
                                        name = name + " " + valuee
                                    }
                                    if let value = object["image"] as? String{
                                        image = url + value
                                    }
                                    if let value = object["profile_image"] as? String{
                                        image = url + value
                                    }
                                    
                                }
                                self.arrMessage.append(messages(doctor_id: doctor_id, patient_id: patient_id, message: message, who: who, name: name, image: image))
                            }
                            if self.arrMessage.count > 0{
                                self.messagesTbl.alpha = 1
                                self.messagesTbl.reloadData()
                            }else{
                                self.messagesTbl.alpha = 0
                            }
                        }
                    }
                    else{
                        if let status = value["state"] as? Int{
                            self.otherStatus(Number: status)
                        }
                    }
                    self.stopAnimation(Lot: self.lott)
                }
            case .failure:
                self.errorInConnection()
                self.stopAnimation(Lot: self.lott)
            }
        }
    }
}

extension messagesVC :UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrMessage.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = messagesTbl.dequeueReusableCell(withIdentifier: "cell") as! messagesCell
        circleImage(image: cell.personImage)
        customView(custom: cell.messageView)
        cell.settingData(imageURL: arrMessage[indexPath.row].image, Name: arrMessage[indexPath.row].name, msg: arrMessage[indexPath.row].message)
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        messagesTbl.deselectRow(at: indexPath, animated: true)
        
        let vc = UIStoryboard.init(name: "doctor", bundle: nil).instantiateViewController(withIdentifier: "chatVC") as! chatVC
        
        vc.method = "chat"
        vc.url = urls.healthyPlusURL
        vc.recivereImage = arrMessage[indexPath.row].image
        vc.patientID = arrMessage[indexPath.row].patient_id
        vc.doctorID = arrMessage[indexPath.row].doctor_id
        if isDoctor{
            vc.who = "doctor"
        }else{
            vc.who = "patient"
        }
        self.navigationController?.pushViewController(vc, animated: true)
    }
    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
        
        let delete = UITableViewRowAction(style: UITableViewRowActionStyle.default, title: "Delete".localized) { (UITableViewRowAction, indexpath) in
            
            
        }
        delete.backgroundColor = colors.appColor
        
        return [delete]
    }
}
