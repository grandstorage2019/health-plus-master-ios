//
//  chatVC.swift
//  HealthyPlus
//
//  Created by apple on 8/16/18.
//  Copyright © 2018 Kirollos. All rights reserved.
//

import UIKit
import Alamofire
import Lottie

class chatVC: UIViewController {
    
    var url = " "
    var sendMsgUrl = " "
    var method = " "
    var patientID = 0
    var doctorID = 0
    var recivereImage =  " "
    var who = " "
    var isDoctor = false
    var arrMessage = Array<messages>()
    
    var lott: LOTAnimationView = LOTAnimationView(name: "Boat_Loader")
    
    @IBOutlet weak var sendMsg: UITextField!
    @IBOutlet weak var chatTbl: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
    }
    override func viewDidAppear(_ animated: Bool) {
        loadMeassages()
    }
    @IBAction func sendAction(_ sender: Any) {
        lott = playAnimation(vc: self)
        if sendMsg.text != ""{
            let param = [
                "method":"send_message",
                "patient_id": "\(patientID)",
                "doctor_id": "\(doctorID)",
                "massage":"\(sendMsg.text!)",
                "who": "\(who)"
            ]
            
            Alamofire.request(urls.healthyPlusURL, method: .post, parameters: param, encoding: JSONEncoding(options: []), headers: nil).responseJSON { (response) in
                switch response.result {
                    
                case .success:
                    if let value = response.result.value as? NSDictionary {
                        if value["state"] as? Int == 101{
                            if self.who == "patient"{
                                self.arrMessage.append(messages(doctor_id: self.doctorID, patient_id: self.patientID, message: self.sendMsg.text!, who: "patient", name: " ", image: self.recivereImage))
                                let indexPath = NSIndexPath(row: self.arrMessage.count - 1, section: 0)
                                self.chatTbl.insertRows(at: [indexPath as IndexPath], with: UITableViewRowAnimation.left)
                                self.chatTbl.scrollToRow(at: indexPath as IndexPath, at: .bottom, animated: true)
                            }else{
                                self.arrMessage.append(messages(doctor_id: self.doctorID, patient_id: self.patientID, message: self.sendMsg.text!, who: "doctor", name: " ", image: self.recivereImage))
                                let indexPath = NSIndexPath(row: self.arrMessage.count - 1, section: 0)
                                self.chatTbl.insertRows(at: [indexPath as IndexPath], with: UITableViewRowAnimation.left)
                                self.chatTbl.scrollToRow(at: indexPath as IndexPath, at: .bottom, animated: true)
                                
                            }
                            self.sendMsg.text = ""
                            self.chatTbl.reloadData()
                        }else{
                            if let status = value["state"] as? Int{
                                self.otherStatus(Number: status)
                            }
                        }
                        self.stopAnimation(Lot: self.lott)
                    }
                case .failure:
                    self.errorInConnection()
                    self.stopAnimation(Lot: self.lott)
                }
            }
        }else{
            
            self.showAttentionMessage(msg: "type something before send")
            self.stopAnimation(Lot: self.lott)
        }
    }
    
    func scrollToBottom(){
        DispatchQueue.global(qos: .background).async {
            DispatchQueue.main.async {
                let indexPath = IndexPath(row: self.arrMessage.count-1, section: 0)
                self.chatTbl.scrollToRow(at: indexPath, at: .bottom, animated: true)
            }
        }
    }
    func loadMeassages(){
        lott = playAnimation(vc: self)
        let param = [
            "method": "\(method)",
            "language":"\(sharedHandler.getLanguage())",
            "doctor_id":"\(doctorID)",
            "patient_id": "\(patientID)"
        ]
        Alamofire.request(url, method: .post, parameters: param, encoding: JSONEncoding(options: []), headers: nil).responseJSON { (response) in
            switch response.result {
                
            case .success:
                if let value = response.result.value as? NSDictionary {
                    if value["state"] as? Int == 101{
                        if let arr = value["\(self.method)"] as? NSArray{
                            
                            for data in arr{
                                var doctor_id = 0
                                var patient_id = 0
                                var message = " "
                                var who = " "
                                var name = " "
                                var image = " "
                                var url = " "
                                
                                if let object = data as? Dictionary <String, Any>{
                                    if let value = object["doctor_id"] as? Int{
                                        doctor_id = value
                                    }
                                    if let value = object["patient_id"] as? Int{
                                        patient_id = value
                                    }
                                    if let value = object["massage"] as? String{
                                        message = value
                                    }
                                    if let value = object["who"] as? String{
                                        who = value
                                        if who == "patient"{
                                            url = urls.patientImage
                                        }else{
                                            url = urls.profileImage
                                        }
                                    }
                                    if let valuee = object["name"] as? String{
                                        name = valuee
                                    }
                                    if let value = object["image"] as? String{
                                        image = url + value
                                    }
                                    
                                }
                                self.arrMessage.append(messages(doctor_id: doctor_id, patient_id: patient_id, message: message, who: who, name: name, image: image))
                            }
                            if self.arrMessage.count > 0{
                                self.chatTbl.alpha = 1
                                self.chatTbl.reloadData()
                                self.scrollToBottom()
                            }else{
                                self.chatTbl.alpha = 0
                            }
                        }
                    }
                    else{
                        if let status = value["state"] as? Int{
                            self.otherStatus(Number: status)
                        }
                    }
                    self.stopAnimation(Lot: self.lott)
                }
            case .failure:
                self.errorInConnection()
                self.stopAnimation(Lot: self.lott)
            }
        }
    }
}

extension chatVC: UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrMessage.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if arrMessage[indexPath.row].who == who{
            let cell = chatTbl.dequeueReusableCell(withIdentifier: "senderCell") as! chatCell
            cell.message.text = arrMessage[indexPath.row].message
            customView(custom: cell.chatView)
            return cell
        }else{
            let cell = chatTbl.dequeueReusableCell(withIdentifier: "recieverCell") as! chatCell
            cell.message.text = arrMessage[indexPath.row].message
            setImage(url: recivereImage, Image: cell.personImage)
            circleImage(image: cell.personImage)
            customView(custom: cell.chatView)
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
}
