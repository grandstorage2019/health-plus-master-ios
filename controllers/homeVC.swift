//
//  homeVC.swift
//  HealthyPlus
//
//  Created by apple on 7/15/18.
//  Copyright © 2018 Kirollos. All rights reserved.
//

import UIKit
import Alamofire
import Gifu
import Lottie

class homeVC: UIViewController {
    
    var link = ""
    var arrdvertise = Array<advertiseModel>()
    
    var lott: LOTAnimationView = LOTAnimationView(name: "Boat_Loader")
    
    @IBOutlet weak var gifView: GIFImageView!
    @IBOutlet weak var adImage: UIImageView!
    @IBOutlet weak var adName: UILabel!
    @IBOutlet weak var collectionAdvertise: UICollectionView!
    
    @IBAction func adShow(_ sender: Any) {
        guard let url = URL(string: link) else {
            return //be safe
        }
        if #available(iOS 10.0, *) {
            UIApplication.shared.open(url, options: [:], completionHandler: nil)
        } else {
            UIApplication.shared.openURL(url)
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        var logo = UIImage()
        if sharedHandler.getpatientType() == 1{
            logo = UIImage(named: "101")!
            home(method: "h_plus_home", url: urls.healthyPlusURL, imageURL: urls.healthyPlusHomeImageURL, type: "hospitals")
        }else{
            logo = UIImage(named: "103")!
            home(method: "h_care_home", url: urls.healthyCarURL, imageURL: urls.healthyPlusCareImageURL, type: "h_care_home")
        }
        pushBarButtonLeft()
        self.navigationItem.titleView = UIImageView(image: logo)
        collectionAdvertise.delegate = self
        collectionAdvertise.dataSource = self
    }
    
    func home(method: String, url: String, imageURL: String, type: String){
        lott = playAnimation(vc: self)
        let param = [
            "method": "\(method)",
            "language": "\(sharedHandler.getLanguage())"
        ]
        Alamofire.request(url, method: .post, parameters: param, encoding: JSONEncoding(options: []), headers: nil).responseJSON { (response) in
            switch response.result {
                
            case .success:
                if let value = response.result.value as? NSDictionary {
                    if value["state"] as? Int == 101{
                        if let arr = value[type] as? NSArray{
                            for data in arr{
                                
                                var image = " "
                                var name = " "
                                var link = " "
                                var is_promited = false
                                
                                if let object = data as? Dictionary <String, Any>{
                                    if let value = object["image"] as? String{
                                        image = imageURL + value
                                    }
                                    if let value = object["name"] as? String{
                                        name = value
                                    }
                                    if let value = object["link"] as? String{
                                        link = value
                                    }
                                    if let value = object["is_promited"] as? Bool{
                                        is_promited = value
                                    }
                                    if let value = object["is_promoted"] as? Bool{
                                        is_promited = value
                                    }
                                    if is_promited == true{
                                        self.setImage(url: image, image: self.adImage)
                                        self.adName.text = name
                                        self.link = link
                                    }else{
                                        self.arrdvertise.append(advertiseModel(image: image, name: name, link: link, is_promited: is_promited))
                                    }
                                }
                            }
                            self.collectionAdvertise.reloadData()
                            //                            self.stopAnimation()
                        }
                    }else{
                        if let status = value["state"] as? Int{
                            self.otherStatus(Number: status)
                            //                            self.stopAnimation()
                        }
                    }
                    self.stopAnimation(Lot: self.lott)
                }
            case .failure:
                    self.errorInConnection()
                    self.stopAnimation(Lot: self.lott)
            }
        }
    }
    
    
}
extension homeVC: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return arrdvertise.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionAdvertise.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! advertiseCell
        cell.settingData(URL: self.arrdvertise[indexPath.row].image, adNamee: self.arrdvertise[indexPath.row].name)
        cell.adShow.tag = indexPath.row
        cell.adShow.addTarget(self, action: #selector(showAdvertise), for: UIControlEvents.touchUpInside)
        return cell
    }
    @objc func showAdvertise(sender : UIButton){
        let data = arrdvertise[sender.tag]
        
        guard let url = URL(string: data.link) else {
            return //be safe
        }
        if #available(iOS 10.0, *) {
            UIApplication.shared.open(url, options: [:], completionHandler: nil)
        } else {
            UIApplication.shared.openURL(url)
        }
    }
}
