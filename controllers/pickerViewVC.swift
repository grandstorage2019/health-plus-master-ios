//
//  pickerViewVC.swift
//  HealthyPlus
//
//  Created by apple on 7/19/18.
//  Copyright © 2018 Kirollos. All rights reserved.
//

import UIKit
import Alamofire
import Lottie

class pickerViewVC: UIViewController {
    
    var count = 0
    var areaID = 0
    var countryID = 0
    var governorateID = 0
    var specializationID = 0
    var arrRegion = Array<services>()
    
    var lott: LOTAnimationView = LOTAnimationView(name: "Boat_Loader")

    @IBOutlet weak var picker: UIPickerView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        picker.delegate = self
        picker.dataSource = self
        
        setNavigationTitle(Title: "Search")
        
        if sharedHandler.getpatientType() == 1{
            specializationData(url: urls.healthyPlusURL, method: "specializations", id: 0, type: " ")
        }else{
            specializationData(url: urls.healthyCarURL, method: "specializations", id: 0, type: " ")
        }
        
        let barButton = UIBarButtonItem(title: "Done".localized, style: .done, target: self, action: #selector(doneAction))
        self.navigationItem.rightBarButtonItem = barButton
    }
    func specializationData(url: String, method: String, id: Int, type: String){
        lott = playAnimation(vc: self)

        let param = [
            "method": "\(method)",
            "language": "\(sharedHandler.getLanguage())",
            "\(type)": "\(id)"
        ]
        Alamofire.request(url, method: .post, parameters: param, encoding: JSONEncoding(options: []), headers: nil).responseJSON { (response) in
            switch response.result {
                
            case .success:
                if let value = response.result.value as? NSDictionary {
                    if value["state"] as? Int == 101{
                        if let arr = value["\(method)"] as? NSArray{
                            self.arrRegion.removeAll()
                            self.picker.reloadAllComponents()
                            if method == "areas"{
                                self.arrRegion.append(services(id: -1, name: "All".localized ))
                            }
                            for data in arr{
                                
                                var id = 0
                                var name = " "
                                
                                if let object = data as? Dictionary <String, Any>{
                                    if let value = object["id"] as? Int{
                                        id = value
                                    }
                                    if let value = object["name"] as? String{
                                        name = value
                                    }
                                }
                                self.arrRegion.append(services(id: id, name: name))
                            }
                            if arr.count > 0 {
                                if self.count == 0{
                                    self.specializationID = self.arrRegion[0].id
                                }else if self.count == 1{
                                    self.countryID = self.arrRegion[0].id
                                }else if self.count == 2{
                                    self.governorateID = self.arrRegion[0].id
                                }else{
                                    self.areaID = self.arrRegion[0].id
                                }
                            }
                            self.picker.reloadAllComponents()
                        }
                    }
                    else{
                        if let status = value["state"] as? Int{
                            self.otherStatus(Number: status)
                        }
                    }
                    self.stopAnimation(Lot: self.lott)

                }
            case .failure:
                self.errorInConnection()
                self.stopAnimation(Lot: self.lott)
                if self.count == 0{
                }else if self.count == 1{
                    self.count -= 1
                }else if self.count == 2{
                    self.count -= 1
                }else{
                    self.count -= 1
                }

            }
        }
    }
}

extension pickerViewVC:  UIPickerViewDelegate, UIPickerViewDataSource{
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return arrRegion.count
    }
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return arrRegion[row].name
    }
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        if arrRegion.count > 0{
            if count == 0{
                specializationID = arrRegion[row].id
            }else if count == 1{
                countryID = arrRegion[row].id
            }else if count == 2{
                governorateID = arrRegion[row].id
            }else{
                areaID = arrRegion[row].id
            }
        }
    }
    
    @objc func doneAction(){
        if count == 0{
            count += 1
            specializationData(url: urls.healthyPlusURL, method: "countries", id: specializationID, type: "country_id")
        }else if count == 1{
            count += 1
            specializationData(url: urls.healthyPlusURL, method: "governorates", id: countryID, type: "country_id")
        }else if count == 2{
            count += 1
            specializationData(url: urls.healthyPlusURL, method: "areas", id: governorateID, type: "governorate_id")
        }else{
            let vc = UIStoryboard.init(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "favouriteVC") as! favouriteVC
            vc.areaID = areaID
            vc.specializationID = specializationID
            vc.navigation = "Search"
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
}
