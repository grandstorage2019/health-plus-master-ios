//
//  useTypeVC.swift
//  HealthyPlus
//
//  Created by apple on 7/12/18.
//  Copyright © 2018 Kirollos. All rights reserved.
//

import UIKit

class userTypeVC: UIViewController {
    
    var userType = 0
    
    @IBOutlet weak var DoctorBtn: UIButton!
    @IBOutlet weak var PatientBtn: UIButton!
    @IBOutlet weak var textLbl: UILabel!
    @IBOutlet weak var userTypeStack: UIStackView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        DoctorBtn.layer.cornerRadius = 10
        PatientBtn.layer.cornerRadius = 10
        
        setBtnGradient(btn: DoctorBtn, color: colors.appColor, gradColor: colors.gradColor)
        setBtnGradient(btn: PatientBtn, color: UIColor.gray, gradColor: UIColor.lightGray)
    }
    @IBAction func selectionAction(_ sender: UIButton) {
        userType = sender.tag
        
        let defaults = UserDefaults.standard
        defaults.set(userType, forKey: "userType")
        if sender.tag == 1{
            let jobDetailViewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "signInVC") as! signInVC
            present(jobDetailViewController, animated: true, completion: nil)
        }else{
            selectPatientType()
        }
    }
}
