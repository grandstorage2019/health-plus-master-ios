//
//  searchVC.swift
//  HealthyPlus
//
//  Created by apple on 7/15/18.
//  Copyright © 2018 Kirollos. All rights reserved.
//

import UIKit

class searchVC: UIViewController {
    
    @IBOutlet weak var view1: UIView!
    @IBOutlet weak var view2: UIView!
    @IBOutlet weak var searchTxt: UITextField!
    
    
    @IBAction func selectAction(_ sender: Any) {
        let vc = UIStoryboard.init(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "pickerViewVC") as! pickerViewVC
        self.navigationController?.pushViewController(vc, animated: true)
    }
    @IBAction func searchAction(_ sender: Any) {
        if searchTxt.text != ""{
            let vc = UIStoryboard.init(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "favouriteVC") as! favouriteVC
            vc.txt = searchTxt.text!
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        pushBarButtonLeft()
        setNavigationTitle(Title: "Search")
        customView(custom: view1)
        customView(custom: view2)
        if sharedHandler.getLanguage() == "en"{
            searchTxt.textAlignment = .left
        }else{
            searchTxt.textAlignment = .right
        }
    }
}
