//
//  confirmationLicenseVC.swift
//  HealthyPlus
//
//  Created by apple on 8/16/18.
//  Copyright © 2018 Kirollos. All rights reserved.
//

import UIKit
import Photos
import Alamofire
import BSImagePicker
import Lottie

class confirmationLicenseVC: UIViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    var isPicked = false
    var selectedImage = UIImagePickerController()
    
    var lott: LOTAnimationView = LOTAnimationView(name: "Boat_Loader")

    @IBOutlet weak var LicenseImage: UIImageView!
    @IBOutlet weak var gradBtn: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setBtnGradient(btn: gradBtn, color: colors.appColor, gradColor: colors.gradColor)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        if picker == selectedImage{
            if let originalImage = info[UIImagePickerControllerOriginalImage] as? UIImage{
                self.isPicked = true
                LicenseImage.image = originalImage
            }
            self.dismiss(animated: true, completion: nil)
        }
    }
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func confirmLicenseAction(_ sender: Any) {
        if isPicked{
            let data: Data? = UIImageJPEGRepresentation(LicenseImage.image!, 0.0)
            let imageInBase64 =  data?.base64EncodedString(options: .endLineWithLineFeed) ?? ""
            lott = playAnimation(vc: self)

            let param = [
                "method":"upload_licence",
                "image_name":"picture.jpg",
                "image_data": imageInBase64,
                "id": "\(sharedHandler.getUserID())"
            ]
            Alamofire.request(urls.filterInsurance, method: .post, parameters: param, encoding: JSONEncoding(options: []), headers: nil)
                .responseJSON { response in
                    switch response.result {
                    case .success:
                        if let value = response.result.value as? NSDictionary {
                            if value["state"] as? Int == 101{
                                let vc = UIStoryboard.init(name: "doctor", bundle: nil).instantiateViewController(withIdentifier: "plansVC") as! plansVC
                                
                                self.navigationController?.pushViewController(vc, animated: true)
                            }else{
                                if let status = value["state"] as? Int{
                                    self.otherStatus(Number: status)
                                }
                            }
                            self.stopAnimation(Lot: self.lott)
                        }
                    case .failure:
                        self.errorInConnection()
                        self.stopAnimation(Lot: self.lott)

                    }
            }
        }
    }
    @IBAction func addPictureAcion(_ sender: Any) {
        let status = PHPhotoLibrary.authorizationStatus()
        if (status == PHAuthorizationStatus.authorized) {
            selectedImage.delegate = self
            selectedImage.sourceType = UIImagePickerControllerSourceType.photoLibrary
            selectedImage.allowsEditing = false
            self.present(selectedImage, animated: true)
        } else if (status == PHAuthorizationStatus.denied) {
        } else if (status == PHAuthorizationStatus.notDetermined) {
            PHPhotoLibrary.requestAuthorization({ (newStatus) in
                
                if (newStatus == PHAuthorizationStatus.authorized) {
                    self.selectedImage.delegate = self
                    self.selectedImage.sourceType = UIImagePickerControllerSourceType.photoLibrary
                    self.selectedImage.allowsEditing = false
                    self.present(self.selectedImage, animated: true)
                }
            })
        }
    }
}
