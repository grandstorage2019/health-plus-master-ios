//
//  confirmPlanVC.swift
//  HealthyPlus
//
//  Created by apple on 8/16/18.
//  Copyright © 2018 Kirollos. All rights reserved.
//

import UIKit
import Alamofire
import Lottie

class confirmPlanVC: UIViewController {
    
    var planID = 0
    var details =  " "
    var lott: LOTAnimationView = LOTAnimationView(name: "Boat_Loader")

    @IBOutlet weak var planView: UIView!
    @IBOutlet weak var planDetails: UILabel!
    @IBOutlet weak var gradBtn: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        planDetails.text = details
        customView(custom: planView)
        setBtnGradient(btn: gradBtn, color: colors.appColor, gradColor: colors.gradColor)
    }
    @IBAction func nextAction(_ sender: Any) {
        setplan()
    }
    func setplan(){
        lott = playAnimation(vc: self)

        let param = [
            "method":"set_plan",
            "plan_id":"\(planID)",
            "id": "\(sharedHandler.getUserID())"
        ]
        Alamofire.request(urls.filterInsurance, method: .post, parameters: param, encoding: JSONEncoding(options: []), headers: nil).responseJSON { (response) in
            switch response.result {
                
            case .success:
                if let value = response.result.value as? NSDictionary {
                    if value["state"] as? Int == 101{
                        self.pushHome()
                    }else{
                        if let status = value["state"] as? Int{
                            self.otherStatus(Number: status)
                        }
                    }
                    self.stopAnimation(Lot: self.lott)

                }
            case .failure:
                self.errorInConnection()
                self.stopAnimation(Lot: self.lott)

            }
        }
    }
}
