//
//  plansVC.swift
//  HealthyPlus
//
//  Created by apple on 8/16/18.
//  Copyright © 2018 Kirollos. All rights reserved.
//

import UIKit
import Alamofire
import Lottie

class plansVC: UIViewController {
    
    var arrPlans = Array<plans>()
    var lott: LOTAnimationView = LOTAnimationView(name: "Boat_Loader")

    @IBOutlet weak var plansTbl: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        loadPlans()
        plansTbl.delegate = self
        plansTbl.dataSource = self
    }
    
    func loadPlans(){
        lott = playAnimation(vc: self)

        let param = [
            "method": "plans",
            "language":"\(sharedHandler.getLanguage())"
        ]
        Alamofire.request(urls.filterInsurance, method: .post, parameters: param, encoding: JSONEncoding(options: []), headers: nil).responseJSON { (response) in
            switch response.result {
                
            case .success:
                if let value = response.result.value as? NSDictionary {
                    if value["state"] as? Int == 101{
                        if let arr = value["plans"] as? NSArray{
                            
                            for data in arr{
                                var id = " "
                                var name = " "
                                var details = " "
                                
                                if let object = data as? Dictionary <String, Any>{
                                    
                                    if let value = object["id"] as? String{
                                        id = value
                                    }
                                    if let valuee = object["name"] as? String{
                                        name = valuee
                                    }
                                    if let value = object["details"] as? String{
                                        details = value
                                    }
                                    
                                }
                                self.arrPlans.append(plans(id: id, name: name, details: details))
                            }
                            self.plansTbl.reloadData()
                        }
                    }
                    else{
                        if let status = value["state"] as? Int{
                            self.otherStatus(Number: status)
                        }
                    }
                    self.stopAnimation(Lot: self.lott)

                }
            case .failure:
                self.errorInConnection()
                self.stopAnimation(Lot: self.lott)

            }
        }
    }
}

extension plansVC: UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrPlans.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = plansTbl.dequeueReusableCell(withIdentifier: "cell") as! plansCEll
        setCustomViewGradient(customView: cell.gradView, color: colors.appColor, gradColor: colors.gradColor)
        cell.settingData(Name: arrPlans[indexPath.row].name, Details: arrPlans[indexPath.row].details)
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let vc = UIStoryboard.init(name: "doctor", bundle: nil).instantiateViewController(withIdentifier: "confirmPlanVC") as! confirmPlanVC
        vc.details = arrPlans[indexPath.row].details
        vc.planID = Int(arrPlans[indexPath.row].id)!
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
}
