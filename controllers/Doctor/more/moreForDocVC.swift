//
//  moreDorDocVC.swift
//  HealthyPlus
//
//  Created by apple on 8/5/18.
//  Copyright © 2018 Kirollos. All rights reserved.
//

import UIKit
import MOLH

class moreForDocVC: UIViewController {
    
    @IBOutlet weak var view1: UIView!
    @IBOutlet weak var view2: UIView!
    @IBOutlet weak var view3: UIView!
    @IBOutlet weak var view4: UIView!
    @IBOutlet weak var view5: UIView!
    
    @IBOutlet weak var arBtn: UIButton!
    @IBOutlet weak var enBtn: UIButton!
    
    @IBOutlet weak var ar: UILabel!
    @IBOutlet weak var en: UILabel!
    
    @IBOutlet weak var viewHeight: NSLayoutConstraint!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        customView(custom: view1)
        customView(custom: view2)
        customView(custom: view3)
        customView(custom: view4)
        customView(custom: view5)
        
        setNavigationTitle(Title: "Settings")
        if MOLHLanguage.currentAppleLanguage() == "en"{
            enBtn.setImage(#imageLiteral(resourceName: "select"), for: .normal)
        }else{
            arBtn.setImage(#imageLiteral(resourceName: "select"), for: .normal)
        }
    }
    func setAlph(alpha: CGFloat){
        ar.alpha = alpha
        en.alpha = alpha
        arBtn.alpha = alpha
        enBtn.alpha = alpha
        if alpha == 0.0{
            
        }
    }
    @IBAction func accountSettingAction(_ sender: Any) {
        let vc = UIStoryboard.init(name: "doctor", bundle: nil).instantiateViewController(withIdentifier: "docSignUpVc") as! docSignUpVc
        vc.isUpdate = true
        present(vc, animated: true, completion: nil)
    }
    @IBAction func changeLanguageAction(_ sender: Any) {
        if viewHeight.constant == CGFloat(0){
            UIView.animate(withDuration: 0.5, animations: {
                self.viewHeight.constant = CGFloat(74)
                self.setAlph(alpha: 1.0)
            })
            
        }else{
            self.setAlph(alpha: 0.0)
            self.viewHeight.constant = 0.0
        }
    }
    @IBAction func helpCenterAction(_ sender: Any) {
        let vc = UIStoryboard.init(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "contactUsVC") as! contactUsVC
        self.navigationController?.pushViewController(vc, animated: true)
    }
    @IBAction func signOutAction(_ sender: Any) {
        UserDefaults.standard.removeObject(forKey: "userType")
        UserDefaults.standard.removeObject(forKey: "patientType")
        UserDefaults.standard.removeObject(forKey: "id")
        UserDefaults.standard.removeObject(forKey: "loginStatus")
        UserDefaults.standard.removeObject(forKey: "image")
        UserDefaults.standard.removeObject(forKey: "CountryID")
        UserDefaults.standard.removeObject(forKey: "birthDate")
        UserDefaults.standard.removeObject(forKey: "gender")
        UserDefaults.standard.removeObject(forKey: "enLname")
        UserDefaults.standard.removeObject(forKey: "arLname")
        UserDefaults.standard.removeObject(forKey: "arFname")
        UserDefaults.standard.removeObject(forKey: "phone")
        UserDefaults.standard.removeObject(forKey: "email")
        UserDefaults.standard.removeObject(forKey: "userName")
        UserDefaults.standard.removeObject(forKey: "isRemember")
        UserDefaults.standard.removeObject(forKey: "SpecilizationID")
        let vc = UIStoryboard.init(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "userTypeVC") as! userTypeVC
        present(vc, animated: true, completion: nil)
    }
    @IBAction func changeAction(_ sender: UIButton) {
        if sender.tag == 0 && sharedHandler.getLanguage() != "ar"{
            arBtn.setImage(#imageLiteral(resourceName: "select"), for: .normal)
            enBtn.setImage(#imageLiteral(resourceName: "unSelect"), for: .normal)
            MOLH.setLanguageTo(MOLHLanguage.currentAppleLanguage() == "en" ? "ar" : "en")
            MOLH.reset()
        }else if sender.tag == 1 && sharedHandler.getLanguage() != "en"{
            arBtn.setImage(#imageLiteral(resourceName: "unSelect"), for: .normal)
            enBtn.setImage(#imageLiteral(resourceName: "select"), for: .normal)
            MOLH.setLanguageTo(MOLHLanguage.currentAppleLanguage() == "en" ? "ar" : "en")
            MOLH.reset()
        }
    }
}
