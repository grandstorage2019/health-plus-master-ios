//
//  acceptionVC.swift
//  HealthyPlus
//
//  Created by apple on 8/8/18.
//  Copyright © 2018 Kirollos. All rights reserved.
//

import UIKit
import Alamofire
import Lottie

class acceptionVC: UIViewController {
    
    var url = urls.filterInsurance
    var uploadMethod = " "
    var selecImage = UIImage()
    var lott: LOTAnimationView = LOTAnimationView(name: "Boat_Loader")

    
    @IBOutlet weak var viewDetails: UIView!
    @IBOutlet weak var btn1: UIButton!
    @IBOutlet weak var btn2: UIButton!
    @IBOutlet weak var imageV: UIImageView!
    @IBAction func confirmBtn(_ sender: Any) {
        uploadImage()
    }
    @IBAction func canecelBtn(_ sender: Any) {
        removeAnimation()
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        imageV.image = selecImage
        customView(custom: viewDetails)
        showAnimate()
        setBtnGradient(btn: btn1, color: colors.appColor, gradColor: colors.gradColor)
        setBtnGradient(btn: btn2, color: colors.appColor, gradColor: colors.gradColor)
        
    }
    override func viewDidDisappear(_ animated: Bool) {
        self.view.reloadInputViews()
    }
    func showAnimate() {
        self.view.transform = CGAffineTransform(scaleX: 1.3 , y: 1.3)
        self.view.alpha = 0.0;
        UIView.animate(withDuration: 0.25) {
            self.view.alpha = 1.0
            self.view.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
        }
    }
    
    func removeAnimation() {
        UIView.animate(withDuration: 0.25, animations: {
            self.view.transform = CGAffineTransform(scaleX: 1.3 , y: 1.3)
            self.view.alpha = 0.0;
        }) { (finished : Bool) in
            if finished {
                self.view.removeFromSuperview()
            }
        }
    }
    func uploadImage(){
        let data: Data? = UIImageJPEGRepresentation(selecImage, 0.0)
        let imageInBase64 =  data?.base64EncodedString(options: .endLineWithLineFeed) ?? ""
        lott = playAnimation(vc: self)

        let param = [
            "method":"\(uploadMethod)",
            "image_name":"picture.jpg",
            "image_data": imageInBase64,
            "id": "\(sharedHandler.getUserID())"
        ]
        Alamofire.request(url, method: .post, parameters: param, encoding: JSONEncoding(options: []), headers: nil)
            .responseJSON { response in
                switch response.result {
                case .success:
                    if let value = response.result.value as? NSDictionary {
                        if value["state"] as? Int == 101{
                            self.removeAnimation()
                            self.perform(#selector(self.refreshProfileView), with: nil, afterDelay: 3.2)
                        }else{
                            if let status = value["state"] as? Int{
                                self.otherStatus(Number: status)
                            }
                        }
                        self.stopAnimation(Lot: self.lott)
                    }
                case .failure:
                    self.errorInConnection()
                    self.stopAnimation(Lot: self.lott)

                }
        }
    }
    @objc func refreshProfileView() {
        NotificationCenter.default.post(name: NSNotification.Name("TO_IMAGE"), object: nil)
        NotificationCenter.default.post(name: NSNotification.Name("License"), object: nil)
    }
}
