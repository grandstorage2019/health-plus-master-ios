//
//  clinicAddressVC.swift
//  HealthyPlus
//
//  Created by apple on 8/6/18.
//  Copyright © 2018 Kirollos. All rights reserved.
//

import UIKit
import Alamofire
import Lottie

class clinicAddressVC: UIViewController {
    
    var cityID    = 0
    var areaID    = 0
    var countryID = 0
    var lat       = ""
    var lng       = ""
    var response  : clinicAddressResponse?
    
    var picker1 = UIPickerView()
    var picker2 = UIPickerView()
    var picker3 = UIPickerView()
    
    var arrArea = Array<services>()
    var arrCity = Array<services>()
    var arrCountry = Array<services>()
    
    var lott: LOTAnimationView = LOTAnimationView(name: "Boat_Loader")

    @IBOutlet weak var lbl1: UILabel!
    @IBOutlet weak var lbl2: UILabel!
    @IBOutlet weak var lbl3: UILabel!
    @IBOutlet weak var lbl4: UILabel!
    @IBOutlet weak var lbl5: UILabel!
    @IBOutlet weak var lbl6: UILabel!
    @IBOutlet weak var lbl7: UILabel!
    @IBOutlet weak var lbl8: UILabel!
    @IBOutlet weak var lbl9: UILabel!
    @IBOutlet weak var lbl10: UILabel!
    @IBOutlet weak var lbl11: UILabel!
    @IBOutlet weak var lbl12: UILabel!
    @IBOutlet weak var lbl13: UILabel!
    @IBOutlet weak var lbl14: UILabel!
    @IBOutlet weak var lbl15: UILabel!
    @IBOutlet weak var lbl16: UILabel!
    
    @IBOutlet weak var countryTF1    : UITextField!
    @IBOutlet weak var cityTF2       : UITextField!
    @IBOutlet weak var areaTF3       : UITextField!
    @IBOutlet weak var arStreet4     : UITextField!
    @IBOutlet weak var enStreetTF5   : UITextField!
    @IBOutlet weak var arBlockTF6    : UITextField!
    @IBOutlet weak var enBlockTF7    : UITextField!
    @IBOutlet weak var arBuildingTF8 : UITextField!
    @IBOutlet weak var enBuildingTF9 : UITextField!
    @IBOutlet weak var arFloorTF10   : UITextField!
    @IBOutlet weak var enFloorTF11   : UITextField!
    @IBOutlet weak var arFlatTF12    : UITextField!
    @IBOutlet weak var enFlatTF13    : UITextField!
    @IBOutlet weak var arlandMarkTF14: UITextField!
    @IBOutlet weak var enLandMark15  : UITextField!
    @IBOutlet weak var locationTF16  : UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        picker1.delegate = self
        picker1.dataSource = self
        
        picker2.delegate = self
        picker2.dataSource = self
        
        picker3.delegate = self
        picker3.dataSource = self
        
        countryData()
        
        
        cityTF2.inputView = picker2
        areaTF3.inputView = picker3
        countryTF1.inputView = picker1
        
        setNavigationTitle(Title: "Clinic Address")
        
        let barButton = UIBarButtonItem(title: "Save".localized, style: .done, target: self, action: #selector(SaveBtn))
        self.navigationItem.rightBarButtonItem = barButton
    }
    override func viewDidAppear(_ animated: Bool) {
        LoadData()
    }
    
    @objc func SaveBtn(){
        if sharedHandler.isValidAction(textFeilds: [countryTF1, cityTF2, areaTF3, arStreet4, enStreetTF5, arBlockTF6, enBlockTF7, arBuildingTF8, enBuildingTF9, arFloorTF10, enFloorTF11, arFlatTF12, enFlatTF13, arlandMarkTF14, enLandMark15]){
            sendClinicAddress()
        }else{
            self.showAttentionMessage(msg: "please, Enter All Data")
        }
    }
    
    func LoadData(){
//        lott = playAnimation(vc: self)
        let param = [
            "method"   :"get_clinic_address",
            "doctor_id":"\(sharedHandler.getUserID())",
            "language" :"\(sharedHandler.getLanguage())"
        ]
        
        APIManager.sharedInstance.postRequest(urls.filterInsurance, Parameters: param) { (res) in
            if res.error == nil{
                do {
                    self.response = try JSONDecoder().decode(clinicAddressResponse.self, from: res.data!)
                    if self.response?.state == 101{
                        self.lat                 = (self.response?.clinic_address![0].lat)!
                        self.lng                 = (self.response?.clinic_address![0].lang)!
                        self.areaID              = (self.response?.clinic_address![0].area_id)!
                        self.countryID           = (self.response?.clinic_address![0].country_id)!
                        self.cityID              = (self.response?.clinic_address![0].governorate_id)!
                        self.countryTF1.text     = (self.response?.clinic_address![0].countries_name)!
                        self.areaTF3.text        = (self.response?.clinic_address![0].area_name)!
                        self.cityTF2.text        = (self.response?.clinic_address![0].governorate_name)!
                        self.arStreet4.text      = (self.response?.clinic_address![0].street_name_ar)!
                        self.enStreetTF5.text    = (self.response?.clinic_address![0].street_name_en)!
                        self.arBlockTF6.text     = (self.response?.clinic_address![0].block_ar)!
                        self.enBlockTF7.text     = (self.response?.clinic_address![0].block_en)!
                        self.arBuildingTF8.text  = (self.response?.clinic_address![0].building_number_ar)!
                        self.enBuildingTF9.text  = (self.response?.clinic_address![0].building_number_en)!
                        self.arFloorTF10.text    = (self.response?.clinic_address![0].floor_ar)!
                        self.enFloorTF11.text    = (self.response?.clinic_address![0].floor_en)!
                        self.arFlatTF12.text     = (self.response?.clinic_address![0].apartment_ar)!
                        self.enFlatTF13.text     = (self.response?.clinic_address![0].apartment_en)!
                        self.arlandMarkTF14.text = (self.response?.clinic_address![0].landmark_ar)!
                        self.enLandMark15.text   = (self.response?.clinic_address![0].landmark_en)!
//                        self.stopAnimation(Lot: self.lott)
                    }
                    
                }catch{
                    self.errorInConnection()
                }
            }else{
                self.errorInConnection()
            }
//            self.stopAnimation(Lot: self.lott)
        }
    }
    
    @IBAction func locationAction(_ sender: Any) {
        let vc = UIStoryboard.init(name: "doctor", bundle: nil).instantiateViewController(withIdentifier: "clientMapVC") as! clientMapVC
        vc.lat = lat
        vc.lang = lng
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func sendClinicAddress() {
        lott = playAnimation(vc: self)

        let param = [
            "method"             : "edit_clinic_address",
            "doctor_id"          : "\(sharedHandler.getUserID())",
            "area_id"            : "\(areaID)",
            "building_number_ar" : "\(arBuildingTF8.text!)",
            "building_number_en" : "\(enBuildingTF9.text!)",
            "street_name_ar"     : "\(arStreet4.text!)",
            "street_name_en"     : "\(enStreetTF5.text!)",
            "floor_ar"           : "\(arFloorTF10.text!)",
            "floor_en"           : "\(enFloorTF11.text!)",
            "apartment_ar"       : "\(arFlatTF12.text!)",
            "apartment_en"       : "\(enFlatTF13.text!)",
            "landmark_ar"        : "\(arlandMarkTF14.text!)",
            "landmark_en"        : "\(enLandMark15.text!)",
            "block_ar"           : "\(arBlockTF6.text!)",
            "block_en"           : "\(enBlockTF7.text!)"
        ]
        Alamofire.request(urls.filterInsurance, method: .post, parameters: param, encoding: JSONEncoding(options: []), headers: nil).responseJSON { (response) in
            switch response.result {
                
            case .success:
                if let value = response.result.value as? NSDictionary {
                    if value["state"] as? Int == 101{
                        self.navigationController?.popViewController(animated: true)
                    }
                    else{
                        if let status = value["state"] as? Int{
                            self.otherStatus(Number: status)
                        }
                    }
                    self.stopAnimation(Lot: self.lott)
                }
            case .failure:
                self.errorInConnection()
                self.stopAnimation(Lot: self.lott)
            }
        }
    }
}

extension clinicAddressVC: UITextFieldDelegate{
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField == countryTF1{
            showLabel(label: lbl1)
        }else if textField == cityTF2{
            showLabel(label: lbl2)
        }else if textField == areaTF3{
            showLabel(label: lbl3)
        }else if textField == arStreet4{
            showLabel(label: lbl4)
        }else if textField == enStreetTF5{
            showLabel(label: lbl5)
        }else if textField == arBlockTF6{
            showLabel(label: lbl6)
        }else if textField == enBlockTF7{
            showLabel(label: lbl7)
        }else if textField == arBuildingTF8{
            showLabel(label: lbl8)
        }else if textField == enBuildingTF9{
            showLabel(label: lbl9)
        }else if textField == arFloorTF10{
            showLabel(label: lbl10)
        }else if textField == enFloorTF11{
            showLabel(label: lbl11)
        }else if textField == arFlatTF12{
            showLabel(label: lbl12)
        }else if textField == enFlatTF13{
            showLabel(label: lbl13)
        }else if textField == arlandMarkTF14{
            showLabel(label: lbl14)
        }else if textField == enLandMark15{
            showLabel(label: lbl15)
        }else{
            showLabel(label: lbl16)
        }
    }
    func textFieldDidEndEditing(_ textField: UITextField) {
        if textField.text != ""{
            if textField == countryTF1         {
                grayLabel(label: lbl1)
            }else if textField == cityTF2      {
                grayLabel(label: lbl2)
            }else if textField == areaTF3      {
                grayLabel(label: lbl3)
            }else if textField == arStreet4    {
                grayLabel(label: lbl4)
            }else if textField == enStreetTF5  {
                grayLabel(label: lbl5)
            }else if textField == arBlockTF6   {
                grayLabel(label: lbl6)
            }else if textField == enBlockTF7   {
                grayLabel(label: lbl7)
            }else if textField == arBuildingTF8{
                grayLabel(label: lbl8)
            }else if textField == enBuildingTF9{
                grayLabel(label: lbl9)
            }else if textField == arFloorTF10  {
                grayLabel(label: lbl10)
            }else if textField == enFloorTF11  {
                grayLabel(label: lbl11)
            }else if textField == arFlatTF12   {
                grayLabel(label: lbl12)
            }else if textField == enFlatTF13   {
                grayLabel(label: lbl13)
            }else if textField == arlandMarkTF14{
                grayLabel(label: lbl14)
            }else if textField == enLandMark15  {
                grayLabel(label: lbl15)
            }else{
                grayLabel(label: lbl16)
            }
        }
    }
    func showLabel(label: UILabel){
        UIView.animate(withDuration: 0.5, animations: {
            //  self.newImages.alpha = 0.0
            label.alpha = 1.0
        })
    }
    func grayLabel(label: UILabel) {
        label.textColor = UIColor.lightGray
    }
}

extension clinicAddressVC: UIPickerViewDelegate, UIPickerViewDataSource{
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        if pickerView == picker1{
            return arrCountry.count
        }else if pickerView == picker2{
            return arrCity.count
        }else{
            return arrArea.count
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        if pickerView == picker1{
            if arrCountry.count > 0 {
                return arrCountry[row].name
            }else{
                countryData()
                return " "
            }
        }else if pickerView == picker2{
            if arrCity.count > 0 {
                return arrCity[row].name
            }else{
                self.showAttentionMessage(msg: "Please Enter Country First")
                return " "
            }
        }else{
            if arrArea.count > 0 {
                return arrArea[row].name
            }else{
                self.showAttentionMessage(msg: "Please Enter Country First")
                return " "
            }
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        if pickerView == picker1{
            if arrCountry.count > 0{
                self.countryID = arrCountry[row].id
                self.countryTF1.text = arrCountry[row].name
                self.cityData()
            }
        }else if pickerView == picker2{
            if arrCity.count > 0{
                self.cityID = arrCity[row].id
                self.cityTF2.text = arrCity[row].name
                self.areaData()
            }
        }else{
            if arrArea.count > 0{
                self.areaID = arrArea[row].id
                self.areaTF3.text = arrArea[row].name
            }
        }
    }
    
    func countryData(){
        lott = playAnimation(vc: self)

        let param = [
            "method": "countries",
            "language": "\(sharedHandler.getLanguage())"
        ]
        Alamofire.request(urls.healthyPlusURL, method: .post, parameters: param, encoding: JSONEncoding(options: []), headers: nil).responseJSON { (response) in
            switch response.result {
                
            case .success:
                if let value = response.result.value as? NSDictionary {
                    if value["state"] as? Int == 101{
                        if let arr = value["countries"] as? NSArray{
                            for data in arr{
                                
                                var id = 0
                                var name = " "
                                
                                if let object = data as? Dictionary <String, Any>{
                                    if let value = object["id"] as? Int{
                                        id = value
                                    }
                                    if let value = object["name"] as? String{
                                        name = value
                                    }
                                }
                                self.arrCountry.append(services(id: id, name: name))
                            }
                            self.picker1.reloadAllComponents()
                        }
                    }else{
                        if let status = value["state"] as? Int{
                            self.otherStatus(Number: status)
                        }
                    }
                    self.stopAnimation(Lot: self.lott)
                }
            case .failure:
                self.errorInConnection()
                self.stopAnimation(Lot: self.lott)
            }
        }
    }
    
    func cityData(){
        lott = playAnimation(vc: self)

        let param = [
            "method": "governorates",
            "language": "\(sharedHandler.getLanguage())",
            "country_id": "\(countryID)"
        ]
        Alamofire.request(urls.healthyPlusURL, method: .post, parameters: param, encoding: JSONEncoding(options: []), headers: nil).responseJSON { (response) in
            switch response.result {
                
            case .success:
                if let value = response.result.value as? NSDictionary {
                    if value["state"] as? Int == 101{
                        if let arr = value["governorates"] as? NSArray{
                            for data in arr{
                                
                                var id = 0
                                var name = " "
                                
                                if let object = data as? Dictionary <String, Any>{
                                    if let value = object["id"] as? Int{
                                        id = value
                                    }
                                    if let value = object["name"] as? String{
                                        name = value
                                    }
                                }
                                self.arrCity.append(services(id: id, name: name))
                            }
                            self.picker2.reloadAllComponents()
                        }
                    }else{
                        if let status = value["state"] as? Int{
                            self.otherStatus(Number: status)
                        }
                    }
                    self.stopAnimation(Lot: self.lott)
                }
            case .failure:
                self.errorInConnection()
                self.stopAnimation(Lot: self.lott)
            }
        }
    }
    
    func areaData(){
        lott = playAnimation(vc: self)

        let param = [
            "method": "areas",
            "language": "\(sharedHandler.getLanguage())",
            "governorate_id": "\(cityID)"
        ]
        Alamofire.request(urls.healthyPlusURL, method: .post, parameters: param, encoding: JSONEncoding(options: []), headers: nil).responseJSON { (response) in
            switch response.result {
                
            case .success:
                if let value = response.result.value as? NSDictionary {
                    if value["state"] as? Int == 101{
                        if let arr = value["areas"] as? NSArray{
                            for data in arr{
                                
                                var id = 0
                                var name = " "
                                
                                if let object = data as? Dictionary <String, Any>{
                                    if let value = object["id"] as? Int{
                                        id = value
                                    }
                                    if let value = object["name"] as? String{
                                        name = value
                                    }
                                }
                                self.arrArea.append(services(id: id, name: name))
                            }
                            self.picker3.reloadAllComponents()
                        }
                    }else{
                        if let status = value["state"] as? Int{
                            self.otherStatus(Number: status)
                        }
                    }
                    self.stopAnimation(Lot: self.lott)
                }
            case .failure:
                self.errorInConnection()
                self.stopAnimation(Lot: self.lott)
            }
        }
    }
}
