//
//  addImagesVC.swift
//  HealthyPlus
//
//  Created by apple on 8/6/18.
//  Copyright © 2018 Kirollos. All rights reserved.
//

import UIKit
import Photos
import BSImagePicker
import Alamofire
import Lottie
import KMPopUp

class addImagesVC: UIViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate  {
    
    var url = " "
    var count = 0
    var basicCount  = 0
    var method = " "
    var titleVC = " "
    var uploadMethod = " "
    var responseMethod = " "
    var selectedImage = UIImagePickerController()
    var arrOfImages = Array<services>()
    var arrImages = Array<UIImage>()
    
    var lott: LOTAnimationView = LOTAnimationView(name: "Boat_Loader")

    @IBOutlet weak var counterLBL: UILabel!
    @IBOutlet weak var titleLbl: UILabel!
    @IBOutlet weak var imagesCount: UILabel!
    @IBOutlet weak var colImages: UICollectionView!
    //
    
    override func viewDidLoad() {
        super.viewDidLoad()
        titleLbl.text = titleVC
        colImages.delegate = self
        colImages.dataSource = self
        counterLBL.text = "(0/\(basicCount))"
        
        
        refreshView()
        // Do any additional setup after loading the view.
    }
    override func viewDidAppear(_ animated: Bool) {
        arrOfImages.removeAll()
        images()
    }
    func refreshView(){
        NotificationCenter.default.addObserver(forName: NSNotification.Name("License"), object: nil, queue: nil) { notification in
            self.viewDidAppear(true)
            
        }
    }
    @IBAction func addImagesAction(_ sender: Any) {
        if count < basicCount{
            let status = PHPhotoLibrary.authorizationStatus()
            if (status == PHAuthorizationStatus.authorized) {
                selectedImage.delegate = self
                selectedImage.sourceType = UIImagePickerControllerSourceType.photoLibrary
                selectedImage.allowsEditing = false
                self.present(selectedImage, animated: true)
            } else if (status == PHAuthorizationStatus.denied) {
            } else if (status == PHAuthorizationStatus.notDetermined) {
                PHPhotoLibrary.requestAuthorization({ (newStatus) in
                    
                    if (newStatus == PHAuthorizationStatus.authorized) {
                        self.selectedImage.delegate = self
                        self.selectedImage.sourceType = UIImagePickerControllerSourceType.photoLibrary
                        self.selectedImage.allowsEditing = false
                        self.present(self.selectedImage, animated: true)
                    }
                })
            }
            
        }else{
            if sharedHandler.getLanguage() == "en"{
                KMPoUp.ShowMessage(controller: self, message: "You only can add \(basicCount) Picture", image: #imageLiteral(resourceName: "Attention"))
            }else{
                KMPoUp.ShowMessage(controller: self, message: "يمكنك اضافة \(basicCount) فقط", image: #imageLiteral(resourceName: "Attention"))
            }
        }
    }
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        if picker == selectedImage{
            if let originalImage = info[UIImagePickerControllerOriginalImage] as? UIImage{
                let vc = UIStoryboard.init(name: "doctor", bundle: nil).instantiateViewController(withIdentifier: "acceptionVC") as! acceptionVC
                vc.selecImage = originalImage
                vc.uploadMethod = uploadMethod
                self.addChildViewController(vc)
                vc.view.frame = self.view.frame
                self.view.addSubview(vc.view)
                
            }
            self.dismiss(animated: true, completion: nil)
        }
    }
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        self.dismiss(animated: true, completion: nil)
    }
    
    func images(){
        lott = playAnimation(vc: self)

        let param = [
            "method": "\(method)",
            "language": "\(sharedHandler.getLanguage())",
            "doctor_id": "\(sharedHandler.getUserID())"
        ]
        Alamofire.request(urls.filterInsurance, method: .post, parameters: param, encoding: JSONEncoding(options: []), headers: nil).responseJSON { (response) in
            switch response.result {
                
            case .success:
                if let value = response.result.value as? NSDictionary {
                    if value["state"] as? Int == 101{
                        if let arr = value["\(self.responseMethod)"] as? NSArray{
                            
                            for data in arr{
                                var id = 0
                                var image = " "
                                
                                if let object = data as? Dictionary <String, Any>{
                                    if let value = object["id"] as? Int{
                                        id = value
                                    }
                                    if let value = object["image"] as? String{
                                        image = self.url + value
                                    }
                                }
                                self.arrOfImages.append(services(id: id, name: image))
                            }
                            self.colImages.reloadData()
                            self.count = self.arrOfImages.count
                            self.counterLBL.text = "(\(self.count)/\(self.basicCount))"
                        }
                    }else{
                        if let status = value["state"] as? Int{
                            self.otherStatus(Number: status)
                        }
                    }
                    self.stopAnimation(Lot: self.lott)
                }
            case .failure:
                self.errorInConnection()
                self.stopAnimation(Lot: self.lott)
            }
        }
    }
}
extension addImagesVC: UICollectionViewDelegate, UICollectionViewDataSource{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return arrOfImages.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = colImages.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! imagesCell
        cell.setImage(url: arrOfImages[indexPath.row].name)
        return cell
    }
}
