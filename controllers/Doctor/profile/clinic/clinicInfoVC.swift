//
//  clinicInfoVC.swift
//  HealthyPlus
//
//  Created by apple on 8/6/18.
//  Copyright © 2018 Kirollos. All rights reserved.
//

import UIKit
import Alamofire
import Lottie

class clinicInfoVC: UIViewController {
    
    var lott: LOTAnimationView = LOTAnimationView(name: "Boat_Loader")

    @IBOutlet weak var enClinicNameTF: UITextField!
    @IBOutlet weak var arClinicNameTF: UITextField!
    @IBOutlet weak var clinicNumberTF: UITextField!
    @IBOutlet weak var reservationPriceTF: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        clinicData()
        
        customView(custom: enClinicNameTF)
        customView(custom: arClinicNameTF)
        customView(custom: clinicNumberTF)
        customView(custom: reservationPriceTF)
        
        setNavigationTitle(Title: "Clinic Info")
        
        let barButton = UIBarButtonItem(title: "Save".localized, style: .done, target: self, action: #selector(SaveBtn))
        self.navigationItem.rightBarButtonItem = barButton
    }
    
    func clinicData(){
        lott = playAnimation(vc: self)

        let param = [
            "method": "get_clinic_name_number",
            "doctor_id": "\(sharedHandler.getUserID())"
        ]
        Alamofire.request(urls.filterInsurance, method: .post, parameters: param, encoding: JSONEncoding(options: []), headers: nil).responseJSON { (response) in
            switch response.result {
                
            case .success:
                if let value = response.result.value as? NSDictionary {
                    if value["state"] as? Int == 101{
                        if let valuee = value["clinic_name_number"] as? NSDictionary{
                            if let valueee = valuee["clinic_name_en"] as? String{
                                self.enClinicNameTF.text = valueee
                            }
                            if let valueee = valuee["clinic_name_ar"] as? String{
                                self.arClinicNameTF.text = valueee
                            }
                            if let valueee = valuee["clinic_number"] as? String{
                                self.clinicNumberTF.text = valueee
                            }
                            if let valueee = valuee["price"] as? Int{
                                self.reservationPriceTF.text = "\(valueee)"
                            }
                        }
                    }
                    else{
                        if let status = value["state"] as? Int{
                            self.otherStatus(Number: status)
                        }
                    }
                    self.stopAnimation(Lot: self.lott)
                }
            case .failure:
                self.errorInConnection()
                self.stopAnimation(Lot: self.lott)
            }
        }
    }
    
    func editClinicData(){
        let param = [
            "method"        : "edit_clinic_details",
            "doctor_id"     : "\(sharedHandler.getUserID())",
            "clinic_name_en": "\(enClinicNameTF.text!)",
            "clinic_name_ar": "\(arClinicNameTF.text!)",
            "clinic_number" : "\(clinicNumberTF.text ?? "0" )",
            "price"         : "\(reservationPriceTF.text ?? "0" )"
            
        ]
        addNewInDoctor(param: param)
    }
    
    @objc func SaveBtn(){
        if sharedHandler.isValidAction(textFeilds: [enClinicNameTF, arClinicNameTF]){
            editClinicData()
        }
    }
}
