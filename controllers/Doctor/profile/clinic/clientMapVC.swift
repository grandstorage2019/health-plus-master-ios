//
//  clientMapVC.swift
//  HealthyPlus
//
//  Created by apple on 8/9/18.
//  Copyright © 2018 Kirollos. All rights reserved.
//

import UIKit
import GoogleMaps
import GooglePlaces

class clientMapVC: UIViewController, GMSMapViewDelegate {
    
    
    var lat = ""
    var lang = ""
    
    
    var resultsArray = [String]()
    let marker = GMSMarker()
    var locationManager = CLLocationManager()
    
    @IBOutlet weak var mapView: GMSMapView!
    @IBOutlet weak var searchBar: UISearchBar!
    
    override func viewDidLoad() {
        super.viewDidLoad()
//        searchBar.delegate = self
        marker.isDraggable = true
        mapView.delegate = self
        googleMap()
        locationManager.startUpdatingLocation()
        // Do any additional setup after loading the view.
        
        let barButton = UIBarButtonItem(title: "Save".localized, style: .done, target: self, action: #selector(SaveBtn))
        self.navigationItem.rightBarButtonItem = barButton
    }
    @objc func SaveBtn(){
        if lat != " " && lang != " "{
            
            let param = [
                "method":"edit_clinic_location",
                "doctor_id" : "\(sharedHandler.getUserID())",
                "lat" : "\(lat)",
                "lang" : "\(lang)"
            ]
            addNewInDoctor(param: param)
        }else{
            showAttentionMessage(msg: "Select Your Location")
        }
    }
    @IBAction func currentLocationAction(_ sender: Any) {
        mapView.isMyLocationEnabled = true
        self.locationManager.delegate = self
    }
    func googleMap(){
        if lat != "" && lang != ""{
            let camera = GMSCameraPosition.camera(withLatitude: CLLocationDegrees(lat)!, longitude: CLLocationDegrees(lang)!, zoom: 5.5)
            mapView.animate(to: camera)
            
            marker.position = CLLocationCoordinate2D(latitude: CLLocationDegrees(lat)!, longitude: CLLocationDegrees(lang)!)
            marker.map = mapView
        }else{
            mapView.isMyLocationEnabled = true
            self.locationManager.delegate = self
        }
    }
    func mapView (_ mapView: GMSMapView, didEndDragging didEndDraggingMarker: GMSMarker) {
        lat = String(marker.position.latitude)
        lang = String(marker.position.longitude)
    }
}
extension clientMapVC: CLLocationManagerDelegate {
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error){
        locationManager.stopUpdatingLocation()
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        let location = locations.last
        let camera = GMSCameraPosition.camera(withLatitude: (location?.coordinate.latitude)!, longitude: (location?.coordinate.longitude)!, zoom: 17.0)
        lat = "\((location?.coordinate.latitude)!)"
        lang = "\((location?.coordinate.longitude)!)"
        mapView.animate(to: camera)
        
        marker.position = CLLocationCoordinate2D(latitude: CLLocationDegrees(lat)!, longitude: CLLocationDegrees(lang)!)
        marker.map = mapView
        //Finally stop updating location otherwise it will come again and again in this delegate
        self.locationManager.stopUpdatingLocation()
    }
}

//extension clientMapVC: UISearchBarDelegate{
//    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
//
//        if searchText != ""{
//            searchPlaceFromGoogle(place: searchText)
//        }
//    }
//    func searchPlaceFromGoogle(place: String){
//        var googleApi = "https://maps.googleapis.com/maps/api/place/textsearch/json?query=\(place)&key=AIzaSyAxrbK_lTG5eTTN9H74_wMAWm9IUPse1Lo"
//        googleApi = googleApi.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!
//        var url = URLRequest(url: URL(string: googleApi)!)
//        url.httpMethod = "GET"
//
//        let task = URLSession.shared.dataTask(with: url) { (data, response , error) in
//            if error == nil{
//                let jsonDict = try? JSONSerialization.jsonObject(with: data!, options: .mutableContainers)
//                print(jsonDict)
//            }else{
//
//            }
//        }
//        task.resume()
//    }
//}
