//
//  profileVC.swift
//  HealthyPlus
//
//  Created by apple on 8/5/18.
//  Copyright © 2018 Kirollos. All rights reserved.
//

import UIKit
import Alamofire
import Photos
import BSImagePicker
import Lottie

class profileVC: UIViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    var specialID = 0
    var arrDocImages = [#imageLiteral(resourceName: "infinity"), #imageLiteral(resourceName: "images37"), #imageLiteral(resourceName: "plus")]
    var arrDocString = ["About Doctor".localized, "Medical Specialties".localized, "Services Dr.".localized]
    
    var arrClinicImages = [#imageLiteral(resourceName: "pen"), #imageLiteral(resourceName: "camera"), #imageLiteral(resourceName: "map"), #imageLiteral(resourceName: "home-1")]
    var arrClinicString = ["Clinic Basic Info".localized, "Clinic Pictures".localized, "Clinic Address".localized, "Insurance Company".localized]
    
    var selectedImage = UIImagePickerController()
    
    var lott: LOTAnimationView = LOTAnimationView(name: "Boat_Loader")
    
    @IBOutlet weak var docPicture: UIImageView!
    @IBOutlet weak var uploadPicture: UIButton!
    
    @IBOutlet weak var btn1: UIButton!
    @IBOutlet weak var btn2: UIButton!
    
    @IBOutlet weak var star: UILabel!
    @IBOutlet weak var docName: UILabel!
    @IBOutlet weak var reviewLbl: UILabel!
    @IBOutlet weak var reservationLbl: UILabel!
    @IBOutlet weak var specializationLbl: UILabel!
    
    @IBOutlet weak var profileTable: UITableView!
    @IBOutlet weak var segmented: UISegmentedControl!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        refreshView()
        
        profileTable.delegate   = self
        profileTable.dataSource = self
        
        circleImage(image: docPicture)
        
        setNavigationTitle(Title: "About Dr.")
        
        setBtnGradient(btn: btn1, color: UIColor.darkGray, gradColor: UIColor.lightGray)
        setBtnGradient(btn: btn2, color: UIColor.darkGray, gradColor: UIColor.lightGray)
    }
    func refreshView(){
        NotificationCenter.default.addObserver(forName: NSNotification.Name("TO_IMAGE"), object: nil, queue: nil) { notification in
            self.viewDidAppear(true)
            
        }
    }
    override func viewDidAppear(_ animated: Bool) {
        
        specializationData()
        profileTable.reloadData()
    }
    override func viewDidDisappear(_ animated: Bool) {
        stopAnimation(Lot: lott)
    }
    func specializationData(){
        lott = playAnimation(vc: self)

        let param = [
            "method": "doctor_home",
            "language": "\(sharedHandler.getLanguage())",
            "doctor_id": "\(sharedHandler.getUserID())"
        ]
        Alamofire.request(urls.filterInsurance, method: .post, parameters: param, encoding: JSONEncoding(options: []), headers: nil).responseJSON { (response) in
            switch response.result {
                
            case .success:
                if let value = response.result.value as? NSDictionary {
                    if value["state"] as? Int == 101{
                        if let value = value["stars"] as? Int{
                            self.star.text = "\(value)"
                        }
                        if let value = value["views"] as? Int{
                            self.reviewLbl.text = "\(value)"
                        }
                        if let value = value["books"] as? Int{
                            self.reservationLbl.text = "\(value)"
                        }
                        if let value = value["profile_image"] as? String{
                            if value != ""{
                                self.setImage(url: urls.profileImage + value, Image: self.docPicture)
                            }
                        }
                        if let value = value["specialization"] as? String{
                            self.specializationLbl.text = value
                        }
                        if let value = value["name"] as? String{
                            self.docName.text = value
                        }
                        if let value = value["specialization_id"] as? Int{
                            self.specialID = value
                        }
                    }
                    else{
                        if let status = value["state"] as? Int{
                            self.otherStatus(Number: status)
                        }
                    }
                    self.stopAnimation(Lot: self.lott)

                }
            case .failure:
                self.errorInConnection()
                self.stopAnimation(Lot: self.lott)

            }
        }
    }
    @IBAction func messagesAction(_ sender: Any) {
        let vc    = UIStoryboard.init(name: "doctor", bundle: nil).instantiateViewController(withIdentifier: "messagesVC") as! messagesVC
        vc.url    = urls.filterInsurance
        vc.method = "chatters"
        vc.parameterID = "doctor_id"
        
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func uploadPictureAction(_ sender: Any) {
        let status = PHPhotoLibrary.authorizationStatus()
        if (status == PHAuthorizationStatus.authorized) {
            selectedImage.delegate = self
            selectedImage.sourceType = UIImagePickerControllerSourceType.photoLibrary
            selectedImage.allowsEditing = false
            self.present(selectedImage, animated: true)
        } else if (status == PHAuthorizationStatus.denied) {
        } else if (status == PHAuthorizationStatus.notDetermined) {
            PHPhotoLibrary.requestAuthorization({ (newStatus) in
                
                if (newStatus == PHAuthorizationStatus.authorized) {
                    self.selectedImage.delegate = self
                    self.selectedImage.sourceType = UIImagePickerControllerSourceType.photoLibrary
                    self.selectedImage.allowsEditing = false
                    self.present(self.selectedImage, animated: true)
                }
            })
        }
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        if picker == selectedImage{
            if let originalImage = info[UIImagePickerControllerOriginalImage] as? UIImage{
                let vc = UIStoryboard.init(name: "doctor", bundle: nil).instantiateViewController(withIdentifier: "acceptionVC") as! acceptionVC
                vc.selecImage = originalImage
                vc.uploadMethod = "upload_profile_image"
                self.addChildViewController(vc)
                vc.view.frame = self.view.frame
                self.view.addSubview(vc.view)
//                self.docPicture.image = originalImage
            }
            self.dismiss(animated: true, completion: nil)
        }
    }
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        self.dismiss(animated: true, completion: nil)
    }
    @IBAction func segmentedAction(_ sender: Any) {
        profileTable.reloadData()
    }
}
extension profileVC: UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if segmented.selectedSegmentIndex == 0{
            return arrDocString.count
        }else{
            return arrClinicString.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = profileTable.dequeueReusableCell(withIdentifier: "cell") as! profileCell
        customView(custom: cell.cellView)
        
        if segmented.selectedSegmentIndex == 0 {
            cell.cellImage.image = arrDocImages[indexPath.row]
            cell.lblName.text    = arrDocString[indexPath.row]
        }else{
            cell.cellImage.image = arrClinicImages[indexPath.row]
            cell.lblName.text    = arrClinicString[indexPath.row]
        }
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if segmented.selectedSegmentIndex == 0{
            if indexPath.row == 0{
                let vc = UIStoryboard.init(name: "doctor", bundle: nil).instantiateViewController(withIdentifier: "aboutDocVC") as! aboutDocVC
                self.navigationController?.pushViewController(vc, animated: true)
            }else if indexPath.row == 1{
                let vc = UIStoryboard.init(name: "doctor", bundle: nil).instantiateViewController(withIdentifier: "medicalSpecialtiesVC") as! medicalSpecialtiesVC
                vc.specialID = specialID
                vc.specialName = specializationLbl.text ?? "Select Specilization".localized
                self.navigationController?.pushViewController(vc, animated: true)
            }else if indexPath.row == 2{
                let vc = UIStoryboard.init(name: "doctor", bundle: nil).instantiateViewController(withIdentifier: "servicesDocVC") as! servicesDocVC
                self.navigationController?.pushViewController(vc, animated: true)
            }
        }else{
            if indexPath.row == 0{
                let vc = UIStoryboard.init(name: "doctor", bundle: nil).instantiateViewController(withIdentifier: "clinicInfoVC") as! clinicInfoVC
                self.navigationController?.pushViewController(vc, animated: true)
            }else if indexPath.row == 1{
                let vc = UIStoryboard.init(name: "doctor", bundle: nil).instantiateViewController(withIdentifier: "addImagesVC") as! addImagesVC
                vc.basicCount = 10
                vc.url = urls.clinicImages
                vc.titleVC = "Clinic Pictures".localized
                vc.method = "clinic_pictures"
                vc.uploadMethod = "upload_clinic_picture"
                vc.responseMethod = "clinic_pictures"
                self.navigationController?.pushViewController(vc, animated: true)
            }else if indexPath.row == 3{
                let vc = UIStoryboard.init(name: "doctor", bundle: nil).instantiateViewController(withIdentifier: "servicesDocVC") as! servicesDocVC
                vc.isInsurance = true
                self.navigationController?.pushViewController(vc, animated: true)
            }else if indexPath.row == 2{
                let vc = UIStoryboard.init(name: "doctor", bundle: nil).instantiateViewController(withIdentifier: "clinicAddressVC") as! clinicAddressVC
                self.navigationController?.pushViewController(vc, animated: true)
            }
        }
    }
}
