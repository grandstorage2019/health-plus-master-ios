//
//  servicesDocVC.swift
//  HealthyPlus
//
//  Created by apple on 8/5/18.
//  Copyright © 2018 Kirollos. All rights reserved.
//

import UIKit
import Alamofire
import Lottie

class servicesDocVC: UIViewController {
    
    var isInsurance = false
    var arrSelect = Array<Int>()
    var arrServices = Array<servicesChecked>()
    
    var lott: LOTAnimationView = LOTAnimationView(name: "Boat_Loader")

    @IBOutlet weak var servicesTbl: UITableView!
    @IBOutlet weak var addBtn: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        servicesTbl.delegate = self
        servicesTbl.dataSource = self
    }
    override func viewDidAppear(_ animated: Bool) {
        if isInsurance{
            addBtn.alpha = 0
            setNavigationTitle(Title: "Insurance Company")
            ServicesData(method: "insurance_companies")
            let barButton = UIBarButtonItem(title: "Save".localized, style: .done, target: self, action: #selector(SaveBtn))
            self.navigationItem.rightBarButtonItem = barButton
        }else{
            setNavigationTitle(Title: "Services Dr.")
            ServicesData(method: "services")
            let barButton = UIBarButtonItem(title: "Save".localized, style: .done, target: self, action: #selector(SaveBtn))
            self.navigationItem.rightBarButtonItem = barButton
        }
    }
    @IBAction func addAction(_ sender: Any) {
        let vc = UIStoryboard.init(name: "doctor", bundle: nil).instantiateViewController(withIdentifier: "newServiceVC") as! newServiceVC
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @objc func SaveBtn(){
        var service = ""
        if arrSelect.count > 0{
            
            for i in 0..<arrSelect.count{
                service = service + "\(arrSelect[i])"
                if i + 1 != arrSelect.count{
                    service = service + ","
                }
            }
        }
        var param: Parameters = ["": ""]
        if isInsurance{
            param = [
                "method": "edit_insurance_companies",
                "insurance_companies": "\(service)",
                "doctor_id": "\(sharedHandler.getUserID())"
            ]
            print(service)
        }else{
            param = [
                "method": "edit_services",
                "services": "\(service)",
                "doctor_id": "\(sharedHandler.getUserID())"
            ]
        }
        addNewInDoctor(param: param)
    }
    func ServicesData(method: String){
        lott = playAnimation(vc: self)

        let param = [
            "method": "\(method)",
            "language": "\(sharedHandler.getLanguage())",
            "doctor_id": "\(sharedHandler.getUserID())"
        ]
        Alamofire.request(urls.filterInsurance, method: .post, parameters: param, encoding: JSONEncoding(options: []), headers: nil).responseJSON { (response) in
            switch response.result {
                
            case .success:
                if let value = response.result.value as? NSDictionary {
                    if value["state"] as? Int == 101{
                        if let arr = value["\(method)"] as? NSArray{
                            
                            for data in arr{
                                var id = 0
                                var name = " "
                                var check = false
                                
                                if let object = data as? Dictionary <String, Any>{
                                    if let value = object["id"] as? Int{
                                        id = value
                                    }
                                    if let value = object["name"] as? String{
                                        name = value
                                    }
                                    if let valuee = object["checked"] as? Bool{
                                        check = valuee
                                    }
                                }
                                if check{
                                    self.arrSelect.append(id)
                                }
                                self.arrServices.append(servicesChecked(id: id, name: name, check: check))
                            }
                            self.servicesTbl.reloadData()
                        }
                    }
                    else{
                        if let status = value["state"] as? Int{
                            self.otherStatus(Number: status)
                        }
                    }
                    self.stopAnimation(Lot: self.lott)

                }
                
            case .failure:
                self.errorInConnection()
                self.stopAnimation(Lot: self.lott)

            }
        }
    }
}
extension servicesDocVC: UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrServices.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = servicesTbl.dequeueReusableCell(withIdentifier: "cell") as! servicesDocCell
        cell.serviceName.text = arrServices[indexPath.row].name
        cell.selectBtn.tag = indexPath.row
        cell.selectBtn.addTarget(self, action: #selector(Selecting(sender:)), for: .touchUpInside)
        if arrServices[indexPath.row].check{
            cell.selectBtn.setImage(#imageLiteral(resourceName: "select"), for: .normal)
        }
        return cell
    }
    @objc func Selecting(sender: UIButton){
        let data = arrServices[sender.tag]
        if data.check == false{
            arrServices[sender.tag].check = true
            arrSelect.append(data.id)
            sender.setImage(#imageLiteral(resourceName: "select"), for: .normal)
        }else{
            arrServices[sender.tag].check = false
            sender.setImage(#imageLiteral(resourceName: "unSelect"), for: .normal)
            for i in 0..<arrSelect.count{
                if arrSelect[i] == data.id{
                    arrSelect.remove(at: i)
                    break
                }else{
                    
                }
            }
        }
    }
}
