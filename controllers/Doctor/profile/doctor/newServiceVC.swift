//
//  newServiceVC.swift
//  HealthyPlus
//
//  Created by apple on 8/5/18.
//  Copyright © 2018 Kirollos. All rights reserved.
//

import UIKit

class newServiceVC: UIViewController {
    
    
    @IBOutlet weak var view1: UIView!
    @IBOutlet weak var view2: UIView!
    @IBOutlet weak var enServiceName: UITextField!
    @IBOutlet weak var arServiceName: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        customView(custom: view1)
        customView(custom: view2)
        
        setNavigationTitle(Title: "New Service")
        
        let barButton = UIBarButtonItem(title: "Save".localized, style: .done, target: self, action: #selector(SaveBtn))
        self.navigationItem.rightBarButtonItem = barButton
    }
    @objc func SaveBtn(){
        if sharedHandler.isValidAction(textFeilds: [enServiceName, arServiceName]){
            let param = [
                "method": "add_service",
                "name_en": "\(enServiceName.text!)",
                "name_ar": "\(arServiceName.text!)"
            ]
            addNewInDoctor(param: param)
        }
    }
}
