//
//  medicalSpecialtiesVC.swift
//  HealthyPlus
//
//  Created by apple on 8/6/18.
//  Copyright © 2018 Kirollos. All rights reserved.
//

import UIKit
import Alamofire
import Lottie

class medicalSpecialtiesVC: UIViewController {
    
    var specialID = 0
    var specialName = " "
    var picker = UIPickerView()
    var arrSelect = Array<Int>()
    var arrServices = Array<services>()
    var arrSubServices = Array<servicesChecked>()
    
    var lott: LOTAnimationView = LOTAnimationView(name: "Boat_Loader")

    @IBOutlet weak var specilizationTF: UITextField!
    @IBOutlet weak var tblHeight: NSLayoutConstraint!
    @IBOutlet weak var specilizationTBL: UITableView!
    
    @IBOutlet weak var view1: UIView!
    @IBOutlet weak var view2: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        customView(custom: view1)
        customView(custom: view2)
        
        picker.delegate = self
        picker.dataSource = self
        
        specilizationTBL.delegate = self
        specilizationTBL.dataSource = self
        
        specilizationTF.inputView = picker
        
        specilizationTF.text = specialName
        
        
        setNavigationTitle(Title: "Medical Specialities")
        let barButton = UIBarButtonItem(title: "Save".localized, style: .done, target: self, action: #selector(SaveBtn))
        self.navigationItem.rightBarButtonItem = barButton
    }
    override func viewDidAppear(_ animated: Bool) {
        specializationData(url: urls.filterInsurance)
        subSpecializationData(url: urls.filterInsurance)
    }
    func specializationData(url: String){
        lott = playAnimation(vc: self)

        let param = [
            "method": "specializations",
            "language": "\(sharedHandler.getLanguage())"
        ]
        Alamofire.request(url, method: .post, parameters: param, encoding: JSONEncoding(options: []), headers: nil).responseJSON { (response) in
            switch response.result {
                
            case .success:
                if let value = response.result.value as? NSDictionary {
                    if value["state"] as? Int == 101{
                        if let arr = value["specializations"] as? NSArray{
                            for data in arr{
                                
                                var id = 0
                                var name = " "
                                
                                if let object = data as? Dictionary <String, Any>{
                                    if let value = object["id"] as? Int{
                                        id = value
                                    }
                                    if let value = object["name"] as? String{
                                        name = value
                                    }
                                }
                                self.arrServices.append(services(id: id, name: name))
                            }
                            self.picker.reloadAllComponents()
                        }
                    }
                    else{
                        if let status = value["state"] as? Int{
                            self.otherStatus(Number: status)
                        }
                    }

                }
            case .failure:
                self.errorInConnection()

            }
        }
    }
    
    @objc func SaveBtn(){
        if sharedHandler.isValidAction(textFeilds: [specilizationTF]) == true && arrSelect.count > 0 && specialID != 0{
            
            let param = [
                "method": "edit_specializations",
                "doctor_id": "\(sharedHandler.getUserID())",
                "specializations": "\(arrSelect[0])",
                "specialization_id": "\(specialID)"
            ]
            addNewInDoctor(param: param)
        }
    }
    
    func subSpecializationData(url: String){

        let param = [
            "method": "sub_sepcializations",
            "language": "\(sharedHandler.getLanguage())",
            "doctor_id": "\(sharedHandler.getUserID())",
            "specialization_id": "\(specialID)"
        ]
        arrSubServices.removeAll()
        Alamofire.request(url, method: .post, parameters: param, encoding: JSONEncoding(options: []), headers: nil).responseJSON { (response) in
            switch response.result {
                
            case .success:
                if let value = response.result.value as? NSDictionary {
                    if value["state"] as? Int == 101{
                        if let arr = value["sub_sepcializations"] as? NSArray{
                            for data in arr{
                                
                                var id = 0
                                var name = " "
                                var check = false
                                
                                if let object = data as? Dictionary <String, Any>{
                                    if let value = object["id"] as? Int{
                                        id = value
                                    }
                                    if let value = object["name"] as? String{
                                        name = value
                                    }
                                    if let valuee = object["checked"] as? Bool{
                                        check = valuee
                                    }
                                    if check{
                                        self.arrSelect.append(id)
                                    }
                                }
                                self.arrSubServices.append(servicesChecked(id: id, name: name, check: check))
                            }
                            if self.arrSubServices.count > 0{
                                self.specilizationTBL.reloadData()
                                self.tblHeight.constant = CGFloat(40) * CGFloat(self.arrSubServices.count)
                            }else{
                                self.tblHeight.constant = CGFloat(0)
                            }
                        }
                    }
                    else{
                        if let status = value["state"] as? Int{
                            self.otherStatus(Number: status)
                        }
                    }
                    self.specilizationTBL.reloadData()
                    self.stopAnimation(Lot: self.lott)

                }
            case .failure:
                self.errorInConnection()
                self.stopAnimation(Lot: self.lott)

            }
        }
    }
}
extension medicalSpecialtiesVC: UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrSubServices.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = specilizationTBL.dequeueReusableCell(withIdentifier: "cell") as! servicesDocCell
        cell.serviceName.text = arrSubServices[indexPath.row].name
        cell.selectBtn.tag = indexPath.row
        cell.selectBtn.addTarget(self, action: #selector(Selecting(sender:)), for: .touchUpInside)
        if arrSubServices[indexPath.row].check{
            cell.selectBtn.setImage(#imageLiteral(resourceName: "select"), for: .normal)
        }
        return cell
    }
    @objc func Selecting(sender: UIButton){
        let data = arrSubServices[sender.tag]
        if data.check == false{
            arrSubServices[sender.tag].check = true
            arrSelect.append(data.id)
            sender.setImage(#imageLiteral(resourceName: "select"), for: .normal)
        }else{
            arrSubServices[sender.tag].check = false
            sender.setImage(#imageLiteral(resourceName: "unSelect"), for: .normal)
            for i in 0..<arrSelect.count{
                if arrSelect[i] == data.id{
                    arrSelect.remove(at: i)
                    break
                }else{
                    
                }
            }
        }
    }
}
extension medicalSpecialtiesVC: UIPickerViewDelegate, UIPickerViewDataSource{
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return arrServices.count
    }
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return arrServices[row].name
    }
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        if arrServices.count > 0{
            specialID            = arrServices[row].id
            specilizationTF.text = arrServices[row].name
            lott = playAnimation(vc: self)
            subSpecializationData(url: urls.filterInsurance)
        }
    }
}
