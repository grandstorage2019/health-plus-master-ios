//
//  aboutDocVC.swift
//  HealthyPlus
//
//  Created by apple on 8/6/18.
//  Copyright © 2018 Kirollos. All rights reserved.
//

import UIKit
import Alamofire
import Lottie
import MOLH

class aboutDocVC: UIViewController {
    
    var date = " "
    var gender = "male".localized
    var enCount = 0
    var arCount = 0
    var birthPicker = UIDatePicker()
    var lott: LOTAnimationView = LOTAnimationView(name: "Boat_Loader")

    @IBOutlet weak var licenseImage: UIImageView!
    @IBOutlet weak var personImage: UIImageView!
    @IBOutlet weak var nameTF: UILabel!
    @IBOutlet weak var specilNameTF: UILabel!
    
    @IBOutlet weak var enAboutDoctorTF1: UITextView!
    @IBOutlet weak var arAboutDoctorTF2: UITextView!
    @IBOutlet weak var enTitleTF3: UITextField!
    @IBOutlet weak var arTitleTF4: UITextField!
    @IBOutlet weak var enFirstNameTF5: UITextField!
    @IBOutlet weak var arFirstNameTF6: UITextField!
    @IBOutlet weak var enLastNameTf7: UITextField!
    @IBOutlet weak var arLastNameTF8: UITextField!
    @IBOutlet weak var setDateTF9: UITextField!
    @IBOutlet weak var enSpecializationTF10: UITextField!
    @IBOutlet weak var arSpecializationTF11: UITextField!
    @IBOutlet weak var enHeight: NSLayoutConstraint!
    @IBOutlet weak var arHeight: NSLayoutConstraint!
    
    @IBOutlet weak var maleBtn: UIButton!
    @IBOutlet weak var femaleBtn: UIButton!
    
    
    @IBOutlet weak var lbl1: UILabel!
    @IBOutlet weak var lbl2: UILabel!
    @IBOutlet weak var lbl3: UILabel!
    @IBOutlet weak var lbl4: UILabel!
    @IBOutlet weak var lbl5: UILabel!
    @IBOutlet weak var lbl6: UILabel!
    @IBOutlet weak var lbl7: UILabel!
    @IBOutlet weak var lbl8: UILabel!
    @IBOutlet weak var lbl9: UILabel!
    @IBOutlet weak var lbl10: UILabel!
    @IBOutlet weak var lbl11: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        aboutDoc()
        circleImage(image: personImage)
        nameTF.text = sharedHandler.getUserName()
        
        setDateTF9.inputView = birthPicker
        showDatePicker()
        updateBorders()
        setNavigationTitle(Title: "About Dr.")
        
        let barButton = UIBarButtonItem(title: "Save".localized, style: .done, target: self, action: #selector(SaveBtn))
        self.navigationItem.rightBarButtonItem = barButton
    }
    
    @objc func SaveBtn(){
        if sharedHandler.isValidAction(textFeilds: [ enTitleTF3, arTitleTF4, enFirstNameTF5, arFirstNameTF6, enLastNameTf7, arLastNameTF8, setDateTF9, enSpecializationTF10, arSpecializationTF11]) && enAboutDoctorTF1.text.count > 0 && arAboutDoctorTF2.text.count > 0{
            
            let param = [
                "method"   : "edit_doctor_info",
                "about_en" : "\(enAboutDoctorTF1.text!)",
                "about_ar" : "\(arAboutDoctorTF2.text!)",
                "fname_en" : "\(enFirstNameTF5.text!)",
                "lname_en" : "\(enLastNameTf7.text!)",
                "fname_ar" : "\(arFirstNameTF6.text!)",
                "lname_ar" : "\(arLastNameTF8.text!)",
                "birth_day": "\(date)",
                "title_en" : "\(enTitleTF3.text!)",
                "title_ar" : "\(arTitleTF4.text!)",
                "level_ar" : "\(arSpecializationTF11.text!)",
                "level_en" : "\(enSpecializationTF10.text!)",
                "gender"   : "\(gender)",
                "id"       : "\(sharedHandler.getUserID())"
            ]
            print(date)
            addNewInDoctor(param: param)
        }else{
            showAttentionMessage(msg: "Please, Enter All Fields")
        }
    }
    
    @IBAction func licenseAction(_ sender: Any) {
        let vc = UIStoryboard.init(name: "doctor", bundle: nil).instantiateViewController(withIdentifier: "addImagesVC") as! addImagesVC
        vc.basicCount = 5
        vc.titleVC = "License Picture".localized
        vc.method = "licence_pictures"
        vc.uploadMethod = "upload_licence"
        vc.url = urls.licensePicture
        vc.responseMethod = "clinic_pictures"
        self.navigationController?.pushViewController(vc, animated: true)
    }
    func aboutDoc(){
        lott = playAnimation(vc: self)

        let param = [
            "method": "about_doctor",
            "language": "\(sharedHandler.getLanguage())",
            "doctor_id": "\(sharedHandler.getUserID())"
        ]
        Alamofire.request(urls.filterInsurance, method: .post, parameters: param, encoding: JSONEncoding(options: []), headers: nil).responseJSON { (response) in
            switch response.result {
                
            case .success:
                if let value = response.result.value as? NSDictionary {
                    if value["state"] as? Int == 101{
                        if let valuee = value["about_doctor"] as? NSDictionary{
                            
                            var enFullName = ""
                            var arFullName = ""
                            
                            if let value = valuee["specialization"] as? String{
                                self.specilNameTF.text = "\(value)"
                            }
                            if let value = valuee["about_ar"] as? String{
                                self.arAboutDoctorTF2.text = "\(value)"
                                self.arCount = value.count
                                self.firstExpandTextField(textView: self.arAboutDoctorTF2)
                                self.lbl2.text = "عن الطبيب" + " (\(500 - self.arCount)/500)"
                            }
                            if let value = valuee["about_en"] as? String{
                                self.enAboutDoctorTF1.text = "\(value)"
                                self.enCount = value.count
                                self.firstExpandTextField(textView: self.enAboutDoctorTF1)
                                self.lbl1.text = "About Doctor" + " (\(500 - self.enCount)/500)"
                            }
                            if let value = valuee["title_en"] as? String{
                                self.enSpecializationTF10.text = "\(value)"
                            }
                            if let value = valuee["title_ar"] as? String{
                                self.arSpecializationTF11.text = "\(value)"
                            }
                            if let value = valuee["fname_en"] as? String{
                                self.enFirstNameTF5.text = "\(value)"
                                enFullName = value
                            }
                            if let value = valuee["fname_ar"] as? String{
                                self.arFirstNameTF6.text = "\(value)"
                                arFullName = value
                            }
                            if let value = valuee["lname_en"] as? String{
                                self.enLastNameTf7.text = "\(value)"
                                enFullName = enFullName + " " + value
                            }
                            if let value = valuee["lname_ar"] as? String{
                                self.arLastNameTF8.text = "\(value)"
                                arFullName = arFullName + " " + value
                            }
                            if let value = valuee["level_ar"] as? String{
                                self.arTitleTF4.text = "\(value)"
                            }
                            if let value = valuee["level_en"] as? String{
                                self.enTitleTF3.text = "\(value)"
                            }
                            if let value = valuee["birth_day"] as? String{
                                let Date = value.split(separator: " ")
                                self.date = String(Date[0])
                                self.setDateTF9.text = "\(Date[0])"
                            }
                            if let value = valuee["lname_en"] as? String{
                                self.enLastNameTf7.text = "\(value)"
                            }
                            
                            if let value = valuee["gender"] as? String{
                                if value == "female" || value == "Female" || value == "انثي" || value == "أنثي"{
                                    self.femaleBtn.setImage(#imageLiteral(resourceName: "select"), for: .normal)
                                }else{
                                    self.maleBtn.setImage(#imageLiteral(resourceName: "select"), for: .normal)
                                }
                                self.gender = value
                            }
                            if let value = valuee["license"] as? String{
                                self.setImage(url: urls.licensePicture + value, Image: self.licenseImage)
                            }
                            if let value = valuee["profile_image"] as? String{
                                self.setImage(url: urls.profileImage + value, Image: self.personImage)
                            }
                            if MOLHLanguage.currentAppleLanguage() == "en"{
                                self.nameTF.text = enFullName
                            }else{
                                self.nameTF.text = arFullName
                            }
                        }
                    }
                    else{
                        if let status = value["state"] as? Int{
                            self.otherStatus(Number: status)
                        }
                    }
                    self.stopAnimation(Lot: self.lott)

                }
            case .failure:
                self.errorInConnection()
                self.stopAnimation(Lot: self.lott)

            }
        }
    }
    
    @IBAction func genderAction(_ sender: UIButton) {
        if sender.tag == 1{
            maleBtn.setImage(#imageLiteral(resourceName: "select"), for: .normal)
            femaleBtn.setImage(#imageLiteral(resourceName: "unSelect"), for: .normal)
            gender = "male".localized
        }else{
            femaleBtn.setImage(#imageLiteral(resourceName: "select"), for: .normal)
            maleBtn.setImage(#imageLiteral(resourceName: "unSelect"), for: .normal)
            gender = "female".localized
        }
    }
    
    func showDatePicker(){
        birthPicker.datePickerMode = .date
        
        let toolbar = UIToolbar();
        toolbar.sizeToFit()
        
        //done button & cancel button
        let doneButton = UIBarButtonItem(title: "Done", style: .done, target: self, action: #selector(donedatePicker))
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: "Cancel", style: .done, target: self, action: #selector(cancelDatePicker))
        toolbar.setItems([doneButton,spaceButton,cancelButton], animated: false)
        
        setDateTF9.inputAccessoryView = toolbar
        
    }
    @objc func donedatePicker(){
        //For date formate
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd"
        setDateTF9.text = formatter.string(from: birthPicker.date)
        date = setDateTF9.text!
        self.view.endEditing(true)
    }
    @objc func cancelDatePicker(){
        self.view.endEditing(true)
    }

    func updateBorders(){
        enAboutDoctorTF1.layer.borderColor = UIColor.lightGray.cgColor
        enAboutDoctorTF1.layer.borderWidth = 0.75
        enAboutDoctorTF1.layer.cornerRadius = 5
        
        arAboutDoctorTF2.layer.borderColor = UIColor.lightGray.cgColor
        arAboutDoctorTF2.layer.borderWidth = 0.75
        arAboutDoctorTF2.layer.cornerRadius = 5
    }
}

extension aboutDocVC: UITextFieldDelegate{
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField == enAboutDoctorTF1{
            showLabel(label: lbl1)
        }else if textField == arAboutDoctorTF2{
            showLabel(label: lbl2)
            //            lbl2.text = "(\(textField.text!.count)/500)"
        }else if textField == enTitleTF3{
            showLabel(label: lbl3)
        }else if textField == arTitleTF4{
            showLabel(label: lbl4)
        }else if textField == enFirstNameTF5{
            showLabel(label: lbl5)
        }else if textField == arFirstNameTF6{
            showLabel(label: lbl6)
        }else if textField == enLastNameTf7{
            showLabel(label: lbl7)
        }else if textField == arLastNameTF8{
            showLabel(label: lbl8)
        }else if textField == setDateTF9{
            showLabel(label: lbl9)
        }else if textField == enSpecializationTF10{
            showLabel(label: lbl10)
        }else if textField == arSpecializationTF11{
            showLabel(label: lbl11)
        }
    }
    func textFieldDidEndEditing(_ textField: UITextField) {
        
        if textField == enAboutDoctorTF1         {
            grayLabel(label: lbl1)
        }else if textField == arAboutDoctorTF2      {
            grayLabel(label: lbl2)
        }else if textField == enTitleTF3      {
            grayLabel(label: lbl3)
        }else if textField == arTitleTF4    {
            grayLabel(label: lbl4)
        }else if textField == enFirstNameTF5  {
            grayLabel(label: lbl5)
        }else if textField == arFirstNameTF6   {
            grayLabel(label: lbl6)
        }else if textField == enLastNameTf7   {
            grayLabel(label: lbl7)
        }else if textField == arLastNameTF8{
            grayLabel(label: lbl8)
        }else if textField == setDateTF9{
            grayLabel(label: lbl9)
        }else if textField == enSpecializationTF10  {
            grayLabel(label: lbl10)
        }else if textField == arSpecializationTF11  {
            grayLabel(label: lbl11)
        }
    }
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if textField == enAboutDoctorTF1{
            if range.location <= 499{
                
                if string != ""{
                    enCount += 1
                }else{
                    enCount -= 1
                }
                if range.location == 0 && enCount > 3{
                    self.lbl1.text = "About Doctor (500/500)"
                    enCount = 0
                }else{
                    self.lbl1.text = "About Doctor (\(500 - enCount)/500)"
                }
                return true
            }else{
                return false
            }
        }else if textField == arAboutDoctorTF2{
            if range.location <= 499{
                
                if string != ""{
                    arCount += 1
                }else{
                    arCount -= 1
                }
                if range.location == 0 && arCount > 3{
                    self.lbl2.text = "عن الطبيب (500/500)"
                    arCount = 0
                }else{
                    self.lbl2.text = "عن الطبيب(\(500 - arCount)/500)"
                }
                return true
            }else{
                return false
            }
        }else {
            return true
        }
    }
    func showLabel(label: UILabel){
        UIView.animate(withDuration: 0.5, animations: {
            label.textColor = colors.appColor
        })
    }
    func grayLabel(label: UILabel) {
        UIView.animate(withDuration: 0.5, animations: {
            label.textColor = UIColor.lightGray
        })
    }
}

extension aboutDocVC: UITextViewDelegate{
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView == enAboutDoctorTF1{
            if textView.text == "About Doctor" {
                textView.text = ""
                textView.textColor = .black
            }else{
                textView.textColor = .black
            }
        }else if textView == arAboutDoctorTF2{
            if textView.text == "عن الطبيب" {
                textView.text = ""
                textView.textColor = .black
            }else{
                textView.textColor = .black
            }
        }
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView == enAboutDoctorTF1{
            if textView.text == ""{
                textView.text = "About Doctor"
                textView.textColor = .lightGray
            }
        }else if textView == arAboutDoctorTF2{
            if textView.text == ""{
                textView.text = "عن الطبيب"
                textView.textColor = .lightGray
            }
        }
    }
    
    func textViewDidChange(_ textView: UITextView) {
        if textView.contentSize.height != textView.frame.size.height {
            
            let fixedWidth = textView.frame.size.width
            textView.sizeThatFits(CGSize(width: fixedWidth, height: CGFloat.greatestFiniteMagnitude))
            
            var newFrame = textView.frame
            let newSize = textView.sizeThatFits(CGSize(width: fixedWidth, height: CGFloat.greatestFiniteMagnitude))
            
            
            newFrame.size = CGSize(width: max(newSize.width, fixedWidth), height: newSize.height)
            
            textView.frame = newFrame;
            if textView == enAboutDoctorTF1{
                enHeight.constant = CGFloat(newFrame.size.height)
            }else if textView == arAboutDoctorTF2{
                arHeight.constant = CGFloat(newFrame.size.height)
            }
        }
    }
    func firstExpandTextField(textView: UITextView){
        if textView.contentSize.height != textView.frame.size.height {
            
            let fixedWidth = textView.frame.size.width
            textView.sizeThatFits(CGSize(width: fixedWidth, height: CGFloat.greatestFiniteMagnitude))
            
            var newFrame = textView.frame
            let newSize = textView.sizeThatFits(CGSize(width: fixedWidth, height: CGFloat.greatestFiniteMagnitude))
            
            
            newFrame.size = CGSize(width: max(newSize.width, fixedWidth), height: newSize.height)
            
            textView.frame = newFrame;
            if textView == enAboutDoctorTF1{
                enHeight.constant = CGFloat(newFrame.size.height)
            }else if textView == arAboutDoctorTF2{
                arHeight.constant = CGFloat(newFrame.size.height)
            }
        }
    }
}
