//
//  singlePatientVC.swift
//  HealthyPlus
//
//  Created by apple on 8/8/18.
//  Copyright © 2018 Kirollos. All rights reserved.
//

import UIKit

class singlePatientVC: UIViewController {
    
    var patientObj = Patient(id: 0, doctorID: 0, countryId: 0, gender: " ", phone: " ", name: " ", email: " ", picture: " ", notes: " ", insuranceId: " ", insuranceCompany: " ")
    
    @IBOutlet weak var patientView: UIView!
    @IBOutlet weak var patientNameLbl: UILabel!
    @IBOutlet weak var patientIDLbl: UILabel!
    @IBOutlet weak var genderLbl: UILabel!
    @IBOutlet weak var notesLbl: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        patientNameLbl.text = patientObj.name
        patientIDLbl.text   = "\(patientObj.phone)"
        genderLbl.text      = patientObj.gender
        notesLbl.text       = patientObj.notes
        
        customView(custom: patientView)
        
        setNavigationTitle(Title: "Patient Info.")
    }
    
    @IBAction func deletePatientAction(_ sender: Any) {
        let vc = UIStoryboard.init(name: "doctor", bundle: nil).instantiateViewController(withIdentifier: "addNewPatientFromResevationVC") as! addNewPatientFromResevationVC
        vc.ID   = String(patientObj.id)
        vc.Name = patientObj.name
        vc.isDelete = true
        self.addChildViewController(vc)
        vc.view.frame = self.view.frame
        self.view.addSubview(vc.view)
        vc.didMove(toParentViewController: self)
    }
    
    @IBAction func doctorTimeAction(_ sender: Any) {
        let vc = UIStoryboard.init(name: "doctor", bundle: nil).instantiateViewController(withIdentifier: "doctorTimesVC") as! doctorTimesVC
        vc.patientID = patientObj.id
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func callAction(_ sender: Any) {
        if let url = URL(string: "tel://\(patientObj.phone)"), UIApplication.shared.canOpenURL(url) {
            if #available(iOS 10, *) {
                UIApplication.shared.open(url)
            } else {
                UIApplication.shared.openURL(url)
            }
        }
    }
    
    @IBAction func chatAction(_ sender: Any) {
        let vc = UIStoryboard.init(name: "doctor", bundle: nil).instantiateViewController(withIdentifier: "chatVC") as! chatVC
        
        vc.method = "chat"
        vc.url = urls.filterInsurance
        vc.recivereImage = patientObj.image
        vc.patientID = patientObj.id
        vc.doctorID = sharedHandler.getUserID()
        vc.who = "doctor"
        vc.sendMsgUrl = urls.healthyPlusURL
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
}
