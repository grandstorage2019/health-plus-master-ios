//
//  doctorTimesVC.swift
//  HealthyPlus
//
//  Created by apple on 9/11/18.
//  Copyright © 2018 Kirollos. All rights reserved.
//

import UIKit
import Lottie
import Alamofire

class doctorTimesVC: UIViewController {

    var patientID = 0
    var arrTimes = Array<Times>()
    var lott: LOTAnimationView = LOTAnimationView(name: "Boat_Loader")
    
    @IBOutlet weak var colBooking: UICollectionView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        FavouriteData(url: urls.healthyPlusURL)
        colBooking.delegate = self
        colBooking.dataSource = self
        
        // Do any additional setup after loading the view.
    }
    func FavouriteData(url: String){
        lott = playAnimation(vc: self)
        let param = [
            "method": "doctor_info",
            "language": "\(sharedHandler.getLanguage())",
            "doctor_id": "\(sharedHandler.getUserID())"
        ]
        Alamofire.request(url, method: .post, parameters: param, encoding: JSONEncoding(options: []), headers: nil).responseJSON { (response) in
            switch response.result {
                
            case .success:
                if let value = response.result.value as? NSDictionary {
                    if value["state"] as? Int == 101{
                        if let arr = value["times"] as? NSArray{
                            for data in arr{
                                
                                var day = " "
                                var from_time = " "
                                var to_time = " "
                                var waiting_time = " "
                                var date = " "
                                var time_type = " "
                                
                                if let object = data as? Dictionary <String, Any>{
                                    if let value = object["day"] as? String{
                                        day = value
                                    }
                                    if let value = object["from_time"] as? String{
                                        from_time = value
                                    }
                                    if let value = object["to_time"] as? String{
                                        to_time = value
                                    }
                                    if let value = object["waiting_time"] as? String{
                                        waiting_time = value
                                    }
                                    if let value = object["date"] as? String{
                                        date = value
                                    }
                                    if let value = object["time_type"] as? String{
                                        time_type = value
                                    }
                                }
                                self.arrTimes.append(Times(day: day, from_time: from_time, to_time: to_time, waiting_time: waiting_time, date: date, time_type: time_type))
                            }
                        }
                        if self.arrTimes.count > 0{
                            self.colBooking.reloadData()
                        }else{
//                            self.bookingViewHeight.constant = 0.0
//                            self.leftArrow.alpha = 0
//                            self.rightArrow.alpha = 0
                        }
                    }else{
                        if let status = value["state"] as? Int{
                            self.otherStatus(Number: status)
                        }
                    }
                    self.stopAnimation(Lot: self.lott)
                }
            case .failure:
                self.errorInConnection()
                self.stopAnimation(Lot: self.lott)
            }
        }
    }

}
extension doctorTimesVC: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return arrTimes.count
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width = (colBooking.layer.frame.size.width - 10)/2
        return CGSize(width: width, height: 200.0 )
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = colBooking.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! bookCell
        cell.settingdata(Day: arrTimes[indexPath.row].day, Date: arrTimes[indexPath.row].date, From: arrTimes[indexPath.row].from_time, To: arrTimes[indexPath.row].to_time)
        return cell
        
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if collectionView == colBooking{
            if arrTimes[indexPath.row].waiting_time != " "{
                let vc = UIStoryboard.init(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "bookingTimeVC") as! bookingTimeVC
                vc.isDoctor = true
                vc.doctorID = sharedHandler.getUserID()
                vc.patientID = patientID
                vc.times = arrTimes[indexPath.row]
                
                self.navigationController?.pushViewController(vc, animated: true)
            }
            else{
                let vc = UIStoryboard.init(name: "doctor", bundle: nil).instantiateViewController(withIdentifier: "addNewPatientFromResevationVC") as! addNewPatientFromResevationVC
                vc.ID   = String(patientID)
                vc.Name = " "
                vc.datee = arrTimes[indexPath.row].date
                vc.isDoctor = true
                vc.time = "Book First"
                vc.type = "Book First"
                vc.patientId = patientID
                self.addChildViewController(vc)
                vc.view.frame = self.view.frame
                self.view.addSubview(vc.view)
                vc.didMove(toParentViewController: self)
            }
        }
    }
}
