//
//  patientVC.swift
//  HealthyPlus
//
//  Created by apple on 8/5/18.
//  Copyright © 2018 Kirollos. All rights reserved.
//

import UIKit
import Alamofire
import Lottie

class patientVC: UIViewController {
    
    var arrPatient = Array<Patient>()
    var lott: LOTAnimationView = LOTAnimationView(name: "Boat_Loader")

    @IBOutlet weak var dashedView: UIView!
    @IBOutlet weak var noPatientView: UIView!
    
    @IBOutlet weak var patientTbl: UITableView!
    @IBOutlet weak var addPatientOutlet: UIButton!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        patientTbl.delegate = self
        patientTbl.dataSource = self
        
        setNavigationTitle(Title: "Patients")
        dashedView(dashView: dashedView)
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        patientTbl.alpha = 0
        addPatientOutlet.alpha = 0
        dashedView.alpha = 1
    }
    override func viewDidAppear(_ animated: Bool) {
        arrPatient.removeAll()
        patientData()
    }
    @IBAction func addPatientAction(_ sender: Any) {
        let vc = UIStoryboard.init(name: "doctor", bundle: nil).instantiateViewController(withIdentifier: "addPatientVC") as! addPatientVC
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func patientData(){
        lott = playAnimation(vc: self)

        let param = [
            "method": "doctor_patients",
            "language": "\(sharedHandler.getLanguage())",
            "added_by_doctor_id": "\(sharedHandler.getUserID())"
        ]
        Alamofire.request(urls.filterInsurance, method: .post, parameters: param, encoding: JSONEncoding(options: []), headers: nil).responseJSON { (response) in
            switch response.result {
                
            case .success:
                if let value = response.result.value as? NSDictionary {
                    if value["state"] as? Int == 101{
                        if let arr = value["patients"] as? NSArray{
                            
                            for data in arr{
                                var id = 0
                                var added_by_doctor_id = 0
                                var country_id = 0
                                var gender = " "
                                var phone = " "
                                var name = " "
                                var email = " "
                                var image = " "
                                var notes = " "
                                var insurance_id = " "
                                var insurance_company = " "
                                
                                if let object = data as? Dictionary <String, Any>{
                                    if let value = object["id"] as? Int{
                                        id = value
                                    }
                                    if let value = object["added_by_doctor_id"] as? Int{
                                        added_by_doctor_id = value
                                    }
                                    if let value = object["country_id"] as? Int{
                                        country_id = value
                                    }
                                    if let value = object["gender"] as? String{
                                        gender = value
                                    }
                                    if let valuee = object["phone"] as? String{
                                        phone = valuee
                                    }
                                    if let value = object["name"] as? String{
                                        name = value
                                    }
                                    if let valuee = object["email"] as? String{
                                        email = valuee
                                    }
                                    if let value = object["image"] as? String{
                                        image = urls.healthyPlusReservationImageURL + value
                                    }
                                    if let valuee = object["notes"] as? String{
                                        notes = valuee
                                    }
                                    if let value = object["insurance_id"] as? String{
                                        insurance_id = value
                                    }
                                    if let valuee = object["insurance_company"] as? String{
                                        insurance_company = valuee
                                    }
                                }
                                self.arrPatient.append(Patient(id: id, doctorID: added_by_doctor_id, countryId: country_id, gender: gender, phone: phone, name: name, email: email, picture: image, notes: notes, insuranceId: insurance_id, insuranceCompany: insurance_company))
                            }
                            if self.arrPatient.count > 0{
                                self.patientTbl.reloadData()
                                self.handlingView()
                            }
                        }
                    }
                    else{
                        if let status = value["state"] as? Int{
                            self.otherStatus(Number: status)
                        }
                    }
                    self.stopAnimation(Lot: self.lott)
                }
            case .failure:
                self.errorInConnection()
                self.stopAnimation(Lot: self.lott)
            }
        }
    }
    func handlingView(){
        UIView.animate(withDuration: 0.5) {
            self.dashedView.alpha = 0
            self.patientTbl.alpha = 1
            self.addPatientOutlet.alpha = 1
        }
    }
}
extension patientVC: UITableViewDelegate, UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrPatient.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = patientTbl.dequeueReusableCell(withIdentifier: "cell") as! PatientCell
        cell.settingData(name: arrPatient[indexPath.row].name, id: String(arrPatient[indexPath.row].id), mobile: arrPatient[indexPath.row].phone, gender: arrPatient[indexPath.row].gender)
        customView(custom: cell.patientView)
        
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let vc = UIStoryboard.init(name: "doctor", bundle: nil).instantiateViewController(withIdentifier: "singlePatientVC") as! singlePatientVC
        vc.patientObj = arrPatient[indexPath.row]
        self.navigationController?.pushViewController(vc, animated: true)
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
}
