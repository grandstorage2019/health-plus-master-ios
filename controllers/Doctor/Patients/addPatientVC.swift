//
//  addPatientVC.swift
//  HealthyPlus
//
//  Created by apple on 8/7/18.
//  Copyright © 2018 Kirollos. All rights reserved.
//

import UIKit
import Alamofire
import Lottie

class addPatientVC: UIViewController {
    
    var gender = "male".localized
    var insuranceId = 0
    var picker = UIPickerView()
    var birthPicker = UIDatePicker()
    var arrServices = Array<services>()
    
    var lott: LOTAnimationView = LOTAnimationView(name: "Boat_Loader")

    
    @IBOutlet weak var view1: UIView!
    @IBOutlet weak var view2: UIView!
    @IBOutlet weak var view3: UIView!
    @IBOutlet weak var view4: UIView!
    @IBOutlet weak var view5: UIView!
    @IBOutlet weak var view6: UIView!
    @IBOutlet weak var view7: UIView!
    @IBOutlet weak var view8: UIView!
    
    @IBOutlet weak var notesTF7                : UITextField!
    @IBOutlet weak var emailTF4                : UITextField!
    @IBOutlet weak var bithDateTF5             : UITextField!
    @IBOutlet weak var patientNameTF1          : UITextField!
    @IBOutlet weak var MobileNumberTF2         : UITextField!
    @IBOutlet weak var PatientNumberTF3        : UITextField!
    @IBOutlet weak var insuranceCompanyTF8     : UITextField!
    @IBOutlet weak var PatientIdentificationTF6: UITextField!
    
    @IBOutlet weak var maleBtn                 : UIButton!
    @IBOutlet weak var femaleBtn               : UIButton!
    @IBOutlet weak var switchOutlet            : UISwitch!
    @IBOutlet weak var stackViewOutlet         : UIStackView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        insuranceData()
        picker.delegate = self
        picker.dataSource = self
        bithDateTF5.inputView = birthPicker
        insuranceCompanyTF8.inputView = picker
        MobileNumberTF2.keyboardType = .decimalPad
        showDatePicker()
        
        setNavigationTitle(Title: "Add Patient")
        
        let barButton = UIBarButtonItem(title: "Save".localized, style: .done, target: self, action: #selector(SaveBtn))
        self.navigationItem.rightBarButtonItem = barButton
        
    }
    
    @IBAction func switchAction(_ sender: Any) {
        if switchOutlet.isOn{
            UIView.animate(withDuration: 0.5) {
                self.stackViewOutlet.alpha = 1
            }
        }else{
            UIView.animate(withDuration: 0.5) {
                self.stackViewOutlet.alpha = 0
            }
        }
    }
    
    @IBAction func genderAction(_ sender: UIButton) {
        if sender.tag == 1{
            maleBtn.setImage(#imageLiteral(resourceName: "select"), for: .normal)
            femaleBtn.setImage(#imageLiteral(resourceName: "unSelect"), for: .normal)
            gender = "male".localized
        }else{
            femaleBtn.setImage(#imageLiteral(resourceName: "select"), for: .normal)
            maleBtn.setImage(#imageLiteral(resourceName: "unSelect"), for: .normal)
            gender = "female".localized
        }
    }
    
    @objc func SaveBtn(){
        if sharedHandler.isValidAction(textFeilds: [patientNameTF1, MobileNumberTF2, PatientNumberTF3, emailTF4, bithDateTF5, PatientIdentificationTF6, notesTF7]){
            let param = [
                "method"            : "add_patient" ,
                "name"              : "\(patientNameTF1.text!)" ,
                "phone"             : "\(MobileNumberTF2.text!)" ,
                "password"          :" \(PatientNumberTF3.text!)" ,
                "birth_day"         : "\(bithDateTF5.text!)",
                "notes"             : "\(notesTF7.text!)",
                "gender"            : "\(gender)",
                "email"             : "\(emailTF4.text!)",
                "device_id"         : "\(sharedHandler.getDeviceID())",
                "insurance_id"      : "\(insuranceId)",
                "added_by_doctor_id": "\(sharedHandler.getUserID())"
            ]
            addNewInDoctor(param: param)
        }else{
            showAttentionMessage(msg: "Please, Fill All Fields")
        }
    }
    
    func insuranceData(){
        lott = playAnimation(vc: self)

        let param = [
            "method": "insurance_companies",
            "language": "\(sharedHandler.getLanguage())",
            "doctor_id": "\(sharedHandler.getUserID())"
        ]
        Alamofire.request(urls.filterInsurance, method: .post, parameters: param, encoding: JSONEncoding(options: []), headers: nil).responseJSON { (response) in
            switch response.result {
                
            case .success:
                if let value = response.result.value as? NSDictionary {
                    if value["state"] as? Int == 101{
                        if let arr = value["insurance_companies"] as? NSArray{
                            
                            for data in arr{
                                var id = 0
                                var name = " "
                                
                                if let object = data as? Dictionary <String, Any>{
                                    if let value = object["id"] as? Int{
                                        id = value
                                    }
                                    if let value = object["name"] as? String{
                                        name = value
                                    }
                                }
                                self.arrServices.append(services(id: id, name: name))
                            }
                            self.picker.reloadAllComponents()
                        }
                    }
                    else{
                        if let status = value["state"] as? Int{
                            self.otherStatus(Number: status)
                        }
                    }
                    self.stopAnimation(Lot: self.lott)

                }
            case .failure:
                self.errorInConnection()
                self.stopAnimation(Lot: self.lott)

            }
        }
    }
    
}

extension addPatientVC: UITextFieldDelegate{
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField == patientNameTF1{
            showLabel(label: view1)
        }else if textField == MobileNumberTF2{
            showLabel(label: view2)
        }else if textField == PatientNumberTF3{
            showLabel(label: view3)
        }else if textField == emailTF4{
            showLabel(label: view4)
        }else if textField == bithDateTF5{
            showLabel(label: view5)
        }else if textField == PatientIdentificationTF6{
            showLabel(label: view6)
        }else if textField == notesTF7{
            showLabel(label: view7)
        }else if textField == insuranceCompanyTF8{
            showLabel(label: view8)
        }
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        if textField == patientNameTF1{
            grayLabel(label: view1)
        }else if textField == MobileNumberTF2{
            grayLabel(label: view2)
        }else if textField == PatientNumberTF3{
            grayLabel(label: view3)
        }else if textField == emailTF4{
            grayLabel(label: view4)
        }else if textField == bithDateTF5{
            grayLabel(label: view5)
        }else if textField == PatientIdentificationTF6{
            grayLabel(label: view6)
        }else if textField == notesTF7{
            grayLabel(label: view7)
        }else if textField == insuranceCompanyTF8{
            grayLabel(label: view8)
        }
    }
    
    func showLabel(label: UIView){
        UIView.animate(withDuration: 0.5, animations: {
            //  self.newImages.alpha = 0.0
            label.backgroundColor = colors.appColor
        })
    }
    
    func grayLabel(label: UIView) {
        label.backgroundColor = UIColor.lightGray
    }
}

extension addPatientVC: UIPickerViewDelegate, UIPickerViewDataSource{
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return arrServices.count
    }
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return arrServices[row].name
    }
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        if arrServices.count > 0{
            insuranceId = arrServices[row].id
            
            insuranceCompanyTF8.text = arrServices[row].name
        }else{
            insuranceData()
        }
    }
    func showDatePicker(){
        birthPicker.datePickerMode = .date
        
        let toolbar = UIToolbar();
        toolbar.sizeToFit()
        
        //done button & cancel button
        let doneButton = UIBarButtonItem(title: "Done".localized, style: .done, target: self, action: #selector(donedatePicker))
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: "Cancel".localized, style: .done, target: self, action: #selector(cancelDatePicker))
        toolbar.setItems([doneButton,spaceButton,cancelButton], animated: false)
        
        bithDateTF5.inputAccessoryView = toolbar
        
    }
    @objc func donedatePicker(){
        //For date formate
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd"
        bithDateTF5.text = formatter.string(from: birthPicker.date)
        //dismiss date picker dialog
        self.view.endEditing(true)
    }
    @objc func cancelDatePicker(){
        self.view.endEditing(true)
    }
}
