//
//  addAppointmentVC.swift
//  HealthyPlus
//
//  Created by apple on 8/13/18.
//  Copyright © 2018 Kirollos. All rights reserved.
//

import UIKit
import ExpyTableView
import Alamofire
import Lottie

class addAppointmentVC: UIViewController {
    
    var id = " "
    var doctor_id = " "
    var from_date          = " "
    var to_date            = " "
    var is_life_time       = " "
    var Sat_from_time      = " "
    var Sat_to_time        = " "
    var Sat_type           = " "
    var Sat_wating_time    = " "
    var Sat_num_resrvation = " "
    var Sun_from_time      = " "
    var Sun_to_time        = " "
    var Sun_type           = " "
    var Sun_wating_time    = " "
    var Sun_num_resrvation = " "
    var Mon_from_time      = " "
    var Mon_to_time        = " "
    var Mon_type           = " "
    var Mon_wating_time    = " "
    var Mon_num_resrvation = " "
    var Tue_from_time      = " "
    var Tue_to_time        = " "
    var Tue_type           = " "
    var Tue_wating_time    = " "
    var Tue_num_resrvation = " "
    var Thu_from_time      = " "
    var Thu_to_time        = " "
    var Thu_type           = " "
    var Thu_wating_time    = " "
    var Thu_num_resrvation = " "
    var Wed_from_time      = " "
    var Wed_to_time        = " "
    var Wed_type           = " "
    var Wed_wating_time    = " "
    var Wed_num_resrvation = " "
    var Fri_from_time      = " "
    var Fri_to_time        = " "
    var Fri_type           = " "
    var Fri_wating_time    = " "
    var Fri_num_resrvation = " "
    
    var arrType = ["Specific Dates".localized, "Life Long".localized]
    var arrDays   = [false, false, false, false, false, false, false]
    
    
    var typePicker = UIPickerView()
    
    var toPicker = UIDatePicker()
    var fromPicker = UIDatePicker()
    
    var lott: LOTAnimationView = LOTAnimationView(name: "Boat_Loader")

    @IBOutlet weak var expandableTbl: ExpyTableView!
    @IBOutlet weak var specificDates: UITextField!
    @IBOutlet weak var toLbl: UITextField!
    @IBOutlet weak var fromLbl: UITextField!
    @IBOutlet weak var stackView: UIStackView!
    @IBOutlet weak var stackHeight: NSLayoutConstraint!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        appointmentData()
        
        typePicker.delegate   = self
        typePicker.dataSource = self
        
        expandableTbl.dataSource = self
        expandableTbl.delegate   = self
        
        toLbl.inputView = toPicker
        fromLbl.inputView = fromPicker
        specificDates.inputView = typePicker
        
        showtoPicker()
        showfromPicker()
        
        expandableTbl.estimatedRowHeight  = 400
        expandableTbl.expandingAnimation  = .fade
        expandableTbl.collapsingAnimation = .fade
        expandableTbl.tableFooterView     = UIView()
        expandableTbl.rowHeight = UITableViewAutomaticDimension
        
        setNavigationTitle(Title: "Appointments")
    }
    func appointmentData(){
        lott = playAnimation(vc: self)

        let param = [
            "method": "work_times",
            "doctor_id": "\(sharedHandler.getUserID())"
        ]
        Alamofire.request(urls.filterInsurance, method: .post, parameters: param, encoding: JSONEncoding(options: []), headers: nil).responseJSON { (response) in
            switch response.result {
                
            case .success:
                if let value = response.result.value as? NSDictionary {
                    if value["state"] as? Int == 101{
                        if let arr = value["work_times"] as? NSArray{
                            for data in arr{
                                if let dic = data as? Dictionary <String, Any> {
                                    if let value = dic["id"] as? String{
                                        self.id = value
                                    }
                                    if let value = dic["doctor_id"] as? String{
                                        self.doctor_id = value
                                    }
                                    if let value = dic["from_date"] as? String{
                                        self.from_date = value
                                        self.fromLbl.text = value
                                    }
                                    if let value = dic["to_date"] as? String{
                                        self.to_date = value
                                        self.toLbl.text = value
                                    }
                                    if let valuee = dic["is_life_time"] as? String{
                                        self.is_life_time = valuee
                                        if valuee == "0"{
                                            self.specificDates.text = self.arrType[0]
                                        }else{
                                            self.specificDates.text = self.arrType[1]
                                            self.stackView.alpha = 0
                                            self.stackHeight.constant = 0
                                        }
                                    }
                                    if let value = dic["Sat_from_time"] as? String{
                                        self.Sat_from_time = value
                                        self.arrDays[0] = true
                                        self.expandableTbl.expand(0)
                                    }
                                    if let valuee = dic["Sat_to_time"] as? String{
                                        self.Sat_to_time = valuee
                                    }
                                    if let valuee = dic["Sat_type"] as? String{
                                        self.Sat_type = valuee
                                    }
                                    if let value = dic["Sat_wating_time"] as? String{
                                        self.Sat_wating_time = value
                                    }
                                    if let valuee = dic["Sat_num_resrvation"] as? String{
                                        self.Sat_num_resrvation = valuee
                                    }
                                    if let value = dic["Sun_from_time"] as? String{
                                        self.Sun_from_time = value
                                        self.arrDays[1] = true
                                        self.expandableTbl.expand(1)
                                    }
                                    if let valuee = dic["Sun_to_time"] as? String{
                                        self.Sun_to_time = valuee
                                    }
                                    if let valuee = dic["Sun_type"] as? String{
                                        self.Sun_type = valuee
                                    }
                                    if let value = dic["Sun_wating_time"] as? String{
                                        self.Sun_wating_time = value
                                    }
                                    if let valuee = dic["Sun_num_resrvation"] as? String{
                                        self.Sun_num_resrvation = valuee
                                    }
                                    if let value = dic["Mon_from_time"] as? String{
                                        self.Mon_from_time = value
                                        self.arrDays[2] = true
                                        self.expandableTbl.expand(2)
                                    }
                                    if let valuee = dic["Mon_to_time"] as? String{
                                        self.Mon_to_time = valuee
                                    }
                                    if let valuee = dic["Mon_type"] as? String{
                                        self.Mon_type = valuee
                                    }
                                    if let value = dic["Mon_wating_time"] as? String{
                                        self.Mon_wating_time = value
                                    }
                                    if let valuee = dic["Mon_num_resrvation"] as? String{
                                        self.Mon_num_resrvation = valuee
                                    }
                                    if let value = dic["Tue_from_time"] as? String{
                                        self.Tue_from_time = value
                                        self.arrDays[3] = true
                                        self.expandableTbl.expand(3)
                                    }
                                    if let valuee = dic["Tue_to_time"] as? String{
                                        self.Tue_to_time = valuee
                                    }
                                    if let valuee = dic["Tue_type"] as? String{
                                        self.Tue_type = valuee
                                    }
                                    if let value = dic["Tue_wating_time"] as? String{
                                        self.Tue_wating_time = value
                                    }
                                    if let valuee = dic["Tue_num_resrvation"] as? String{
                                        self.Tue_num_resrvation = valuee
                                    }
                                    if let value = dic["Thu_from_time"] as? String{
                                        self.Thu_from_time = value
                                        self.arrDays[5] = true
                                        self.expandableTbl.expand(5)
                                    }
                                    if let valuee = dic["Thu_to_time"] as? String{
                                        self.Thu_to_time = valuee
                                    }
                                    if let valuee = dic["Thu_type"] as? String{
                                        self.Thu_type = valuee
                                    }
                                    if let value = dic["Thu_wating_time"] as? String{
                                        self.Thu_wating_time = value
                                    }
                                    if let valuee = dic["Thu_num_resrvation"] as? String{
                                        self.Thu_num_resrvation = valuee
                                    }
                                    if let value = dic["Wed_from_time"] as? String{
                                        self.Wed_from_time = value
                                        self.arrDays[4] = true
                                        self.expandableTbl.expand(4)
                                    }
                                    if let valuee = dic["Wed_to_time"] as? String{
                                        self.Wed_to_time = valuee
                                    }
                                    if let valuee = dic["Wed_type"] as? String{
                                        self.Wed_type = valuee
                                    }
                                    if let value = dic["Wed_wating_time"] as? String{
                                        self.Wed_wating_time = value
                                    }
                                    if let valuee = dic["Wed_num_resrvation"] as? String{
                                        self.Wed_num_resrvation = valuee
                                    }
                                    if let value = dic["Fri_from_time"] as? String{
                                        self.Fri_from_time = value
                                        self.arrDays[6] = true
                                        self.expandableTbl.expand(6)
                                    }
                                    if let valuee = dic["Fri_to_time"] as? String{
                                        self.Fri_to_time = valuee
                                    }
                                    if let valuee = dic["Fri_type"] as? String{
                                        self.Fri_type = valuee
                                    }
                                    if let value = dic["Fri_wating_time"] as? String{
                                        self.Fri_wating_time = value
                                    }
                                    if let valuee = dic["Fri_num_resrvation"] as? String{
                                        self.Fri_num_resrvation = valuee
                                    }
                                }
                            }
                        }
                        self.expandableTbl.reloadData()
                    }
                    else{
                        if let status = value["state"] as? Int{
                            self.otherStatus(Number: status)
                        }
                    }
                    self.stopAnimation(Lot: self.lott)

                }
            case .failure:
                self.errorInConnection()
                self.stopAnimation(Lot: self.lott)

            }
        }
    }
    
    func showfromPicker(){
        fromPicker.datePickerMode = .date
        
        let toolbar = UIToolbar();
        toolbar.sizeToFit()
        
        //done button & cancel button
        let doneButton = UIBarButtonItem(title: "Done".localized, style: .done, target: self, action: #selector(donefromPicker))
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: "Cancel".localized, style: .done, target: self, action: #selector(cancelDatePicker))
        toolbar.setItems([doneButton,spaceButton,cancelButton], animated: false)
        
        fromLbl.inputAccessoryView = toolbar
        
    }
    @objc func donefromPicker(){
        //For date formate
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd"
        fromLbl.text = formatter.string(from: fromPicker.date)
        //dismiss date picker dialog
        self.view.endEditing(true)
    }
    @objc func cancelDatePicker(){
        self.view.endEditing(true)
    }
    
    func showtoPicker(){
        toPicker.datePickerMode = .date
        
        let toolbar = UIToolbar();
        toolbar.sizeToFit()
        
        //done button & cancel button
        let doneButton = UIBarButtonItem(title: "Done".localized, style: .done, target: self, action: #selector(donetoPicker))
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: "Cancel".localized, style: .done, target: self, action: #selector(cancelDatePicker))
        toolbar.setItems([doneButton,spaceButton,cancelButton], animated: false)
        
        toLbl.inputAccessoryView = toolbar
        
    }
    @objc func donetoPicker(){
        //For date formate
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd"
        toLbl.text = formatter.string(from: toPicker.date)
        //dismiss date picker dialog
        self.view.endEditing(true)
    }
}

extension addAppointmentVC:  ExpyTableViewDelegate, ExpyTableViewDataSource{
    func tableView(_ tableView: ExpyTableView, expandableCellForSection section: Int) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: headerTableViewCell.self)) as! headerTableViewCell
        cell.layoutMargins = UIEdgeInsets.zero
        cell.isSwitch.tag = section
        cell.isSwitch.isOn = arrDays[section]
        cell.isSwitch.addTarget(self, action: #selector(switchAction(sender:)), for: .touchUpInside)
        
        switch section {
        case 0:
            cell.dayName.text = "Saturday".localized
            return cell
        case 1:
            cell.dayName.text = "Sunday".localized
            return cell
        case 2:
            cell.dayName.text = "Monday".localized
            return cell
        case 3:
            cell.dayName.text = "Tuesday".localized
            return cell
        case 4:
            cell.dayName.text = "Wendesday".localized
            return cell
        case 5:
            cell.dayName.text = "Thursday".localized
            return cell
        case 6:
            cell.dayName.text = "Friday".localized
            return cell
        default:
            cell.dayName.text = "Saturday".localized
            return cell
        }
    }
    @objc func switchAction(sender: UISwitch){
        let indexPath = sender.tag
        
        if sender.isOn{
            expandableTbl.expand(indexPath)
        }else{
            expandableTbl.collapse(indexPath)
        }
        
    }
    func tableView(_ tableView: ExpyTableView, canExpandSection section: Int) -> Bool {
        return true
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: ExpyTableView, expyState state: ExpyState, changeForSection section: Int) {
        
        switch state {
        case .willExpand:
            print("WILL EXPAND")
            
        case .willCollapse:
            print("WILL COLLAPSE")
            
        case .didExpand:
            print("DID EXPAND")
            
        case .didCollapse:
            print("DID COLLAPSE")
        }
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        return 7
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // Please see https://github.com/okhanokbay/ExpyTableView/issues/12
        
        return 2
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: rowTableViewCell.self)) as! rowTableViewCell
        
        switch indexPath.section {
            
        case 0:
            cell.settingData(from: Sat_from_time, to: Sat_to_time, type: Sat_type, waiting: Sat_wating_time)
            return cell
        case 1:
            cell.settingData(from: Sun_from_time, to: Sun_to_time, type: Sun_type, waiting: Sun_wating_time)
            return cell
        case 2:
            cell.settingData(from: Mon_from_time, to: Mon_to_time, type: Mon_type, waiting: Mon_wating_time)
            return cell
        case 3:
            cell.settingData(from: Tue_from_time, to: Tue_to_time, type: Tue_type, waiting: Tue_wating_time)
            return cell
        case 4:
            cell.settingData(from: Wed_from_time, to: Wed_to_time, type: Wed_type, waiting: Wed_wating_time)
            return cell
        case 5:
            cell.settingData(from: Thu_from_time, to: Thu_to_time, type: Thu_type, waiting: Thu_wating_time)
            return cell
        case 6:
            cell.settingData(from: Fri_from_time, to: Fri_to_time, type: Fri_type, waiting: Fri_wating_time)
            return cell
        default:
            return cell
        }
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
        if arrDays[indexPath.section]{
            self.arrDays[indexPath.section] = false
        }else{
            self.arrDays[indexPath.section] = true
        }
        expandableTbl.reloadSections(IndexSet(indexPath), with: UITableViewRowAnimation.none)
    }
}

extension addAppointmentVC: UIPickerViewDelegate, UIPickerViewDataSource{
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return arrType.count
    }
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return arrType[row]
    }
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        specificDates.text = arrType[row]
        if row == 0{
            self.is_life_time = "0"
            UIView.animate(withDuration: 0.5) {
                self.stackHeight.constant = 30
                self.stackView.alpha = 1
            }
        }else{
            self.is_life_time = "1"
        }
    }
}
