//
//  patientReservationVC.swift
//  HealthyPlus
//
//  Created by apple on 8/13/18.
//  Copyright © 2018 Kirollos. All rights reserved.
//

import UIKit
import Alamofire
import Lottie

class patientReservationVC: UIViewController {
    
    var date = " "
    var arrPatientReservation = Array<patientReservation>()
    
    var lott: LOTAnimationView = LOTAnimationView(name: "Boat_Loader")
    
    @IBOutlet weak var noReservaionLbl: UILabel!
    @IBOutlet weak var patientReservationTbl: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        patientReservationTbl.delegate = self
        patientReservationTbl.dataSource = self
        appointmentReservationData()
    }
    func appointmentReservationData(){
        lott = playAnimation(vc: self)

        let param = [
            "method"   : "patient_reservations",
            "language" : "\(sharedHandler.getLanguage())",
            "doctor_id": "\(sharedHandler.getUserID())",
            "date"     : "\(date)"
        ]
        print(param)
        Alamofire.request(urls.filterInsurance, method: .post, parameters: param, encoding: JSONEncoding(options: []), headers: nil).responseJSON { (response) in
            switch response.result {
                
            case .success:
                if let value = response.result.value as? NSDictionary {
                    if value["state"] as? Int == 101{
                        if let arr = value["patient_reservation"] as? NSArray{
                            
                            for data in arr{
                                var time = " "
                                var name = " "
                                var gender = " "
                                var id = " "
                                var phone = " "
                                
                                if let object = data as? Dictionary <String, Any>{
                                    if let value = object["time"] as? String{
                                        time = value
                                    }
                                    if let value = object["name"] as? String{
                                        name = value
                                    }
                                    if let valuee = object["gender"] as? String{
                                        gender = valuee
                                    }
                                    if let value = object["id"] as? String{
                                        id = value
                                    }
                                    if let valuee = object["phone"] as? String{
                                        phone = valuee
                                    }
                                }
                                self.arrPatientReservation.append(patientReservation(time: time, name: name, gender: gender, id: id, phone: phone))
                            }
                            if self.arrPatientReservation.count > 0{
                                self.patientReservationTbl.reloadData()
                                self.patientReservationTbl.alpha = 1
                                self.noReservaionLbl.alpha = 0
                            }else{
                                self.noReservaionLbl.alpha = 1
                                self.patientReservationTbl.alpha = 0
                            }
                        }
                    }
                    else{
                        if let status = value["state"] as? Int{
                            self.otherStatus(Number: status)
                        }
                    }
                    self.stopAnimation(Lot: self.lott)

                }
            case .failure:
                self.errorInConnection()
                self.stopAnimation(Lot: self.lott)

            }
        }
    }
}
extension patientReservationVC: UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrPatientReservation.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = patientReservationTbl.dequeueReusableCell(withIdentifier: "cell") as! patientReservationCell
        cell.settingData(patientName: arrPatientReservation[indexPath.row].name, patientID: arrPatientReservation[indexPath.row].id, mobile: arrPatientReservation[indexPath.row].phone, gender: arrPatientReservation[indexPath.row].gender, time: arrPatientReservation[indexPath.row].time, date: date, btnTag: indexPath.row)
        customView(custom: cell.reservationView)
        cell.addPatientLbl.addTarget(self, action: #selector(addNewPatient(sender:)), for: .touchUpInside)
        return cell
    }
    
    @objc func addNewPatient(sender: UIButton){
        let data = arrPatientReservation[sender.tag]
        let vc = UIStoryboard.init(name: "doctor", bundle: nil).instantiateViewController(withIdentifier: "addNewPatientFromResevationVC") as! addNewPatientFromResevationVC
        vc.ID   = data.id
        vc.Name = data.name
        self.navigationController?.pushViewController(vc, animated: true)
    }
} 
