//
//  addAppointment.swift
//  HealthyPlus
//
//  Created by Donna on 8/28/18.
//  Copyright © 2018 Kirollos. All rights reserved.
//

import UIKit
import Lottie
import Alamofire

class addAppointment: UIViewController {

    var typePicker : UIPickerView = {
        let pick = UIPickerView()
        pick.backgroundColor = UIColor.clear
        pick.tag = 5
        return pick
    }()
    var datePicker   = UIDatePicker()
    var arrType = ["Specific Dates".localized, "Life Long".localized]
    var lott: LOTAnimationView = LOTAnimationView(name: "Boat_Loader")
    
    
    
    var arrTypeCommon = ["first reservation".localized, "specific time".localized]
    var arrTypeCommonAR = ["اولوية الحضور", "وقت الانتظار"]
    let PickerView : UIPickerView = {
        let pick = UIPickerView()
        pick.backgroundColor = UIColor.clear
        pick.tag = 11
        return pick
    }()
    
    var arrayTypeSelection = [String]()
    var arrWaitingTime = ["0", "5", "10", "15", "20", "25", "30", "35", "40", "45", "50", "55", "60"]
    var arrFirstReservation = ["01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23", "24", "25", "26", "27", "28", "29", "30"]
    
    let selectTypePicker : UIPickerView = {
        let pick = UIPickerView()
        pick.backgroundColor = UIColor.clear
        pick.tag = 22
        return pick
    }()
    
    let selectTimePicker : UIDatePicker = {
        let pick = UIDatePicker()
        pick.datePickerMode = UIDatePickerMode.time
        pick.backgroundColor = UIColor.clear
        pick.tag = 66
        return pick
    }()
    
    var selectionIndex: Int = 0
    var currentTag         : Int    = 0
    var id                 : String = ""
    var doctor_id          : String = ""
    var from_date          : String = ""
    var to_date            : String = ""
    var is_life_time       : String = ""
    var Sat_from_time      : String = ""
    var Sat_to_time        : String = ""
    var Sat_type           : String = ""
    var Sat_wating_time    : String = ""
    var Sat_num_resrvation : String = ""
    var Sun_from_time      : String = ""
    var Sun_to_time        : String = ""
    var Sun_type           : String = ""
    var Sun_wating_time    : String = ""
    var Sun_num_resrvation : String = ""
    var Mon_from_time      : String = ""
    var Mon_to_time        : String = ""
    var Mon_type           : String = ""
    var Mon_wating_time    : String = ""
    var Mon_num_resrvation : String = ""
    var Tue_from_time      : String = ""
    var Tue_to_time        : String = ""
    var Tue_type           : String = ""
    var Tue_wating_time    : String = ""
    var Tue_num_resrvation : String = ""
    var Thu_from_time      : String = ""
    var Thu_to_time        : String = ""
    var Thu_type           : String = ""
    var Thu_wating_time    : String = ""
    var Thu_num_resrvation : String = ""
    var Wed_from_time      : String = ""
    var Wed_to_time        : String = ""
    var Wed_type           : String = ""
    var Wed_wating_time    : String = ""
    var Wed_num_resrvation : String = ""
    var Fri_from_time      : String = ""
    var Fri_to_time        : String = ""
    var Fri_type           : String = ""
    var Fri_wating_time    : String = ""
    var Fri_num_resrvation : String = ""
    
    
    @IBOutlet weak var switch1: UISwitch!
    @IBOutlet weak var switch2: UISwitch!
    @IBOutlet weak var switch3: UISwitch!
    @IBOutlet weak var switch4: UISwitch!
    @IBOutlet weak var switch5: UISwitch!
    @IBOutlet weak var switch6: UISwitch!
    @IBOutlet weak var switch7: UISwitch!
    
    var sharedText     = UITextField()
    @IBOutlet weak var to1: UITextField!// 21 22 23 ...
    @IBOutlet weak var to2: UITextField!
    @IBOutlet weak var to3: UITextField!
    @IBOutlet weak var to4: UITextField!
    @IBOutlet weak var to5: UITextField!
    @IBOutlet weak var to6: UITextField!
    @IBOutlet weak var to7: UITextField!
    
    @IBOutlet weak var type1: UITextField!
    @IBOutlet weak var type2: UITextField!
    @IBOutlet weak var type3: UITextField!
    @IBOutlet weak var type4: UITextField!
    @IBOutlet weak var type5: UITextField!
    @IBOutlet weak var type6: UITextField!
    @IBOutlet weak var type7: UITextField!
    
    @IBOutlet weak var from1: UITextField!//11 12 13 ...
    @IBOutlet weak var from2: UITextField!
    @IBOutlet weak var from3: UITextField!
    @IBOutlet weak var from4: UITextField!
    @IBOutlet weak var from5: UITextField!
    @IBOutlet weak var from6: UITextField!
    @IBOutlet weak var from7: UITextField!
    
    @IBOutlet weak var stack1: UIStackView!
    @IBOutlet weak var stack2: UIStackView!
    @IBOutlet weak var stack3: UIStackView!
    @IBOutlet weak var stack4: UIStackView!
    @IBOutlet weak var stack5: UIStackView!
    @IBOutlet weak var stack6: UIStackView!
    @IBOutlet weak var stack7: UIStackView!
    
    @IBOutlet weak var waiting1: UITextField!
    @IBOutlet weak var waiting2: UITextField!
    @IBOutlet weak var waiting3: UITextField!
    @IBOutlet weak var waiting4: UITextField!
    @IBOutlet weak var waiting5: UITextField!
    @IBOutlet weak var waiting6: UITextField!
    @IBOutlet weak var waiting7: UITextField!
    
    @IBOutlet weak var waitingLbl1: UILabel!
    @IBOutlet weak var waitingLbl2: UILabel!
    @IBOutlet weak var waitingLbl3: UILabel!
    @IBOutlet weak var waitingLbl4: UILabel!
    @IBOutlet weak var waitingLbl5: UILabel!
    @IBOutlet weak var waitingLbl6: UILabel!
    @IBOutlet weak var waitingLbl7: UILabel!
    
    @IBOutlet weak var stackHeight1: NSLayoutConstraint!
    @IBOutlet weak var stackHeight2: NSLayoutConstraint!
    @IBOutlet weak var stackHeight3: NSLayoutConstraint!
    @IBOutlet weak var stackHeight4: NSLayoutConstraint!
    @IBOutlet weak var stackHeight5: NSLayoutConstraint!
    @IBOutlet weak var stackHeight6: NSLayoutConstraint!
    @IBOutlet weak var stackHeight7: NSLayoutConstraint!
    
    
    @IBOutlet weak var specificDates: UITextField!
    @IBOutlet weak var toLbl: UITextField!
    @IBOutlet weak var fromLbl: UITextField!
    @IBOutlet weak var stackView: UIStackView!
    @IBOutlet weak var stackHeight: NSLayoutConstraint!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        appointmentData()
        
        typePicker.delegate   = self
        typePicker.dataSource = self
        
        PickerView.delegate   = self
        PickerView.dataSource = self
        
        selectTypePicker.delegate   = self
        selectTypePicker.dataSource = self
        
//        selectTimePicker.delegate   = self
//        selectTimePicker.dataSource = self
        
        specificDates.inputView = typePicker
        
//        isValidAction(textFeilds: [to1, to2, to3, to4, to5, to6, to7, from1, from2, from3, from3, from4, from5, from6, from7])
        let barButton = UIBarButtonItem(title: "Save".localized, style: .done, target: self, action: #selector(saveBtn))
        self.navigationItem.rightBarButtonItem = barButton
    }
    @objc func saveBtn(){
        if !switch1.isOn{
            Sat_from_time      = ""
            Sat_to_time        = ""
            Sat_type           = ""
            Sat_wating_time    = ""
            Sat_num_resrvation = ""
        }
        if !switch2.isOn{
            Sun_from_time      = ""
            Sun_to_time        = ""
            Sun_type           = ""
            Sun_wating_time    = ""
            Sun_num_resrvation = ""
        }
        if !switch3.isOn{
            Mon_from_time      = ""
            Mon_to_time        = ""
            Mon_type           = ""
            Mon_wating_time    = ""
            Mon_num_resrvation = ""
        }
        if !switch4.isOn{
            Tue_from_time      = ""
            Tue_to_time        = ""
            Tue_type           = ""
            Tue_wating_time    = ""
            Tue_num_resrvation = ""
        }
        if !switch5.isOn{
            Wed_from_time      = ""
            Wed_to_time        = ""
            Wed_type           = ""
            Wed_wating_time    = ""
            Wed_num_resrvation = ""
        }
        if !switch6.isOn{
            Thu_from_time      = ""
            Thu_to_time        = ""
            Thu_type           = ""
            Thu_wating_time    = ""
            Thu_num_resrvation = ""
        }
        if !switch7.isOn{
            Fri_from_time      = ""
            Fri_to_time        = ""
            Fri_type           = ""
            Fri_wating_time    = ""
            Fri_num_resrvation = ""
        }
        saveAppointment()
    }
    func saveAppointment(){
        let param = [
            "method":"save_times",
            "doctor_id":"\(sharedHandler.getUserID())",
            "language":"ar",
            "is_life_time": "\(is_life_time)",
            "from_date": "\(from_date)",
            "to_date": "\(to_date)",
            "Sat_from_time": "\(Sat_from_time)",
            "Sat_to_time": "\(Sat_to_time)",
            "Sat_type": "\(Sat_type)",
            "Sat_num_resrvation":"\(Sat_num_resrvation)",
            "Sun_from_time":"\(Sun_from_time)",
            "Sun_to_time":"\(Sun_to_time)",
            "Sun_type":"\(Sun_type)",
            "Sun_wating_time":"\(Sun_wating_time)",
            "Sun_num_resrvation":"\(Sun_num_resrvation)",
            "Mon_from_time":"\(Mon_from_time)",
            "Mon_to_time":"\(Mon_to_time)",
            "Mon_type":"\(Mon_type)",
            "Mon_num_resrvation":"\(Mon_num_resrvation)",
            "Tue_from_time":"\(Tue_from_time)",
            "Tue_to_time":"\(Tue_to_time)",
            "Tue_type":"\(Tue_type)",
            "Tue_num_resrvation":"\(Tue_num_resrvation)",
            "Thu_from_time":"\(Thu_from_time)",
            "Thu_to_time":"\(Thu_to_time)",
            "Thu_type":"\(Thu_type)",
            "Thu_num_resrvation":"\(Thu_num_resrvation)",
            "Wed_from_time":"\(Wed_from_time)",
            "Wed_to_time":"\(Wed_to_time)",
            "Wed_type":"\(Wed_type)",
            "Wed_wating_time":"\(Wed_wating_time)",
            "Fri_from_time":"\(Fri_from_time)",
            "Fri_to_time":"\(Fri_to_time)",
            "Fri_type":"\(Fri_type)",
            "Fri_wating_time":"\(Fri_wating_time)"
        ]
        addNewInDoctor(param: param)
    }
    @IBAction func switchAction(_ sender: UISwitch) {
        switch sender.tag {
        case 1:
            if sender.isOn{
                self.stack1.alpha = 1
                self.stackHeight1.constant = 160
            }else{
                self.stack1.alpha = 0
                self.stackHeight1.constant = 0.0
            }
        case 2:
            if sender.isOn{
                self.stack2.alpha = 1
                self.stackHeight2.constant = 160
            }else{
                self.stack2.alpha = 0
                self.stackHeight2.constant = 0.0
            }
        case 3:
            if sender.isOn{
                self.stack3.alpha = 1
                self.stackHeight3.constant = 160
            }else{
                self.stack3.alpha = 0
                self.stackHeight3.constant = 0.0
            }
        case 4:
            if sender.isOn{
                self.stack4.alpha = 1
                self.stackHeight4.constant = 160
            }else{
                self.stack4.alpha = 0
                self.stackHeight4.constant = 0.0
            }
        case 5:
            if sender.isOn{
                self.stack5.alpha = 1
                self.stackHeight5.constant = 160
            }else{
                self.stack5.alpha = 0
                self.stackHeight5.constant = 0.0
            }
        case 6:
            if sender.isOn{
                self.stack6.alpha = 1
                self.stackHeight6.constant = 160
            }else{
                self.stack6.alpha = 0
                self.stackHeight6.constant = 0.0
            }
        case 7:
            if sender.isOn{
                self.stack7.alpha = 1
                self.stackHeight7.constant = 160
            }else{
                self.stack7.alpha = 0
                self.stackHeight7.constant = 0.0
            }
        default:
            print("any")
        }
    }
    
    func appointmentData(){
        lott = playAnimation(vc: self)
        
        let param = [
            "method": "work_times",
            "doctor_id": "\(sharedHandler.getUserID())"
        ]
        Alamofire.request(urls.filterInsurance, method: .post, parameters: param, encoding: JSONEncoding(options: []), headers: nil).responseJSON { (response) in
            switch response.result {
                
            case .success:
                if let value = response.result.value as? NSDictionary {
                    if value["state"] as? Int == 101{
                        if let arr = value["work_times"] as? NSArray{
                            for data in arr{
                                if let dic = data as? Dictionary <String, Any> {
                                    if let value = dic["id"] as? String{
                                        self.id = value
                                    }
                                    if let value = dic["doctor_id"] as? String{
                                        self.doctor_id = value
                                    }
                                    if let value = dic["from_date"] as? String{
                                        self.from_date = value
                                        self.fromLbl.text = value
                                    }
                                    if let value = dic["to_date"] as? String{
                                        self.to_date = value
                                        self.toLbl.text = value
                                    }
                                    if let valuee = dic["is_life_time"] as? String{
                                        self.is_life_time = valuee
                                        if valuee == "0"{
                                            self.specificDates.text = self.arrType[0]
                                        }else{
                                            self.specificDates.text = self.arrType[1]
                                            self.stackView.alpha = 0
                                            self.stackHeight.constant = 0
                                        }
                                    }
                                    if let value = dic["Sat_from_time"] as? String{
                                        self.Sat_from_time = value
                                        self.from1.text = value
                                    }
                                    if let valuee = dic["Sat_to_time"] as? String{
                                        self.Sat_to_time = valuee
                                        self.to1.text = valuee
                                    }
                                    if let valuee = dic["Sat_type"] as? String{
                                        self.Sat_type = valuee
                                        self.type1.text = valuee
                                        if valuee != ""{
                                            self.switch1.isOn = true
                                            self.stack1.alpha = 1
                                            self.stackHeight1.constant = 160
                                        }
                                        if valuee == "specific time"{
                                            if let value = dic["Sat_wating_time"] as? String{
                                                self.Sat_wating_time = value
                                                self.waiting1.text = value.localized
                                            }
                                        }else{
                                            if let value = dic["Sat_num_resrvation"] as? String{
                                                self.Sat_num_resrvation = value
                                                self.waiting1.text = value
                                                self.waitingLbl1.text = "Number of Booking".localized
                                            }
                                        }
                                    }
                                    if let value = dic["Sun_from_time"] as? String{
                                        self.Sun_from_time = value
                                        self.from2.text = value
                                    }
                                    if let valuee = dic["Sun_to_time"] as? String{
                                        self.Sun_to_time = valuee
                                        self.to2.text = valuee
                                    }
                                    if let valuee = dic["Sun_type"] as? String{
                                        self.Sun_type = valuee
                                        self.type2.text = valuee
                                        if valuee != ""{
                                            self.switch2.isOn = true
                                            self.stack2.alpha = 1
                                            self.stackHeight2.constant = 160
                                        }
                                        if valuee == "specific time"{
                                            if let value = dic["Sun_wating_time"] as? String{
                                                self.Sun_wating_time = value
                                                self.waiting2.text = value.localized
                                            }
                                        }else{
                                            if let value = dic["Sun_num_resrvation"] as? String{
                                                self.Sun_num_resrvation = value
                                                self.waiting2.text = value
                                                self.waitingLbl2.text = "Number of Booking".localized
                                            }
                                        }
                                    }
                                    if let value = dic["Mon_from_time"] as? String{
                                        self.Mon_from_time = value
                                        self.from3.text = value
                                    }
                                    if let valuee = dic["Mon_to_time"] as? String{
                                        self.Mon_to_time = valuee
                                        self.to3.text = valuee
                                    }
                                    if let valuee = dic["Mon_type"] as? String{
                                        self.Mon_type = valuee
                                        self.type3.text = valuee
                                        if valuee != ""{
                                            self.switch3.isOn = true
                                            self.stack3.alpha = 1
                                            self.stackHeight3.constant = 160
                                        }
                                        if valuee == "specific time"{
                                            if let value = dic["Mon_wating_time"] as? String{
                                                self.Mon_wating_time = value
                                                self.waiting3.text = value.localized
                                            }
                                        }else{
                                            if let value = dic["Mon_num_resrvation"] as? String{
                                                self.Mon_num_resrvation = value
                                                self.waiting3.text = value
                                                self.waitingLbl3.text = "Number of Booking".localized
                                            }
                                        }
                                    }
                                    if let value = dic["Tue_from_time"] as? String{
                                        self.Tue_from_time = value
                                        self.from4.text = value
                                    }
                                    if let valuee = dic["Tue_to_time"] as? String{
                                        self.Tue_to_time = valuee
                                        self.to4.text = valuee
                                    }
                                    if let valuee = dic["Tue_type"] as? String{
                                        self.Tue_type = valuee
                                        self.type4.text = valuee
                                        if valuee != ""{
                                            self.switch4.isOn = true
                                            self.stack4.alpha = 1
                                            self.stackHeight4.constant = 160
                                        }
                                        if valuee == "specific time"{
                                            if let value = dic["Tue_wating_time"] as? String{
                                                self.Tue_wating_time = value
                                                self.waiting4.text = value.localized
                                            }
                                        }else{
                                            if let value = dic["Tue_num_resrvation"] as? String{
                                                self.Tue_num_resrvation = value
                                                self.waiting4.text = value
                                                self.waitingLbl4.text = "Number of Booking".localized
                                            }
                                        }
                                    }
                                    if let value = dic["Thu_from_time"] as? String{
                                        self.Thu_from_time = value
                                        self.from6.text = value
                                    }
                                    if let valuee = dic["Thu_to_time"] as? String{
                                        self.Thu_to_time = valuee
                                        self.to6.text = valuee
                                    }
                                    if let valuee = dic["Thu_type"] as? String{
                                        self.Thu_type = valuee
                                        self.type6.text = valuee
                                        if valuee != ""{
                                            self.switch6.isOn = true
                                            self.stack6.alpha = 1
                                            self.stackHeight6.constant = 160
                                        }
                                        if valuee == "specific time"{
                                            if let value = dic["Thu_wating_time"] as? String{
                                                self.Thu_wating_time = value
                                                self.waiting6.text = value.localized
                                            }
                                        }else{
                                            if let value = dic["Thu_num_resrvation"] as? String{
                                                self.Thu_num_resrvation = value
                                                self.waiting6.text = value
                                                self.waitingLbl6.text = "Number of Booking".localized
                                            }
                                        }
                                    }
                                    if let value = dic["Wed_from_time"] as? String{
                                        self.Wed_from_time = value
                                        self.from5.text = value
                                    }
                                    if let valuee = dic["Wed_to_time"] as? String{
                                        self.Wed_to_time = valuee
                                        self.to5.text = valuee
                                    }
                                    if let valuee = dic["Wed_type"] as? String{
                                        self.Wed_type = valuee
                                        self.type5.text = valuee
                                        if valuee != ""{
                                            self.switch5.isOn = true
                                            self.stack5.alpha = 1
                                            self.stackHeight5.constant = 160
                                        }
                                        if valuee == "specific time"{
                                            if let value = dic["Wed_wating_time"] as? String{
                                                self.Wed_wating_time = value
                                                self.waiting5.text = value.localized
                                            }
                                        }else{
                                            if let value = dic["Wed_num_resrvation"] as? String{
                                                self.Wed_num_resrvation = value
                                                self.waiting5.text = value
                                                self.waitingLbl5.text = "Number of Booking".localized
                                            }
                                        }
                                    }
                                    if let value = dic["Fri_from_time"] as? String{
                                        self.Fri_from_time = value
                                        self.from7.text = value
                                    }
                                    if let valuee = dic["Fri_to_time"] as? String{
                                        self.Fri_to_time = valuee
                                        self.to7.text = valuee
                                    }
                                    if let valuee = dic["Fri_type"] as? String{
                                        self.Fri_type = valuee
                                        self.type7.text = valuee
                                        if valuee != ""{
                                            self.switch7.isOn = true
                                            self.stack7.alpha = 1
                                            self.stackHeight7.constant = 160
                                        }
                                        
                                        if valuee == "specific time"{
                                            if let value = dic["Fri_wating_time"] as? String{
                                                self.Fri_wating_time = value
                                                self.waiting7.text = value.localized
                                            }
                                        }else{
                                            if let value = dic["Fri_num_resrvation"] as? String{
                                                self.Fri_num_resrvation = value
                                                self.waiting7.text = value
                                                self.waitingLbl7.text = "Number of Booking".localized
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                    else{
                        if let status = value["state"] as? Int{
                            self.otherStatus(Number: status)
                        }
                    }
                    self.stopAnimation(Lot: self.lott)
                    
                }
            case .failure:
                self.errorInConnection()
                self.stopAnimation(Lot: self.lott)
                
            }
        }
    }

    
}

extension addAppointment: UIPickerViewDelegate, UIPickerViewDataSource{
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        if pickerView.tag == 5{
            return arrType.count
        }else if pickerView.tag == 22 || pickerView.tag == 44{
            return arrayTypeSelection.count
        }else{
            return arrTypeCommon.count
        }
    }
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        if pickerView.tag == 5{
            return arrType[row]
        }else if pickerView.tag == 22 || pickerView.tag == 44{
            return arrayTypeSelection[row]
        }else{
            return arrTypeCommon[row]
        }
        
    }
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        if pickerView.tag == 5{
            specificDates.text = arrType[row]
                
            if row == 0{
                self.is_life_time = "0"
                UIView.animate(withDuration: 0.5) {
                    self.stackHeight.constant = 30
                    self.stackView.alpha = 1
                    
                }
            }else{
                self.is_life_time = "1"
                self.stackView.alpha = 0
                self.stackHeight.constant = 0
            }
            
        }else if pickerView.tag == 22 || pickerView.tag == 44{
            if row >= 0 {
                self.selectionIndex = row
                sharedText.text = arrayTypeSelection[row]
                if pickerView.tag == 22{
                    if sharedText.tag == 41{
                        Sat_num_resrvation = sharedText.text!
                    }else if sharedText.tag == 42{
                        Sun_num_resrvation = sharedText.text!
                    }else if sharedText.tag == 43{
                        Mon_num_resrvation = sharedText.text!
                    }else if sharedText.tag == 44{
                        Tue_num_resrvation = sharedText.text!
                    }else if sharedText.tag == 45{
                        Wed_num_resrvation = sharedText.text!
                    }else if sharedText.tag == 46{
                        Thu_num_resrvation = sharedText.text!
                    }else if sharedText.tag == 47{
                        Fri_num_resrvation = sharedText.text!
                    }
                }else if pickerView.tag == 44{
                    if sharedText.tag == 41{
                        Sat_wating_time = sharedText.text!
                    }else if sharedText.tag == 42{
                        Sun_wating_time = sharedText.text!
                    }else if sharedText.tag == 43{
                        Mon_wating_time = sharedText.text!
                    }else if sharedText.tag == 44{
                        Tue_wating_time = sharedText.text!
                    }else if sharedText.tag == 45{
                        Wed_wating_time = sharedText.text!
                    }else if sharedText.tag == 46{
                        Thu_wating_time = sharedText.text!
                    }else if sharedText.tag == 47{
                        Fri_wating_time = sharedText.text!
                    }
                }
                
            }
        }else{
            if row >=  0{
                sharedText.text = arrTypeCommon[row]
                if row == 0{
                    selectTypePicker.tag = 22
                    arrayTypeSelection = arrFirstReservation
                }else if row == 1{
                    selectTypePicker.tag = 44
                    arrayTypeSelection = arrWaitingTime
                }
                
                if currentTag == 31{
                    if sharedHandler.getLanguage() == "en"{
                        Sat_type = arrTypeCommon[row]
                    }else{
                        Sat_type = arrTypeCommonAR[row]
                    }
                    if row == 0{
                        waitingLbl1.text = "Waiting Time".localized
                    }else if row == 1{
                        waitingLbl1.text = "Number of Booking".localized
                        waiting1.text = "0"
                    }
                }else if currentTag == 32{
                    if sharedHandler.getLanguage() == "en"{
                        Sun_type = arrTypeCommon[row]
                    }else{
                        Sun_type = arrTypeCommonAR[row]
                    }
                    if row == 0{
                        waitingLbl2.text = "Waiting Time".localized
                    }else if row == 1{
                        waitingLbl2.text = "Number of Booking".localized
                        waiting2.text = "0"
                    }
                }else if currentTag == 33{
                    if sharedHandler.getLanguage() == "en"{
                        Mon_type = arrTypeCommon[row]
                    }else{
                        Mon_type = arrTypeCommonAR[row]
                    }
                    if row == 0{
                        waitingLbl3.text = "Waiting Time".localized
                    }else if row == 1{
                        waitingLbl3.text = "Number of Booking".localized
                        waiting3.text = "0"
                    }
                }else if currentTag == 34{
                    if sharedHandler.getLanguage() == "en"{
                        Tue_type = arrTypeCommon[row]
                    }else{
                        Tue_type = arrTypeCommonAR[row]
                    }
                    if row == 0{
                        waitingLbl4.text = "Waiting Time".localized
                    }else if row == 1{
                        waitingLbl4.text = "Number of Booking".localized
                        waiting4.text = "0"
                    }
                }else if currentTag == 35{
                    if sharedHandler.getLanguage() == "en"{
                        Wed_type = arrTypeCommon[row]
                    }else{
                        Wed_type = arrTypeCommonAR[row]
                    }
                    if row == 0{
                        waitingLbl5.text = "Waiting Time".localized
                    }else if row == 1{
                        waitingLbl5.text = "Number of Booking".localized
                        waiting5.text = "0"
                    }
                }else if currentTag == 36{
                    if sharedHandler.getLanguage() == "en"{
                        Thu_type = arrTypeCommon[row]
                    }else{
                        Thu_type = arrTypeCommonAR[row]
                    }
                    if row == 0{
                        waitingLbl6.text = "Waiting Time".localized
                    }else if row == 1{
                        waitingLbl6.text = "Number of Booking".localized
                        waiting6.text = "0"
                    }
                }else if currentTag == 37{
                    if sharedHandler.getLanguage() == "en"{
                        Fri_type = arrTypeCommon[row]
                    }else{
                        Fri_type = arrTypeCommonAR[row]
                    }

                    if row == 0{
                        waitingLbl7.text = "Waiting Time".localized
                    }else if row == 1{
                        waitingLbl7.text = "Number of Booking".localized
                        waiting7.text = "0"
                    }
                }
            }
        }
    }
}

extension addAppointment: UITextFieldDelegate{
    func textFieldDidBeginEditing(_ textField: UITextField) {
        sharedText = textField
        currentTag = textField.tag
        
        
        if textField == type1 || textField == type2 || textField == type3 || textField == type4 || textField == type5 || textField == type6 || textField == type7{
            CreatePicker(tf: textField)
        }else if textField == waiting1 || textField == waiting2 || textField == waiting3 || textField == waiting4 || textField == waiting5 || textField == waiting6 || textField == waiting7 {
            sharedText.tag = textField.tag
            createWaitingPicker(tf: textField)
        }else if textField.tag == 0{
            showTimePicker(tf: textField)
            if textField == fromLbl{
                currentTag = 1
            }else if textField == toLbl{
                currentTag = 2
            }else {
                sharedText.tag = textField.tag
            }
        }else{
            TimePicker(tf: textField)
        }
    }
}

extension addAppointment{
    //showDate Picker
    
    func showTimePicker(tf: UITextField){
        datePicker.datePickerMode = .date
        
        
        let toolbar = UIToolbar();
        toolbar.sizeToFit()
        
        //done button & cancel button
        let doneButton = UIBarButtonItem(title: "Done".localized, style: .done, target: self, action: #selector(donetoPicker))
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: "Cancel".localized, style: .done, target: self, action: #selector(cancelPicker))
        toolbar.setItems([doneButton,spaceButton,cancelButton], animated: false)
        
        tf.inputAccessoryView = toolbar
        tf.inputView = datePicker
    }
    @objc func donetoPicker(){
        //For date formate
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd"
        sharedText.text = formatter.string(from: datePicker.date)
        if currentTag == 1{
            from_date = sharedText.text!
        }else if currentTag == 2{
            to_date   = sharedText.text!
        }
        self.view.endEditing(true)
    }
    
    @objc func cancelPicker(){
        self.view.endEditing(true)
    }
    
    func CreatePicker(tf: UITextField) {
        let toolBart = UIToolbar()
        toolBart.sizeToFit()
        
        let doneButton = UIBarButtonItem(barButtonSystemItem: .done, target: nil, action: #selector(cancelPicker))
        toolBart.setItems([doneButton], animated: true)
        
        self.PickerView.reloadAllComponents()
        tf.inputAccessoryView = toolBart
        tf.inputView = PickerView
        
    }
    
    func createWaitingPicker(tf: UITextField) {
        let toolBart = UIToolbar()
        toolBart.sizeToFit()
        
        let doneButton = UIBarButtonItem(barButtonSystemItem: .done, target: nil, action: #selector(cancelPicker))
        toolBart.setItems([doneButton], animated: true)
        
        self.selectTypePicker.reloadAllComponents()
        tf.inputAccessoryView = toolBart
        tf.inputView = selectTypePicker
        
    }
}

extension addAppointment{
    //showDate Picker
    
    func TimePicker(tf: UITextField){
        
        let toolbar = UIToolbar();
        toolbar.sizeToFit()
        
        //done button & cancel button
        let doneButton = UIBarButtonItem(title: "Done".localized, style: .done, target: self, action: #selector(doneTimePicker))
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: "Cancel".localized, style: .done, target: self, action: #selector(cancelPicker))
        toolbar.setItems([doneButton,spaceButton,cancelButton], animated: false)
        
        tf.inputAccessoryView = toolbar
        tf.inputView = selectTimePicker
    }
    @objc func doneTimePicker(){
        //For date formate
        let formatter = DateFormatter()
        formatter.dateFormat = "h:mm a"
        sharedText.text = formatter.string(from: selectTimePicker.date)
        if sharedText.tag == 11{
            Sat_from_time = sharedText.text!
        }else if sharedText.tag == 12{
            Sun_from_time = sharedText.text!
        }else if sharedText.tag == 13{
            Mon_from_time = sharedText.text!
        }else if sharedText.tag == 14{
            Tue_from_time = sharedText.text!
        }else if sharedText.tag == 15{
            Wed_from_time = sharedText.text!
        }else if sharedText.tag == 16{
            Thu_from_time = sharedText.text!
        }else if sharedText.tag == 17{
            Fri_from_time = sharedText.text!
        }else if sharedText.tag == 21{
            Sat_to_time = sharedText.text!
        }else if sharedText.tag == 22{
            Sun_to_time = sharedText.text!
        }else if sharedText.tag == 23{
            Mon_to_time = sharedText.text!
        }else if sharedText.tag == 24{
            Tue_to_time = sharedText.text!
        }else if sharedText.tag == 25{
            Wed_to_time = sharedText.text!
        }else if sharedText.tag == 26{
            Thu_to_time = sharedText.text!
        }else if sharedText.tag == 27{
            Fri_to_time = sharedText.text!
        }

        self.view.endEditing(true)
    }
}
