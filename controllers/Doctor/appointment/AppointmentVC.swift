//
//  AppointmentVC.swift
//  HealthyPlus
//
//  Created by apple on 8/5/18.
//  Copyright © 2018 Kirollos. All rights reserved.
//

import UIKit
import Alamofire
import Lottie

class AppointmentVC: UIViewController {
    
    var arrAppointment = Array<appointment>()
    
    var lott: LOTAnimationView = LOTAnimationView(name: "Boat_Loader")
    
    @IBOutlet weak var noAppointmentView: UIView!
    @IBOutlet weak var dashedView: UIView!
    @IBOutlet weak var noAppointmentLbl: UILabel!
    @IBOutlet weak var addAppointmentOutlet: UIButton!
    @IBOutlet weak var appointmentTbl: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        appointmentTbl.delegate = self
        appointmentTbl.dataSource = self
        
        dashedView(dashView: dashedView)
        
        setNavigationTitle(Title: "Appointments")
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        arrAppointment.removeAll()
        appointmentTbl.reloadData()
        appointmentTbl.alpha = 0
        addAppointmentOutlet.alpha = 0
    }
    
    override func viewDidAppear(_ animated: Bool) {
        appointmentData()
    }
    
    func handlingView(){
        UIView.animate(withDuration: 0.5) {
            self.noAppointmentLbl.alpha = 1
            self.addAppointmentOutlet.alpha = 1
        }
    }
    
    func appointmentData(){
        lott = playAnimation(vc: self)

        let param = [
            "method": "reservations",
            "language": "\(sharedHandler.getLanguage())",
            "doctor_id": "\(sharedHandler.getUserID())"
        ]
        Alamofire.request(urls.filterInsurance, method: .post, parameters: param, encoding: JSONEncoding(options: []), headers: nil).responseJSON { (response) in
            switch response.result {
                
            case .success:
                if let value = response.result.value as? NSDictionary {
                    if value["state"] as? Int == 101{
                        if let arr = value["reservations"] as? NSArray{
                            
                            for data in arr{
                                var day = " "
                                var day_col = " "
                                var date = " "
                                var have_reservations = false
                                var num_of_reservations = 0
                                
                                if let object = data as? Dictionary <String, Any>{
                                    if let value = object["day"] as? String{
                                        day = value
                                    }
                                    if let value = object["day_col"] as? String{
                                        day_col = value
                                    }
                                    if let valuee = object["date"] as? String{
                                        date = valuee
                                    }
                                    if let value = object["have_reservations"] as? Bool{
                                        have_reservations = value
                                    }
                                    if let valuee = object["num_of_reservations"] as? Int{
                                        num_of_reservations = valuee
                                    }
                                }
                                self.arrAppointment.append(appointment(Day: day, dayName: day_col, date: date, haveReservation: have_reservations, numOfReservation: num_of_reservations))
                            }
                            if self.arrAppointment.count > 0{
                                self.appointmentTbl.reloadData()
                                self.appointmentTbl.alpha = 1
                                self.addAppointmentOutlet.alpha = 1
                                self.handlingView()
                            }else{
                                self.appointmentTbl.alpha = 0
                                self.addAppointmentOutlet.alpha = 0
                            }
                        }
                    }
                    else{
//                        if let status = value["state"] as? Int{
////                            self.otherStatus(Number: status)
//                        }
                    }
                    self.stopAnimation(Lot: self.lott)

                }
            case .failure:
                self.errorInConnection()
                self.stopAnimation(Lot: self.lott)

            }
        }
    }
    
    @IBAction func addAction(_ sender: Any) {
        let vc = UIStoryboard.init(name: "doctor", bundle: nil).instantiateViewController(withIdentifier: "addAppointment") as! addAppointment
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
}

extension AppointmentVC: UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrAppointment.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = appointmentTbl.dequeueReusableCell(withIdentifier: "cell") as! appointmentCell
        customView(custom: cell.appointmentView)
        cell.setData(day: arrAppointment[indexPath.row].dayName, num: arrAppointment[indexPath.row].numOfReservation, date: arrAppointment[indexPath.row].date)
        return cell
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if arrAppointment[indexPath.row].haveReservation{
            let vc = UIStoryboard.init(name: "doctor", bundle: nil).instantiateViewController(withIdentifier: "patientReservationVC") as! patientReservationVC
            vc.date = arrAppointment[indexPath.row].date
            self.navigationController?.pushViewController(vc, animated: true)
        }else{
            showAttentionMessage(msg: "No Reservation To Show")
        }
    }
    
}
