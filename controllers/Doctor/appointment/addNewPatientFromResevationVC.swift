//
//  addNewPatientFromResevationVC.swift
//  HealthyPlus
//
//  Created by apple on 8/13/18.
//  Copyright © 2018 Kirollos. All rights reserved.
//

import UIKit
import Lottie
import Alamofire
import KMPopUp

class addNewPatientFromResevationVC: UIViewController {
    
    var ID   = " "
    var Name = " "
    var datee = " "
    var time = " "
    var patientId = 0
    var isDoctor = false
    var isDelete = false
    weak var timer: Timer?
    var type = "specific time".localized
    var lott: LOTAnimationView = LOTAnimationView(name: "Boat_Loader")
    
    @IBOutlet weak var addView: UIView!
    @IBOutlet weak var gradientBtn: UIButton!
    @IBOutlet weak var patientName: UILabel!
    @IBOutlet weak var MainLbl: UILabel!
    @IBOutlet weak var subLbl: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        showAnimate()
        customView(custom: addView)
        setBtnGradient(btn: gradientBtn, color: colors.appColor, gradColor: colors.gradColor)
        
    }
    override func viewDidAppear(_ animated: Bool) {
        patientName.text = Name
        if isDelete{
            MainLbl.text = "Delete Patient".localized
            subLbl.text  = "Are you sure to delete".localized
        }else if isDoctor{
            MainLbl.text = "Confirm Booking".localized
            subLbl.text  = "Are you sure to Confirm".localized
        }else{
            MainLbl.text = "Add a New Patient".localized
            subLbl.text  = "You Want to Add This Patient".localized
        }
    }
    @IBAction func cancelAction(_ sender: Any) {
        removeAnimation()
    }
    @IBAction func acceptAction(_ sender: Any) {
        if isDelete{
            deletePatient()
        }else if isDoctor{
            confirmBooking(url: urls.healthyPlusURL)
        }else{
            addPatient()
        }
    }
    func addPatient(){
        let param = [
            "method"             : "add_patient_from_reservation",
            "added_by_doctor_id" : "\(sharedHandler.getUserID())",
            "id"                 : "\(ID)"
        ]
        addNewInDoctor(param: param)
    }
    func deletePatient(){
        let param = [
            "method": "delete_patient",
            "id": "\(ID)"
        ]
        addNewInDoctor(param: param)
    }
    func confirmBooking(url: String){
        lott = playAnimation(vc: self)
        let param = [
            "method": "confirm_booking",
            "doctor_id": "\(sharedHandler.getUserID())",
            "date": "\(datee)",
            "type": "specific time",
            "time": "\(time)",
            "patient_id": "\(patientId)"
        ]
        print(param)
        Alamofire.request(url, method: .post, parameters: param, encoding: JSONEncoding(options: []), headers: nil).responseJSON { (response) in
            switch response.result {
                
            case .success:
                if let value = response.result.value as? NSDictionary {
                    if value["state"] as? Int == 101{
                        KMPoUp.ShowMessage(controller: self, message: "Booking Successfully".localized, image: #imageLiteral(resourceName: "success"))
                        self.timer = Timer.scheduledTimer(withTimeInterval: 1.3, repeats: true) { [weak self] _ in
                            self?.navigationController?.popViewController(animated: true)
                        }
                    }else{
                        if let status = value["state"] as? Int{
                            self.otherStatus(Number: status)
                        }
                    }
                    self.stopAnimation(Lot: self.lott)
                }
            case .failure:
                self.errorInConnection()
                self.stopAnimation(Lot: self.lott)
            }
        }
    }
    func showAnimate() {
        self.view.transform = CGAffineTransform(scaleX: 1.3 , y: 1.3)
        self.view.alpha = 0.0;
        UIView.animate(withDuration: 0.25) {
            self.view.alpha = 1.0
            self.view.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
        }
    }
    
    func removeAnimation() {
        UIView.animate(withDuration: 0.25, animations: {
            self.view.transform = CGAffineTransform(scaleX: 1.3 , y: 1.3)
            self.view.alpha = 0.0;
        }) { (finished : Bool) in
            if finished {
                self.view.removeFromSuperview()
            }
        }
    }
}
