//
//  healthyPlusTabBar.swift
//  HealthyPlus
//
//  Created by apple on 7/15/18.
//  Copyright © 2018 Kirollos. All rights reserved.
//

import UIKit

class healthyPlusTabBar: UITabBarController {
    
    var controllerArray = [UINavigationController]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if sharedHandler.getUserType() == 2{
            
            let controller1 = UINavigationController(rootViewController: UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "homeVC") as! homeVC)
            let controller2 = UINavigationController(rootViewController: UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "reservationVC") as! reservationVC)
            let controller3 = UINavigationController(rootViewController: UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "favouriteVC") as! favouriteVC)
            let controller4 = UINavigationController(rootViewController: UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "searchVC") as! searchVC)
            let controller5 = UINavigationController(rootViewController: UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "MoreVC") as! MoreVC)
            
            controller1.navigationBar.barTintColor = colors.appColor
            controller2.navigationBar.barTintColor = colors.appColor
            controller3.navigationBar.barTintColor = colors.appColor
            controller4.navigationBar.barTintColor = colors.appColor
            controller5.navigationBar.barTintColor = colors.appColor
            
            
            
            controller1.navigationBar.tintColor = UIColor.white
            controller2.navigationBar.tintColor = UIColor.white
            controller3.navigationBar.tintColor = UIColor.white
            controller4.navigationBar.tintColor = UIColor.white
            controller5.navigationBar.tintColor = UIColor.white
            
            controller1.tabBarItem.tag = 1
            controller2.tabBarItem.tag = 2
            controller3.tabBarItem.tag = 3
            controller4.tabBarItem.tag = 4
            controller5.tabBarItem.tag = 5
            
            controller1.tabBarItem.image = #imageLiteral(resourceName: "home")
            controller2.tabBarItem.image = #imageLiteral(resourceName: "reservation")
            controller5.tabBarItem.image = #imageLiteral(resourceName: "more")
            
            controller1.tabBarItem.selectedImage = #imageLiteral(resourceName: "home 2")
            controller2.tabBarItem.selectedImage = #imageLiteral(resourceName: "reservationSelection")
            controller5.tabBarItem.selectedImage = #imageLiteral(resourceName: "moreSelection")
            
            controllerArray.append(controller1)
            controllerArray.append(controller2)
            controllerArray.append(controller3)
            controllerArray.append(controller4)
            controllerArray.append(controller5)
            self.setViewControllers(controllerArray, animated: true)
            
            if !sharedHandler.isLogged(){
                tabBar.items![1].isEnabled = false
                tabBar.items![2].isEnabled = false
            }
            
        }else{
            let controller1 = UINavigationController(rootViewController: UIStoryboard(name: "doctor", bundle: nil).instantiateViewController(withIdentifier: "profileVC") as! profileVC)
            let controller2 = UINavigationController(rootViewController: UIStoryboard(name: "doctor", bundle: nil).instantiateViewController(withIdentifier: "patientVC") as! patientVC)
            let controller3 = UINavigationController(rootViewController: UIStoryboard(name: "doctor", bundle: nil).instantiateViewController(withIdentifier: "AppointmentVC") as! AppointmentVC)
            let controller4 = UINavigationController(rootViewController: UIStoryboard(name: "doctor", bundle: nil).instantiateViewController(withIdentifier: "moreForDocVC") as! moreForDocVC)
            
            controller1.navigationBar.barTintColor = colors.appColor
            controller2.navigationBar.barTintColor = colors.appColor
            controller3.navigationBar.barTintColor = colors.appColor
            controller4.navigationBar.barTintColor = colors.appColor
            
            controller1.navigationBar.tintColor = UIColor.white
            controller2.navigationBar.tintColor = UIColor.white
            controller3.navigationBar.tintColor = UIColor.white
            controller4.navigationBar.tintColor = UIColor.white
            
            controllerArray.append(controller1)
            controllerArray.append(controller2)
            controllerArray.append(controller3)
            controllerArray.append(controller4)
            self.setViewControllers(controllerArray, animated: true)
        }
    }
}
