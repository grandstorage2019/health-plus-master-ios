//
//  reservationVC.swift
//  HealthyPlus
//
//  Created by apple on 7/15/18.
//  Copyright © 2018 Kirollos. All rights reserved.
//

import UIKit
import Alamofire
import Lottie

class reservationVC: UIViewController {
    
    var arrReservations = Array<reservation>()
    var lott: LOTAnimationView = LOTAnimationView(name: "Boat_Loader")
    
    @IBOutlet weak var tblReservations: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        pushBarButtonLeft()
        self.navigationController?.navigationBar.tintColor = UIColor.white
        setNavigationTitle(Title: "My Appointment")
        if sharedHandler.getpatientType() == 1{
            reservationData(url: urls.healthyPlusURL, imageURL: urls.healthyPlusReservationImageURL)
        }else{
            reservationData(url: urls.healthyCarURL, imageURL: urls.healthyCarReservationImageURL)
        }
    }
   
    func reservationData(url: String, imageURL: String){
        lott = playAnimation(vc: self)
        let param = [
            "method": "reservations",
            "language": "\(sharedHandler.getLanguage())",
            "patient_id": "\(sharedHandler.getUserID())"
        ]
        Alamofire.request(url, method: .post, parameters: param, encoding: JSONEncoding(options: []), headers: nil).responseJSON { (response) in
            switch response.result {
                
            case .success:
                if let value = response.result.value as? NSDictionary {
                    if value["state"] as? Int == 101{
                        if let arr = value["reservations"] as? NSArray{
                            for data in arr{
                                
                                var reservation_id = 0
                                var dateOrServiceName = " "
                                var Title = " "
                                var subTitle = " "
                                var logo = " "
                                var mobile = " "
                                var address = " "
                                var lat = " "
                                var lang = " "
                                
                                if let object = data as? Dictionary <String, Any>{
                                    if let value = object["reservation_id"] as? Int{
                                        reservation_id = value
                                    }
                                    if let value = object["reservation_id"] as? String{
                                        reservation_id = Int(value)!
                                    }
                                    if let value = object["sub_services_name"] as? String{
                                        subTitle = value
                                    }
                                    if let value = object["care_services_name"] as? String{
                                        Title = value
                                        dateOrServiceName = value
                                    }
                                    if let value = object["area_name"] as? String{
                                        address = value
                                    }
                                    if let value = object["location_comment"] as? String{
                                        address = address + " - \(value)"
                                    }
                                    if let value = object["lang"] as? String{
                                        lang = value
                                    }
                                    if let value = object["lat"] as? String{
                                        lat = value
                                    }
                                    if let value = object["lang"] as? Double{
                                        lang = String(value)
                                    }
                                    if let value = object["lat"] as? Double{
                                        lat = String(value)
                                    }
                                    if let value = object["logo"] as? String{
                                        logo = imageURL + value
                                    }
                                    if let value = object["profile_image"] as? String{
                                        logo = imageURL + value
                                    }
                                    if let value = object["mobile"] as? String{
                                        mobile = value
                                    }
                                    if let value = object["date"] as? String{
                                        dateOrServiceName = value
                                    }
                                    if let value = object["time"] as? String{
                                        dateOrServiceName = dateOrServiceName + " " + value
                                    }
                                    if let value = object["level"] as? String{
                                        subTitle = value
                                    }
                                    if let value = object["title"] as? String{
                                        Title = value
                                    }
                                    if let value = object["doctor_first_name"] as? String{
                                        Title = Title + " " + value
                                    }
                                    if let value = object["doctor_last_name"] as? String{
                                        Title = Title + " " + value
                                    }
                                    if let value = object["clinic_name"] as? String{
                                        subTitle = value
                                    }
                                    if let value = object["street_name"] as? String{
                                        address = value
                                    }
                                    if let value = object["block"] as? String{
                                        address = address + ", " + value
                                    }
                                    if let value = object["building_number"] as? String{
                                        address = address + ", " + value
                                    }
                                    if let value = object["apartment"] as? String{
                                        address = address + ", " + value
                                    }
                                    if let value = object["floor"] as? String{
                                        address = address + ", " + value
                                    }
                                    if let value = object["landmark"] as? String{
                                        address = address + ", " + value
                                    }
                                    if let value = object["phone"] as? String{
                                        mobile = value
                                    }
                                }
                                self.arrReservations.append(reservation(resID: reservation_id, dateOrServiceName: dateOrServiceName, Title: Title, subTitle: subTitle, logo: logo, mobile: mobile, address: address, lat: lat, lang: lang))
                            }
                            self.tblReservations.reloadData()
                        }
                    }else{
                        if let status = value["state"] as? Int{
                            self.otherStatus(Number: status)
                        }
                    }
                    self.stopAnimation(Lot: self.lott)
                }
            case .failure:
                self.errorInConnection()
                self.stopAnimation(Lot: self.lott)
            }
        }
    }
}
extension reservationVC: UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrReservations.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tblReservations.dequeueReusableCell(withIdentifier: "cell") as! reservationCell
        cell.settingData(NameOrDate: arrReservations[indexPath.row].dateOrServiceName, title: arrReservations[indexPath.row].Title, subtitle: arrReservations[indexPath.row].subTitle, Address: arrReservations[indexPath.row].address, Url: arrReservations[indexPath.row].logo)
        customView(custom: cell.cust)
        circleImage(image: cell.resImage)
        setCustomViewGradient(customView: cell.custView, color: colors.appColor, gradColor: colors.gradColor)
        cell.mapBtn.tag = indexPath.row
        cell.mapBtn.addTarget(self, action: #selector(showMapView(sender:)), for: UIControlEvents.touchUpInside)
        
        cell.callBtn.tag = indexPath.row
        cell.callBtn.addTarget(self, action: #selector(Call(sender:)), for: UIControlEvents.touchUpInside)
        
        cell.cancelBtn.tag = indexPath.row
        cell.cancelBtn.addTarget(self, action: #selector(cancelReservation(sender:)), for: UIControlEvents.touchUpInside)
        
        return cell
    }
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    @objc func showMapView(sender: UIButton){
        let data = arrReservations[sender.tag]
        let vc = UIStoryboard.init(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "mapVC") as! mapVC
        if data.lat != " " && data.lang != " "{
            vc.lat = data.lat
            vc.lang = data.lang
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    @objc func Call(sender: UIButton) {
        let data = arrReservations[sender.tag]
        let url : NSURL = URL(string : "TEL://\(data.mobile))")! as NSURL
        UIApplication.shared.open(url as URL, options: [:], completionHandler: nil)
    }
    @objc func cancelReservation(sender: UIButton){
        var url = " "
        let data = arrReservations[sender.tag]
        let param = [
            "method": "delete_reservation",
            "id": "\(data.reservation_id)"
        ]
        if sharedHandler.getpatientType() == 1{
            url = urls.healthyPlusURL
        }else{
            url = urls.healthyCarURL
        }
        lott = playAnimation(vc: self)
        Alamofire.request(url, method: .post, parameters: param, encoding: JSONEncoding(options: []), headers: nil).responseJSON { (response) in
            switch response.result {
                
            case .success:
                if let value = response.result.value as? NSDictionary {
                    if value["state"] as? Int == 101{
                        if value["result"] as? Bool == true{
                            self.arrReservations.remove(at: sender.tag)
                            self.tblReservations.reloadData()
                        }
                    }
                }
            case .failure:
                self.errorInConnection()
                self.stopAnimation(Lot: self.lott)
            }
        }
    }
}
