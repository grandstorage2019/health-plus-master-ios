//
//  mapVC.swift
//  HealthyPlus
//
//  Created by apple on 7/16/18.
//  Copyright © 2018 Kirollos. All rights reserved.
//

import UIKit
import GoogleMaps
import GooglePlaces

class mapVC: UIViewController, GMSMapViewDelegate {
    
    var lat = " "
    var lang = " "
    
    let marker = GMSMarker()
    
    @IBOutlet weak var mapView: GMSMapView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        mapView.delegate = self
        googleMap()
        // Do any additional setup after loading the view.
    }
    func googleMap(){
        
        let camera = GMSCameraPosition.camera(withLatitude: CLLocationDegrees(lat)!, longitude: CLLocationDegrees(lang)!, zoom: 5.5)
        mapView.animate(to: camera)
        
        marker.position = CLLocationCoordinate2D(latitude: CLLocationDegrees(lat)!, longitude: CLLocationDegrees(lang)!)
        marker.map = mapView
    }
}
