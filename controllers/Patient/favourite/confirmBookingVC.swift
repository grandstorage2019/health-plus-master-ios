//
//  confirmBookingVC.swift
//  HealthyPlus
//
//  Created by apple on 7/24/18.
//  Copyright © 2018 Kirollos. All rights reserved.
//

import UIKit
import Alamofire
import Lottie
import KMPopUp

class confirmBookingVC: UIViewController {
    
    weak var timer: Timer?
    
    var time  = " "
    var datee = " "
    var day   = " "
    var typee = "specific time".localized
    
    @IBOutlet weak var date: UILabel!
    @IBOutlet weak var docName: UILabel!
    @IBOutlet weak var timeLbl: UILabel!
    @IBOutlet weak var location: UILabel!
    @IBOutlet weak var viewsLbl: UILabel!
    @IBOutlet weak var patientName: UILabel!
    
    @IBOutlet weak var infoView: UIView!
    @IBOutlet weak var imageView: UIView!
    @IBOutlet weak var detailsView: UIView!
    @IBOutlet weak var confirmBtn: UIButton!
    @IBOutlet weak var doctorImage: UIImageView!
    
    var lott: LOTAnimationView = LOTAnimationView(name: "Boat_Loader")
    
    var data = healthyFavourite(id: 0, doctor_id: 0, profile_image: " ", is_favourite: false, rate: 0, views: 0, raters: 0, fullName: " ", clinic_name: " ", about: " ", price: 0, waitingTime: " ", Location: " ", available_from: " ", is_life_time: " ", spec_id: " ", area_name: " ", flat_number: " ", clinic_number: " ", type: " ", specializations: " ", main_specialization_name: " ", level: " ")
    
    @IBAction func confirmAction(_ sender: Any) {
        confirmBooking(url: urls.healthyPlusURL)
        
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setNavigationTitle(Title: "Confirm Booking")
        settingDate()
    }
    func settingDate(){
        
        date.text        = "\(day) \(datee)"
        timeLbl.text     = time
//        aboutName.text   = data.about
        location.text    = data.Location
        docName.text     = data.fullName
        viewsLbl.text    = "\(data.views) \("view".localized)"
        patientName.text = sharedHandler.getUserName()
        
        customView(custom: infoView)
        customView(custom: detailsView)
        circleImage(image: doctorImage)
        //        circleViewForImage(custom: imageView)
        setImage(url: data.profile_image, Image: doctorImage)
        setBtnGradient(btn: confirmBtn, color: colors.appColor, gradColor: colors.gradColor)
    }
    func confirmBooking(url: String){
        lott = playAnimation(vc: self)
        let param = [
            "method": "confirm_booking",
            "doctor_id": "\(data.doctor_id)",
            "date": "\(datee)",
            "time": "\(time)",
            "type": "\(typee)",
            "patient_id": "\(sharedHandler.getUserID())"
        ]
        Alamofire.request(url, method: .post, parameters: param, encoding: JSONEncoding(options: []), headers: nil).responseJSON { (response) in
            switch response.result {
                
            case .success:
                if let value = response.result.value as? NSDictionary {
                    if value["state"] as? Int == 101{
                        KMPoUp.ShowMessage(controller: self, message: "Booking Successfully".localized, image: #imageLiteral(resourceName: "success"))
                        self.timer = Timer.scheduledTimer(withTimeInterval: 1.3, repeats: true) { [weak self] _ in
                            self?.navigationController?.popViewController(animated: true)
                        }
                    }else{
                        if let status = value["state"] as? Int{
                            self.otherStatus(Number: status)
                        }
                    }
                    self.stopAnimation(Lot: self.lott)
                }
            case .failure:
                self.errorInConnection()
                self.stopAnimation(Lot: self.lott)
            }
        }
    }
}
