 //
//  doctorInformationVC.swift
//  HealthyPlus
//
//  Created by apple on 7/18/18.
//  Copyright © 2018 Kirollos. All rights reserved.
//

import UIKit
import Alamofire
import Lottie

class doctorInformationVC: UIViewController {
    
    var lott: LOTAnimationView = LOTAnimationView(name: "Boat_Loader")
    
    var data = healthyFavourite(id: 0, doctor_id: 0, profile_image: " ", is_favourite: false, rate: 0, views: 0, raters: 0, fullName: " ", clinic_name: " ", about: " ", price: 0, waitingTime: " ", Location: " ", available_from: " ", is_life_time: " ", spec_id: " ", area_name: " ", flat_number: " ", clinic_number: " ", type: " ", specializations: " ", main_specialization_name: " ", level: " ")
    var arrEnDays = ["Saturday","Sunday","Monday","Tuesday","Wednesday","Thursday","Friday"]
    var arrArDays = ["السبت","الاحد","الأثنين","الثلاثاء","الأربعاء","الخميس","الجمعة"]
    
    var arrTimes = Array<Times>()
    var arrServices = Array<services>()
    
    @IBOutlet weak var fav1: UIButton!
    @IBOutlet weak var fav2: UIButton!
    @IBOutlet weak var fav3: UIButton!
    @IBOutlet weak var fav4: UIButton!
    @IBOutlet weak var fav5: UIButton!
    
    @IBOutlet weak var infoView: UIView!
    @IBOutlet weak var imageView: UIView!
    @IBOutlet weak var detailsView: UIView!
    @IBOutlet weak var locationView: UIView!
    @IBOutlet weak var servicesView: UIView!
    
    @IBOutlet weak var price: UILabel!
    @IBOutlet weak var favBtn: UIButton!
    @IBOutlet weak var docName: UILabel!
    @IBOutlet weak var viewslbl: UILabel!
    @IBOutlet weak var docAbout: UILabel!
    @IBOutlet weak var bookType: UILabel!
    @IBOutlet weak var docRaters: UILabel!
    @IBOutlet weak var mainLocaion: UILabel!
    @IBOutlet weak var subLocation: UILabel!
    @IBOutlet weak var docInformation: UILabel!
    
    @IBOutlet weak var leftArrow: UIImageView!
    @IBOutlet weak var rightArrow: UIImageView!
    @IBOutlet weak var doctorImage: UIImageView!
    
    @IBOutlet weak var colBooking: UICollectionView!
    @IBOutlet weak var servicesCol: UICollectionView!
    @IBOutlet weak var servicesHeight: NSLayoutConstraint!
    
    @IBOutlet weak var bookingViewHeight: NSLayoutConstraint!
    
    @IBAction func favAction(_ sender: Any) {
        if sharedHandler.isLogged(){
            var method = " "
            if data.is_favourite{
                method = "delete_favourite"
            }else{
                method = "add_favourite"
            }
            favAction(method: method)
        }else{
            showAttentionMessage(msg: "Please Login First")
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        settingData()
        
        servicesCol.delegate = self
        servicesCol.dataSource = self
        
        colBooking.delegate = self
        colBooking.dataSource = self
        
        setNavigationTitle(Title: "Dr. Information")
        
        if sharedHandler.getpatientType() == 1{
            FavouriteData(url: urls.healthyPlusURL)
        }else{
            FavouriteData(url: urls.healthyCarURL)
        }
    }
    func settingData(){
        if data.waitingTime == "Book First".localized{
            bookType.text = "\(data.waitingTime)"
        }else{
            bookType.text = "wait".localized + ": \(data.waitingTime)"
        }
        docName.text = data.fullName
        docInformation.text = data.about
        mainLocaion.text = data.area_name
        viewslbl.text = "\(data.views) \("views".localized)"
        price.text = "Detection Price".localized + " : \(data.price) \("KD".localized)"
        subLocation.text = "\(data.Location)"
        docAbout.text = data.level
        docRaters.text = "(\(String(data.raters)) \("People Who Visited".localized)"
        
        if data.is_favourite{
            favBtn.setImage(#imageLiteral(resourceName: "heartSelection"), for: .normal)
        }
        
        customView(custom: infoView)
        circleImage(image: doctorImage)
        customView(custom: detailsView)
        customView(custom: locationView)
        customView(custom: servicesView)
        circleViewForImage(custom: imageView)
        setImage(url: data.profile_image, Image: doctorImage)
        settingFillingRate(rate: Double(data.rate), btn: [fav1, fav2, fav3, fav4, fav5])
    }
    func favAction(method: String){
        lott = playAnimation(vc: self)
        let param = [
            "method": "\(method)",
            "doctor_id" : "\(data.doctor_id)",
            "patient_id": "\(sharedHandler.getUserID())"
        ]
        Alamofire.request(urls.healthyPlusURL, method: .post, parameters: param, encoding: JSONEncoding(options: []), headers: nil).responseJSON { (response) in
            switch response.result {
                
            case .success:
                if let value = response.result.value as? NSDictionary {
                    if value["state"] as? Int == 101{
                        if sharedHandler.getpatientType() == 1{
                            if self.data.is_favourite{
                                self.data.is_favourite = false
                                self.favBtn.setImage(#imageLiteral(resourceName: "heart"), for: .normal)
                            }else{
                                self.data.is_favourite = true
                                self.favBtn.setImage(#imageLiteral(resourceName: "heartSelection"), for: .normal)
                            }
                        }
                    }
                    else if let status = value["state"] as? Int{
                        self.otherStatus(Number: status)
                    }
                }
            case .failure:
                self.errorInConnection()
            }
        }
    }
    
    func FavouriteData(url: String){
        lott = playAnimation(vc: self)
        let param = [
            "method": "doctor_info",
            "language": "\(sharedHandler.getLanguage())",
            "doctor_id": "\(data.doctor_id)"
        ]
        Alamofire.request(url, method: .post, parameters: param, encoding: JSONEncoding(options: []), headers: nil).responseJSON { (response) in
            switch response.result {
                
            case .success:
                if let value = response.result.value as? NSDictionary {
                    if value["state"] as? Int == 101{
                        if let arr = value["times"] as? NSArray{
                            for data in arr{
                                
                                var day = " "
                                var from_time = " "
                                var to_time = " "
                                var waiting_time = " "
                                var date = " "
                                var time_type = " "
                                
                                if let object = data as? Dictionary <String, Any>{
                                    if let value = object["day"] as? String{
                                        day = value
                                        if sharedHandler.getLanguage() == "ar"{
                                            for i in 0..<self.arrEnDays.count{
                                                if day == self.arrEnDays[i]{
                                                    day = self.arrArDays[i]
                                                    break
                                                }
                                            }
                                        }
                                    }
                                    if let value = object["from_time"] as? String{
                                        from_time = value
                                        if sharedHandler.getLanguage() == "ar"{
                                            if from_time.contains("AM"){
                                                let newString = from_time.replacingOccurrences(of: "AM", with: "ص", options: .literal, range: nil)
                                                from_time = newString
                                            }
                                            if from_time.contains("PM"){
                                                let newString = from_time.replacingOccurrences(of: "PM", with: "م", options: .literal, range: nil)
                                                from_time = newString
                                            }
                                        }
                                    }
                                    if let value = object["to_time"] as? String{
                                        to_time = value
                                        if sharedHandler.getLanguage() == "ar"{
                                            if to_time.contains("AM"){
                                                let newString = to_time.replacingOccurrences(of: "AM", with: "ص", options: .literal, range: nil)
                                                to_time = newString
                                            }
                                            if to_time.contains("PM"){
                                                let newString = to_time.replacingOccurrences(of: "PM", with: "م", options: .literal, range: nil)
                                                to_time = newString
                                            }
                                        }
                                    }
                                    if let value = object["waiting_time"] as? String{
                                        waiting_time = value
                                    }
                                    if let value = object["date"] as? String{
                                        date = value
                                    }
                                    if let value = object["time_type"] as? String{
                                        time_type = value
                                    }
                                }
                                self.arrTimes.append(Times(day: day, from_time: from_time, to_time: to_time, waiting_time: waiting_time, date: date, time_type: time_type))
                            }
                        }
                        if self.arrTimes.count > 0{
                            self.colBooking.reloadData()
                        }else{
                            self.bookingViewHeight.constant = 0.0
                            self.leftArrow.alpha = 0
                            self.rightArrow.alpha = 0
                        }
                        if let arr = value["doctor_services"] as? NSArray{
                            for data in arr{
                                
                                var id = 0
                                var name = " "
                                
                                if let object = data as? Dictionary <String, Any>{
                                    if let value = object["id"] as? Int{
                                        id = value
                                    }
                                    if let value = object["name"] as? String{
                                        name = value
                                    }
                                }
                                self.arrServices.append(services(id: id, name: name))
                            }
                            if self.arrServices.count > 0 {
                                self.servicesCol.reloadData()
                            }
                            if self.arrServices.count % 2 == 0{
                                self.servicesHeight.constant = CGFloat((self.arrServices.count / 2) * 50)
                            }else{
                                self.servicesHeight.constant = CGFloat(((self.arrServices.count + 1) / 2) * 50)
                            }
                        }
                    }else{
                        if let status = value["state"] as? Int{
                            self.otherStatus(Number: status)
                        }
                    }
                    self.stopAnimation(Lot: self.lott)
                }
            case .failure:
                self.errorInConnection()
                self.stopAnimation(Lot: self.lott)
            }
        }
    }
}
extension doctorInformationVC: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView.tag == 0{
            return arrTimes.count
        }else{
            return arrServices.count
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if collectionView.tag == 0{
            let width = (colBooking.layer.frame.size.width - 10)/2
            return CGSize(width: width, height: 150.0)
        }else{
            
            //return CGSize(width: 100, height: 150.0)
            let size = self.view.layer.frame.width / 2
            return CGSize(width: size - 25, height: 40)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if collectionView.tag == 0{
            let cell = colBooking.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! bookCell
            cell.settingdata(Day: arrTimes[indexPath.row].day, Date: arrTimes[indexPath.row].date, From: arrTimes[indexPath.row].from_time, To: arrTimes[indexPath.row].to_time)
            return cell
        }else{
            let cell = servicesCol.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! servicesCell
            cell.designing(name: arrServices[indexPath.row].name)
            return cell
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if collectionView == colBooking{
            if sharedHandler.isLogged(){
                if arrTimes[indexPath.row].waiting_time != " "{
                    let vc = UIStoryboard.init(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "bookingTimeVC") as! bookingTimeVC
                    vc.data = data
                    vc.doctorID = data.doctor_id
                    vc.times = arrTimes[indexPath.row]
                    self.navigationController?.pushViewController(vc, animated: true)
                }else{
                    let vc = UIStoryboard.init(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "confirmBookingVC") as! confirmBookingVC
                    vc.time = "Book First"
                    vc.typee = "Book First"
                    vc.datee = arrTimes[indexPath.row].date
                    vc.day = arrTimes[indexPath.row].day
                    
                    vc.data = data
                    self.navigationController?.pushViewController(vc, animated: true)
                }
            }else{
                self.showAttentionMessage(msg: "Please Login First")
            }
        }
    }
}
