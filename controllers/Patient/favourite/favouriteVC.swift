//
//  favouriteVC.swift
//  HealthyPlus
//
//  Created by apple on 7/15/18.
//  Copyright © 2018 Kirollos. All rights reserved.
//

import UIKit
import Alamofire
import Lottie

class favouriteVC: UIViewController {
    
    var txt = ""
    var areaID = 0
    var specializationID = 0
    var navigation = "Favourite"
    var arrInsurance = Array<services>()
    var arrCareFavourites = Array<careFavourites>()
    var arrHealthFavourites = Array<healthyFavourite>()
    
    var lott: LOTAnimationView = LOTAnimationView(name: "Boat_Loader")
    
    @IBOutlet weak var tblfavourite: UITableView!
    @IBOutlet weak var picker: UIPickerView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        pushBarButtonLeft()
        
        picker.delegate = self
        picker.dataSource = self
        tblfavourite.delegate = self
        tblfavourite.dataSource = self
        
        setNavigationTitle(Title: navigation)
        
        if areaID != 0 || txt != " "{
            var barButton = UIBarButtonItem()
            barButton = UIBarButtonItem(image: UIImage(named: "filter"), landscapeImagePhone: nil, style: .done, target: self, action: #selector(filterData))
            self.navigationItem.rightBarButtonItem = barButton
        }
    }
    override func viewDidAppear(_ animated: Bool) {
        arrCareFavourites.removeAll()
        arrHealthFavourites.removeAll()
        tblfavourite.reloadData()
        if areaID != 0{
            if sharedHandler.getpatientType() == 1{
                FavouriteData(url: urls.healthyPlusURL, imageURL: urls.healthyPlusReservationImageURL, method: "search", searchByText: "", insuranceID: -1)
            }else{
                FavouriteData(url: urls.healthyCarURL, imageURL: urls.healthyCarReservationImageURL, method: "search", searchByText: "", insuranceID: -1)
            }
        }else if txt != ""{
            if sharedHandler.getpatientType() == 1{
                FavouriteData(url: urls.healthyPlusURL, imageURL: urls.healthyPlusReservationImageURL, method: "search_by_doctor", searchByText: txt, insuranceID: -1)
            }else{
                FavouriteData(url: urls.healthyCarURL, imageURL: urls.healthyCarReservationImageURL, method: "search_by_name", searchByText: txt, insuranceID: -1)
            }
        }
        else{
            if sharedHandler.getpatientType() == 1{
                FavouriteData(url: urls.healthyPlusURL, imageURL: urls.healthyPlusReservationImageURL, method: "favourites", searchByText: "", insuranceID: -1)
            }else{
                FavouriteData(url: urls.healthyCarURL, imageURL: urls.healthyCarReservationImageURL, method: "favourites", searchByText: "", insuranceID: -1)
            }
        }
    }
    
    @objc func filterData(){
        picker.alpha = 1
        insuranceData(url: urls.filterInsurance)
    }
    func insuranceData(url: String){
        arrInsurance.removeAll()
        self.picker.reloadAllComponents()
        lott = playAnimation(vc: self)
        let param = [
            "method": "insurances",
            "language": "\(sharedHandler.getLanguage())",
        ]
        Alamofire.request(url, method: .post, parameters: param, encoding: JSONEncoding(options: []), headers: nil).responseJSON { (response) in
            switch response.result {
                
            case .success:
                if let value = response.result.value as? NSDictionary {
                    if value["state"] as? Int == 101{
                        self.arrInsurance.append(services(id: -1, name: "All"))
                        if let arr = value["insurance_companies"] as? NSArray{
                            for data in arr{
                                
                                var id = 0
                                var name = " "
                                
                                if let object = data as? Dictionary <String, Any>{
                                    if let value = object["id"] as? Int{
                                        id = value
                                    }
                                    if let value = object["name"] as? String{
                                        name = value
                                    }
                                }
                                self.arrInsurance.append(services(id: id, name: name))
                            }
                            self.picker.reloadAllComponents()
                        }
                    }
                    else{
                        if let status = value["state"] as? Int{
                            self.otherStatus(Number: status)
                        }
                    }
                    self.stopAnimation(Lot: self.lott)
                }
            case .failure:
                self.errorInConnection()
                self.stopAnimation(Lot: self.lott)
            }
        }
    }
    
    func FavouriteData(url: String, imageURL: String, method: String, searchByText: String, insuranceID: Int){
        arrCareFavourites.removeAll()
        arrHealthFavourites.removeAll()
        tblfavourite.reloadData()
        
        lott = playAnimation(vc: self)
        let param = [
            "method": "\(method)",
            "language": "\(sharedHandler.getLanguage())",
            "patient_id": "\(sharedHandler.getUserID())",
            "area_id": "\(areaID)",
            "specialization_id": "\(specializationID)",
            "insurance_id": "\(insuranceID)",
            "query": "\(searchByText)"
        ]
        print(param)
        Alamofire.request(url, method: .post, parameters: param, encoding: JSONEncoding(options: []), headers: nil).responseJSON { (response) in
            switch response.result {
                
            case .success:
                if let value = response.result.value as? NSDictionary {
                    if value["state"] as? Int == 101{
                        if let arr = value["cares"] as? NSArray{
                            for data in arr{
                                
                                var care_id = 0
                                var care_logo = " "
                                var care_name = " "
                                var about = " "
                                var rate = 0.0
                                var views = 0
                                var is_favourite = false
                                var Location = " "
                                var phone = " "
                                var date = " "
                                var facebook = " "
                                var instagram = " "
                                var twitter = " "
                                
                                if let object = data as? Dictionary <String, Any>{
                                    if let value = object["care_logo"] as? String{
                                        care_logo = imageURL + value
                                    }
                                    if let value = object["care_id"] as? Int{
                                        care_id = value
                                    }
                                    if let value = object["care_name"] as? String{
                                        care_name = value
                                    }
                                    if let value = object["about"] as? String{
                                        about = value
                                    }
                                    if let value = object["governorate_name"] as? String{
                                        Location = value
                                    }
                                    if let value = object["area_name"] as? String{
                                        Location = Location + " - \(value)"
                                    }
                                    if let value = object["location_comment"] as? String{
                                        Location = Location + " - \(value)"
                                    }
                                    if let value = object["rate"] as? Double{
                                        rate = value
                                    }
                                    if let value = object["is_favourite"] as? Bool{
                                        is_favourite = value
                                    }
                                    if let value = object["views"] as? Int{
                                        views = value
                                    }
                                    
                                    if let value = object["phone"] as? String{
                                        phone = value
                                    }
                                    if let value = object["from_time"] as? String{
                                        date = value
                                    }
                                    if let value = object["to_time"] as? String{
                                        date = date + " : " + value
                                    }
                                    if let value = object["facebook"] as? String{
                                        facebook = value
                                    }
                                    if let value = object["instagram"] as? String{
                                        instagram = value
                                    }
                                    if let value = object["twitter"] as? String{
                                        twitter = value
                                    }
                                }
                                self.arrCareFavourites.append(careFavourites(id: care_id, logo: care_logo, name: care_name, About: about, rate: rate, views: views, isFav: is_favourite, location: Location, Phone: phone, date: date, facbook: facebook, Instagram: instagram, twitter: twitter))
                            }
                           
                        }
                        else if let arr = value["doctors"] as? NSArray{
                            for data in arr{
                                
                                var id = 0
                                var doctor_id = 0
                                var profile_image = " "
                                var is_favourite = false
                                var rate = 0
                                var views = 0
                                var raters = 0
                                var fullName = " "
                                var clinic_name = " "
                                var about = " "
                                var price = 0
                                var hour = " "
                                var waitingTime = " "
                                var Location = " "
                                var available_from = " "
                                
                                var is_life_time = " "
                                var spec_id = " "
                                var area_name = " "
                                var flat_number = " "
                                var clinic_number = " "
                                var type = " "
                                var specializations = " "
                                var main_specialization_name = " "
                                var level = " "
                                
                                if let object = data as? Dictionary <String, Any>{
                                    if let value = object["id"] as? String{
                                        id = Int(value)!
                                    }
                                    if let value = object["doctor_id"] as? Int{
                                        doctor_id = value
                                    }
                                    if let value = object["profile_image"] as? String{
                                        profile_image = imageURL + value
                                    }
                                    if let value = object["rate"] as? Int{
                                        rate = value
                                    }
                                    if let value = object["views"] as? String{
                                        views = Int(value)!
                                    }
                                    if let value = object["level"] as? String{
                                        level = value
                                    }
                                    
                                    if let value = object["raters"] as? Int{
                                        raters = value
                                    }
                                    if let value = object["title"] as? String{
                                        fullName = value
                                    }
                                    if let value = object["first_name"] as? String{
                                        fullName = fullName + " " +  value
                                    }
                                    if let value = object["last_name"] as? String{
                                        fullName = fullName + " " + value
                                    }
                                    if let value = object["views"] as? Int{
                                        views = value
                                    }
                                    if let value = object["price"] as? Int{
                                        price = value
                                    }
                                    if let value = object["about"] as? String{
                                        about = value
                                    }
                                    if let value = object["waiting_hours"] as? Int{
                                        hour = String(value)
                                    }
                                    if let value = object["waiting_minutes"] as? Int{
                                        if hour == "0" && value == 0{
                                            waitingTime = "Book First".localized
                                        }else{
                                            waitingTime = waitingTime + "\("H".localized) : " + String(value) + " \("M".localized)"
                                        }
                                    }
                                    if let value = object["area_name"] as? String{
                                        Location = value + ","
                                    }
                                    if let value = object["street_name"] as? String{
                                        Location = Location + value + ","
                                    }
                                    if let value = object["block"] as? String{
                                        Location =  Location + value + ","
                                    }
                                    if let value = object["building_number"] as? String{
                                        Location = Location + value + ","
                                    }
                                    if let value = object["apartment"] as? String{
                                        Location = Location + value + ","
                                    }
                                    if let value = object["floor"] as? String{
                                        Location = Location + value + ","
                                    }
                                    if let value = object["landmark"] as? String{
                                        Location = Location + value
                                    }
                                    if let value = object["available_from"] as? String{
                                        available_from = value
                                    }
                                    if let value = object["is_favourite"] as? Bool{
                                        is_favourite = value
                                    }
                                    if let value = object["clinic_name"] as? String{
                                        clinic_name = value
                                    }
                                    if let value = object["is_life_time"] as? String{
                                        is_life_time = value
                                    }
                                    if let value = object["spec_id"] as? String{
                                        spec_id =  value
                                    }
                                    if let value = object["area_name"] as? String{
                                        area_name = value
                                    }
                                    if let value = object["flat_number"] as? String{
                                        flat_number = value
                                    }
                                    if let value = object["clinic_number"] as? String{
                                        clinic_number = value
                                    }
                                    if let value = object["type"] as? String{
                                        type = value
                                    }
                                    if let value = object["specializations"] as? String{
                                        specializations = value
                                    }
                                    if let value = object["main_specialization_name"] as? String{
                                        main_specialization_name = value
                                    }
                                }
                                self.arrHealthFavourites.append(healthyFavourite(id: id, doctor_id: doctor_id, profile_image: profile_image, is_favourite: is_favourite, rate: rate, views: views, raters: raters, fullName: fullName, clinic_name: clinic_name, about: about, price: price, waitingTime: waitingTime, Location: Location, available_from: available_from, is_life_time: is_life_time, spec_id: spec_id, area_name: area_name, flat_number: flat_number, clinic_number: clinic_name, type: type, specializations: specializations, main_specialization_name: main_specialization_name, level: level))
                            }
                            
                        }
                        self.tblfavourite.reloadData()
                        if self.arrHealthFavourites.count <= 0 && self.arrCareFavourites.count <= 0{
                            self.tblfavourite.alpha = 0
                        }
                    }
                    else{
                        if let status = value["state"] as? Int{
                            self.otherStatus(Number: status)
                            self.tblfavourite.alpha = 0
                        }
                    }
                    
                    self.stopAnimation(Lot: self.lott)
                }
            case .failure:
                self.errorInConnection()
                self.stopAnimation(Lot: self.lott)
            }
        }
    }
}
extension favouriteVC: UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if sharedHandler.getpatientType() == 1{
            return arrHealthFavourites.count
        }else{
            return arrCareFavourites.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if sharedHandler.getpatientType() == 1{
            let cell = tblfavourite.dequeueReusableCell(withIdentifier: "cellHealthy") as! healthyFavouriteCell
            
            cell.settingData(image: arrHealthFavourites[indexPath.row].profile_image, fullName: arrHealthFavourites[indexPath.row].fullName, Clinic: arrHealthFavourites[indexPath.row].clinic_name, Views: String(arrHealthFavourites[indexPath.row].raters ), About: arrHealthFavourites[indexPath.row].about, Price: String(arrHealthFavourites[indexPath.row].price), Waiting: arrHealthFavourites[indexPath.row].waitingTime, Location: arrHealthFavourites[indexPath.row].Location, available: arrHealthFavourites[indexPath.row].available_from, isFav: arrHealthFavourites[indexPath.row].is_favourite, specialization: arrHealthFavourites[indexPath.row].level)
            customView(custom: cell.cust)
            
            cell.favouriteBtn.tag = indexPath.row
            cell.favouriteBtn.addTarget(self, action: #selector(addFavourite(sender:)), for: UIControlEvents.touchUpInside)
            
            cell.bookNow.tag = indexPath.row
            cell.bookNow.addTarget(self, action: #selector(docInformation(sender:)), for: UIControlEvents.touchUpInside)
            
            circleImage(image: cell.healthyInage)
            setBtnGradient(btn: cell.btnBookView, color: colors.appColor, gradColor: colors.gradColor)
            settingFillingRate(rate: Double(arrHealthFavourites[indexPath.row].rate), btn: [cell.fav1, cell.fav2, cell.fav3, cell.fav4, cell.fav5])
            
            return cell
        }else{
            let cell = tblfavourite.dequeueReusableCell(withIdentifier: "cellCare") as! careFavouriteCell
            cell.settingData(Name: arrCareFavourites[indexPath.row].care_name, views: String(arrCareFavourites[indexPath.row].views), location: arrCareFavourites[indexPath.row].Location, Phone: arrCareFavourites[indexPath.row].phone, date: arrCareFavourites[indexPath.row].date, des: arrCareFavourites[indexPath.row].date, imageURLL: arrCareFavourites[indexPath.row].care_logo, isFav: arrCareFavourites[indexPath.row].is_favourite)
            circleImage(image: cell.ImageFavourite)
            customView(custom: cell.cust)
            
            cell.favouriteBtm.tag = indexPath.row
            cell.favouriteBtm.addTarget(self, action: #selector(addFavourite(sender:)), for: UIControlEvents.touchUpInside)
            
            
            return cell
        }
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if sharedHandler.getpatientType() == 2{
            let vc = UIStoryboard.init(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "careInformationsVC") as! careInformationsVC
            
            vc.imageUrl = arrCareFavourites[indexPath.row].care_logo
            vc.isFav    = arrCareFavourites[indexPath.row].is_favourite
            vc.mainname = arrCareFavourites[indexPath.row].care_name
            vc.rater    = arrCareFavourites[indexPath.row].rate
            vc.subname  = arrCareFavourites[indexPath.row].about
            vc.views    = arrCareFavourites[indexPath.row].views
            vc.careID   = arrCareFavourites[indexPath.row].care_id
            vc.location = arrCareFavourites[indexPath.row].Location
            vc.facebook = arrCareFavourites[indexPath.row].facebook
            vc.insta    = arrCareFavourites[indexPath.row].instagram
            vc.twitter  = arrCareFavourites[indexPath.row].twitter
            vc.phone    = arrCareFavourites[indexPath.row].phone
//            vc.waiting  = arrCareFavourites[indexPath.row].
            self.navigationController?.pushViewController(vc, animated: true)
        }else{
            tableView.deselectRow(at: indexPath, animated: true)
        }
    }
    @objc func docInformation(sender: UIButton){
        let data = arrHealthFavourites[sender.tag]
        
        let vc = UIStoryboard.init(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "doctorInformationVC") as! doctorInformationVC
        vc.data = data
        self.navigationController?.pushViewController(vc, animated: true)
        arrHealthFavourites.removeAll()
        tblfavourite.reloadData()
    }
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    @objc func addFavourite(sender: UIButton){
        if sharedHandler.isLogged(){
            lott = playAnimation(vc: self)
            var data1: careFavourites
            var data2: healthyFavourite
            var typeID = " "
            var URL = " "
            var id = 0
            var method = "delete_favourite"
            var isfav = false
            
            if sharedHandler.getpatientType() == 1{
                data2 = arrHealthFavourites[sender.tag]
                typeID = "doctor_id"
                URL = urls.healthyPlusURL
                id = data2.doctor_id
                isfav = data2.is_favourite
                
                if data2.is_favourite{
                    method = "delete_favourite"
                }else{
                    method = "add_favourite"
                }
            }else{
                data1 = arrCareFavourites[sender.tag]
                typeID = "care_id"
                URL = urls.healthyCarURL
                id = data1.care_id
                isfav = data1.is_favourite
                
                if data1.is_favourite{
                    method = "delete_favourite"
                }else{
                    method = "add_favourite"
                }
            }
            let param = [
                "method": "\(method)",
                "\(typeID)" : "\(id)",
                "patient_id": "\(sharedHandler.getUserID())"
            ]
            Alamofire.request(URL, method: .post, parameters: param, encoding: JSONEncoding(options: []), headers: nil).responseJSON { (response) in
                switch response.result {
                    
                case .success:
                    if let value = response.result.value as? NSDictionary {
                        if value["state"] as? Int == 101{
                            if isfav{
                                isfav = false
                                sender.setImage(#imageLiteral(resourceName: "heart"), for: .normal)
                            }else{
                                isfav = true
                                sender.setImage(#imageLiteral(resourceName: "heartSelection"), for: .normal)
                            }
                            if self.arrCareFavourites.count > 0{
                                self.arrCareFavourites[sender.tag].is_favourite = isfav
                            }
                            else {
                                self.arrHealthFavourites[sender.tag].is_favourite = isfav
                            }
                            if self.areaID == 0 && self.txt == " "{
                                if sharedHandler.getpatientType() == 1{
                                    self.arrHealthFavourites.remove(at: sender.tag)
                                }else{
                                    self.arrCareFavourites.remove(at: sender.tag)
                                }
                                self.tblfavourite.reloadData()
                            }
                        }
                        else if let status = value["state"] as? Int{
                            self.otherStatus(Number: status)
                        }
                        self.stopAnimation(Lot: self.lott)
                    }
                case .failure:
                    self.errorInConnection()
                    self.stopAnimation(Lot: self.lott)
                }
            }
        }else{
            self.showAttentionMessage(msg: "Please Login First")
        }
    }
}

extension favouriteVC:  UIPickerViewDelegate, UIPickerViewDataSource{
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return arrInsurance.count
    }
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return arrInsurance[row].name
    }
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        if arrInsurance.count > 0{
            picker.alpha = 0
            if areaID != 0{
                if sharedHandler.getpatientType() == 1{
                    FavouriteData(url: urls.healthyPlusURL, imageURL: urls.healthyPlusReservationImageURL, method: "search", searchByText: " ", insuranceID: arrInsurance[row].id)
                }else{
                    FavouriteData(url: urls.healthyCarURL, imageURL: urls.healthyCarURL, method: "search", searchByText: " ", insuranceID: arrInsurance[row].id)
                }
            }else if txt != ""{
                if sharedHandler.getpatientType() == 1{
                    FavouriteData(url: urls.healthyPlusURL, imageURL: urls.healthyPlusReservationImageURL, method: "search_by_doctor", searchByText: txt, insuranceID: arrInsurance[row].id)
                }else{
                    FavouriteData(url: urls.healthyCarURL, imageURL: urls.healthyCarURL, method: "search_by_name", searchByText: txt, insuranceID: arrInsurance[row].id)
                }
            }
        }else{
            picker.alpha = 0
        }
    }
}
