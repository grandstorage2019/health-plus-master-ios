//
//  zoomedImageVC.swift
//  HealthyPlus
//
//  Created by apple on 8/14/18.
//  Copyright © 2018 Kirollos. All rights reserved.
//

import UIKit

class zoomedImageVC: UIViewController {
    
    var zoomedImage = " "
    
    @IBOutlet weak var scrooling: UIScrollView!
    @IBOutlet weak var zoomingImage: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setImage(url: zoomedImage, Image: zoomingImage)
    }
}
