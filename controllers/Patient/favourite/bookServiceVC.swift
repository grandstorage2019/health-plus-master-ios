//
//  bookServiceVC.swift
//  HealthyPlus
//
//  Created by apple on 8/14/18.
//  Copyright © 2018 Kirollos. All rights reserved.
//

import UIKit
import Alamofire
import Lottie
import KMPopUp

class bookServiceVC: UIViewController {
    
    weak var timer: Timer?
    var serviceId = 0
    var lott: LOTAnimationView = LOTAnimationView(name: "Boat_Loader")

    @IBOutlet weak var gradBtn: UIButton!
    
    @IBAction func bookAction(_ sender: Any) {
        booking()
    }
    
    @IBAction func cancelAction(_ sender: Any) {
        removeAnimation()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        showAnimate()
        setBtnGradient(btn: gradBtn, color: colors.appColor, gradColor: colors.gradColor)
        gradBtn.layer.cornerRadius = 0
    }
    func showAnimate() {
        self.view.transform = CGAffineTransform(scaleX: 1.3 , y: 1.3)
        self.view.alpha = 0.0;
        UIView.animate(withDuration: 0.25) {
            self.view.alpha = 1.0
            self.view.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
        }
    }
    func removeAnimation() {
        UIView.animate(withDuration: 0.25, animations: {
            self.view.transform = CGAffineTransform(scaleX: 1.3 , y: 1.3)
            self.view.alpha = 0.0;
        }) { (finished : Bool) in
            if finished {
                self.view.removeFromSuperview()
            }
        }
    }
    func booking(){
        lott = playAnimation(vc: self)

        let param = [
            "method": "add_service",
            "care_sub_services_id": "\(serviceId)",
            "patient_id" : "\(sharedHandler.getUserID())"
        ]
        Alamofire.request(urls.healthyCarURL, method: .post, parameters: param, encoding: JSONEncoding(options: []), headers: nil).responseJSON { (response) in
            switch response.result {
                
            case .success:
                if let value = response.result.value as? NSDictionary {
                    if value["state"] as? Int == 101{
                        KMPoUp.ShowMessage(controller: self, message: "Booking Successfully".localized, image: #imageLiteral(resourceName: "success"))
                        self.timer = Timer.scheduledTimer(withTimeInterval: 1.3, repeats: true) { [weak self] _ in
                            self?.removeAnimation()
                        }
                    }else{
                        if let status = value["state"] as? Int{
                            self.otherStatus(Number: status)
                        }
                    }
                    self.stopAnimation(Lot: self.lott)

                }
            case .failure:
                self.errorInConnection()
                self.stopAnimation(Lot: self.lott)

            }
        }
    }
}
