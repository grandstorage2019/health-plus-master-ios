//
//  careInformationsVC.swift
//  HealthyPlus
//
//  Created by Donna on 7/20/18.
//  Copyright © 2018 Kirollos. All rights reserved.
//

import UIKit
import Alamofire
import ExpyTableView
import Lottie

class careInformationsVC: UIViewController {
    
    var views = 0
    var careID = 0
    var rater = 0.0
    var insuranceID = 0
    
    var phone = " "
    var insta = " "
    var twitter = " "
    var waiting = " "
    var subname = " "
    var mainname = " "
    var imageUrl = " "
    var facebook = " "
    var location = " "
    
    var isFav = false
    var arrRate = Array<rating>()
    var arrOffers = Array<String>()
    var arrImages = Array<String>()
    var arrCareServices = Array<careServices>()
    
    var lott: LOTAnimationView = LOTAnimationView(name: "Boat_Loader")
    
    @IBOutlet weak var Views: UILabel!
    @IBOutlet weak var raters: UILabel!
    @IBOutlet weak var subName: UILabel!
    @IBOutlet weak var mainName: UILabel!
    @IBOutlet weak var phoneLbl: UILabel!
    @IBOutlet weak var waitingLbl: UILabel!
    @IBOutlet weak var twitterLbl: UILabel!
    @IBOutlet weak var facebookLbl: UILabel!
    @IBOutlet weak var LocationLbl: UILabel!
    @IBOutlet weak var instagramLbl: UILabel!
    
    @IBOutlet weak var infoView: UIView!
    @IBOutlet weak var viewImage: UIView!
    @IBOutlet weak var offerView: UIView!
    @IBOutlet weak var ratingView: UIView!
    @IBOutlet weak var detailsView: UIView!
    @IBOutlet weak var PicturesView: UIView!
    @IBOutlet weak var contactUsView: UIView!
    @IBOutlet weak var servicesView: UIView!
    
    @IBOutlet weak var fav1: UIButton!
    @IBOutlet weak var fav2: UIButton!
    @IBOutlet weak var fav3: UIButton!
    @IBOutlet weak var fav4: UIButton!
    @IBOutlet weak var fav5: UIButton!
    @IBOutlet weak var favOutlet: UIButton!
    @IBOutlet weak var rateUsOutlet: UIButton!
    
    @IBOutlet weak var careImage: UIImageView!
    @IBOutlet weak var tblRaters: UITableView!
    @IBOutlet weak var tblOffers: UITableView!
    @IBOutlet weak var tblHeight: NSLayoutConstraint!
    @IBOutlet weak var collImages: UICollectionView!
    @IBOutlet weak var segmentedOutlet: UISegmentedControl!
    @IBOutlet weak var servicesExpandableTbl: ExpyTableView!
    
    @IBAction func rateUsAction(_ sender: Any) {
        if sharedHandler.isLogged(){
            let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "rateUsVC") as! rateUsVC
            vc.careID = careID
            self.addChildViewController(vc)
            vc.view.frame = self.view.frame
            self.view.addSubview(vc.view)
            vc.didMove(toParentViewController: self)
        }else{
            self.showAttentionMessage(msg: "Please Login First")
        }
    }
    @IBAction func segmentedAtion(_ sender: Any) {
        if segmentedOutlet.selectedSegmentIndex == 0{
            infoView.alpha = 0
            offerView.alpha = 0
            ratingView.alpha = 0
            servicesView.alpha = 1
        }else if segmentedOutlet.selectedSegmentIndex == 1{
            offerView.alpha = 1
            infoView.alpha = 0
            ratingView.alpha = 0
            servicesView.alpha = 0
        }else if segmentedOutlet.selectedSegmentIndex == 2{
            infoView.alpha = 1
            offerView.alpha = 0
            ratingView.alpha = 0
            servicesView.alpha = 0
        }else if segmentedOutlet.selectedSegmentIndex == 3{
            infoView.alpha = 0
            offerView.alpha = 0
            ratingView.alpha = 1
            servicesView.alpha = 0
        }
    }
    @IBAction func favAction(_ sender: Any) {
        var method = " "
        if isFav{
            method = "delete_favourite"
        }else{
            method = "add_favourite"
        }
        favAction(method: method)
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        refreshRating()
        tblOffers.delegate = self
        tblOffers.dataSource = self
        
        tblRaters.delegate = self
        tblRaters.dataSource = self
        
        servicesExpandableTbl.delegate = self
        servicesExpandableTbl.dataSource = self
        
        settingData()
        getServices()
        getInfo(url: urls.healthyCarURL)
        getOffers(url: urls.healthyCarURL)
        getRating(url: urls.healthyCarURL)
        setBtnGradient(btn: rateUsOutlet, color: colors.appColor, gradColor: colors.gradColor)
        
        setNavigationTitle(Title: "Information")
    }
    
    func refreshRating(){
        NotificationCenter.default.addObserver(forName: NSNotification.Name("Refresh"), object: nil, queue: nil) { notification in
            self.getRating(url: urls.healthyCarURL)
        }
    }
    
    func settingData(){
        
        circleImage(image: careImage)
        customView(custom: offerView)
        customView(custom: detailsView)
        customView(custom: PicturesView)
        customView(custom: servicesView)
        customView(custom: contactUsView)
        circleViewForImage(custom: viewImage)
        setImage(url: imageUrl, Image: careImage)
        
        Views.text    = "\(views) " + "view".localized
        
        subName.text  = subname
        mainName.text = mainname
        LocationLbl.text = location
        facebookLbl.text = facebook
        instagramLbl.text = insta
        twitterLbl.text = twitter
        phoneLbl.text = phone
//        waitingLbl.text = waiting
        
        segmentedOutlet.layer.masksToBounds = false
        segmentedOutlet.layer.shadowOffset = CGSize(width: 0, height: 1)
        segmentedOutlet.layer.shadowRadius = 5
        segmentedOutlet.layer.shadowOpacity = 0.2
        
        if isFav {
            favOutlet.setImage(UIImage(named: "heartSelection"), for: .normal)
        }
    }
    func favAction(method: String){
        lott = playAnimation(vc: self)
        let param = [
            "method": "\(method)",
            "care_id" : "\(careID)",
            "patient_id": "\(sharedHandler.getUserID())"
        ]
        Alamofire.request(urls.healthyCarURL, method: .post, parameters: param, encoding: JSONEncoding(options: []), headers: nil).responseJSON { (response) in
            switch response.result {
                
            case .success:
                if let value = response.result.value as? NSDictionary {
                    if value["state"] as? Int == 101{
                        if sharedHandler.getpatientType() == 2{
                            if self.isFav{
                                self.isFav = false
                                self.favOutlet.setImage(#imageLiteral(resourceName: "heart"), for: .normal)
                            }else{
                                self.isFav = true
                                self.favOutlet.setImage(#imageLiteral(resourceName: "heartSelection"), for: .normal)
                            }
                        }
                    }
                    else if let status = value["state"] as? Int{
                        self.otherStatus(Number: status)
                    }
                    self.stopAnimation(Lot: self.lott)
                }
            case .failure:
                self.errorInConnection()
                self.stopAnimation(Lot: self.lott)
            }
        }
    }
    func getServices(){
        lott = playAnimation(vc: self)
        let param = [
            "method": "services",
            "care_id" : "\(careID)",
            "language": "\(sharedHandler.getLanguage())"
        ]
        Alamofire.request(urls.healthyCarURL, method: .post, parameters: param, encoding: JSONEncoding(options: []), headers: nil).responseJSON { (response) in
            switch response.result {
            case .success:
                if let value = response.result.value as? NSDictionary {
                    if value["state"] as? Int == 101{
                        if let arr = value["services"] as? NSArray{
                            for data in arr{
                                
                                var id = 0
                                var name = " "
                                var logo = " "
                                var sub_services = Array<subCareServices>()
                                
                                if let object = data as? Dictionary <String, Any>{
                                    if let value = object["id"] as? Int{
                                        id = value
                                    }
                                    if let value = object["name"] as? String{
                                        name = value
                                    }
                                    if let value = object["logo"] as? String{
                                        logo = urls.careServicesImageURL + value
                                    }
                                    if let arrSub_services = object["sub_services"] as? NSArray{
                                        for services in arrSub_services{
                                            if let obj = services as? Dictionary <String, Any>{
                                                
                                                var id = 0
                                                var image = " "
                                                var quantity = " "
                                                var price = " "
                                                var name = " "
                                                var unit = " "
                                                
                                                if let value = obj["id"] as? Int{
                                                    id = value
                                                }
                                                if let value = obj["image"] as? String{
                                                    image = urls.carSubServicesImageURL + value
                                                }
                                                if let value = obj["quantity"] as? String{
                                                    quantity = value
                                                }
                                                if let value = obj["price"] as? Int{
                                                    price = "\(value)"
                                                }
                                                if let value = obj["name"] as? String{
                                                    name = value
                                                }
                                                if let value = obj["unit"] as? String{
                                                    unit = value
                                                }
                                                sub_services.append(subCareServices(id: id, image: image, quantity: quantity, price: price, name: name, unit: unit))
                                            }
                                        }
                                    }
                                }
                                self.arrCareServices.append(careServices(id: id, name: name, logo: logo, subServices: sub_services))
                            }
                            self.servicesExpandableTbl.reloadData()
                            //                            self.tblHeight.constant = CGFloat(100 * self.arrRate.count)
                        }
                    }else if let status = value["state"] as? Int{
                        self.otherStatus(Number: status)
                    }
                }
            case .failure:
                self.errorInConnection()
            }
        }
    }
    func getRating(url: String){
        let param = [
            "method": "rating",
            "language": "\(sharedHandler.getLanguage())",
            "care_id": "\(careID)"
        ]
        Alamofire.request(url, method: .post, parameters: param, encoding: JSONEncoding(options: []), headers: nil).responseJSON { (response) in
            switch response.result {
                
            case .success:
                if let value = response.result.value as? NSDictionary {
                    if value["state"] as? Int == 101{
                        self.arrRate = []
                        self.tblRaters.reloadData()
                        if let arr = value["rating"] as? NSArray{
                            for data in arr{
                                
                                var name = " "
                                var com = " "
                                var star = 0
                                
                                if let object = data as? Dictionary <String, Any>{
                                    if let value = object["name"] as? String{
                                        name = value
                                    }
                                    if let value = object["stars"] as? Int{
                                        star = value
                                    }
                                    if let value = object["comment"] as? String{
                                        com = value
                                    }
                                }
                                self.arrRate.append(rating(name: name, com: com, star: star))
                            }
                            self.tblRaters.reloadData()
                            self.tblHeight.constant = CGFloat(100 * self.arrRate.count)
                        }
                        if let value = value["total_rating"] as? Double{
                            self.settingFillingRate(rate: value, btn: [self.fav1, self.fav2, self.fav3, self.fav4, self.fav5])
                        }
                        if let value = value["raters"] as? Double{
                            self.raters.text   = "( \(value) \("People Who Visited".localized) )"
                        }
                    }else{
                        if let status = value["state"] as? Int{
                            self.otherStatus(Number: status)
                        }
                    }
                }
            case .failure:
                self.errorInConnection()
            }
        }
    }
    func getInfo(url: String){
        let param = [
            "method": "care_pictures",
            "care_id": "\(careID)"
        ]
        Alamofire.request(url, method: .post, parameters: param, encoding: JSONEncoding(options: []), headers: nil).responseJSON { (response) in
            switch response.result {
                
            case .success:
                if let value = response.result.value as? NSDictionary {
                    if value["state"] as? Int == 101{
                        if let arr = value["care_pictures"] as? NSArray{
                            for data in arr{
                                
                                if let object = data as? Dictionary <String, Any>{
                                    if var value = object["image"] as? String{
                                        value = urls.healthyCareOffersImage + value
                                        self.arrImages.append(value)
                                    }
                                }
                            }
                            self.collImages.reloadData()
                        }
                    }else{
                        if let status = value["state"] as? Int{
                            self.otherStatus(Number: status)
                        }
                    }
                }
            case .failure:
                self.errorInConnection()
            }
        }
    }
    func getOffers(url: String){
        let param = [
            "method": "care_offers",
            "care_id": "\(careID)",
            "language": "\(sharedHandler.getLanguage())"
        ]
        Alamofire.request(url, method: .post, parameters: param, encoding: JSONEncoding(options: []), headers: nil).responseJSON { (response) in
            switch response.result {
                
            case .success:
                if let value = response.result.value as? NSDictionary {
                    if value["state"] as? Int == 101{
                        if let arr = value["care_offers"] as? NSArray{
                            for data in arr{
                                
                                if let object = data as? Dictionary <String, Any>{
                                    if var value = object["image"] as? String{
                                        value = urls.healthyCareOffersImage + value
                                        self.arrOffers.append(value)
                                    }
                                }
                            }
                            self.tblOffers.reloadData()
                        }
                    }else{
                        if let status = value["state"] as? Int{
                            self.otherStatus(Number: status)
                        }
                    }
                    self.stopAnimation(Lot: self.lott)
                }
            case .failure:
                self.errorInConnection()
                self.stopAnimation(Lot: self.lott)
            }
        }
    }
}

extension careInformationsVC: UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView == tblRaters{
            return arrRate.count
        }else if tableView == tblOffers{
            return arrOffers.count
        }else{
            return arrCareServices[section].sub_services.count + 1
        }
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if tableView == tblRaters{
            let cell = tblRaters.dequeueReusableCell(withIdentifier: "cell") as! rateCell
            
            customView(custom: cell.cellView)
            cell.SettingData(name: arrRate[indexPath.row].Name, commenttxt: arrRate[indexPath.row].Comment)
            settingFillingRate(rate: Double(arrRate[indexPath.row].Stars), btn: [cell.fav1, cell.fav2, cell.fav3, cell.fav4, cell.fav5])
            
            return cell
        }else if tableView == tblOffers{
            let cell = tblOffers.dequeueReusableCell(withIdentifier: "cell") as! offersCell
            
            cell.showAction.tag = indexPath.row
            cell.showAction.addTarget(self, action: #selector(ShowOfferAtion(sender:)), for: .touchUpInside)
            setImage(url: arrOffers[indexPath.row], Image: cell.offerImage)
            
            return cell
        }else{
            let cell = servicesExpandableTbl.dequeueReusableCell(withIdentifier: String(describing: servicesExpandCell.self)) as! servicesExpandCell
            if arrCareServices[indexPath.section].sub_services.count > 0{
                
                cell.hourLbl.text = "\(arrCareServices[indexPath.section].sub_services[0].quantity) \(arrCareServices[indexPath.section].sub_services[0].unit)"
                cell.moneyLbl.text = arrCareServices[indexPath.section].sub_services[0].price
                cell.nameLbl.text = arrCareServices[indexPath.section].sub_services[0].name
                cell.addBtn.tag = arrCareServices[indexPath.section].sub_services[0].id
                self.setImage(url: arrCareServices[indexPath.section].sub_services[0].image, Image: cell.imageServices)
                cell.addBtn.addTarget(self, action: #selector(addService(sender:)), for: .touchUpInside)
            }
            return cell
        }
    }
    
    @objc func addService(sender: UIButton){
        let vc = UIStoryboard.init(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "bookServiceVC") as! bookServiceVC
        vc.serviceId = sender.tag
        self.addChildViewController(vc)
        vc.view.frame = self.view.frame
        self.view.addSubview(vc.view)
        vc.didMove(toParentViewController: self)
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
    @objc func ShowOfferAtion(sender: UIButton){
        print("showImage")
    }
}

extension careInformationsVC: UICollectionViewDelegate, UICollectionViewDataSource{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return arrImages.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collImages.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! imagesCell
        cell.setImage(url: arrImages[indexPath.row])
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        collectionView.deselectItem(at: indexPath, animated: true)
        let vc = UIStoryboard.init(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "zoomedImageVC") as! zoomedImageVC
        vc.zoomedImage = arrImages[indexPath.row]
        self.navigationController?.pushViewController(vc, animated: true)
    }
}

extension careInformationsVC: ExpyTableViewDataSource{
    
    func tableView(_ tableView: ExpyTableView, expandableCellForSection section: Int) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: servicesHeader.self)) as! servicesHeader
        cell.layoutMargins = UIEdgeInsets.zero
        cell.servicesName.text = arrCareServices[section].name
        
        self.setImage(url: arrCareServices[section].logo, Image: cell.servicesImage)
        return cell
    }
    func tableView(_ tableView: ExpyTableView, canExpandSection section: Int) -> Bool {
        return true
    }
    
}

extension careInformationsVC{
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if tableView == tblRaters{
            return CGFloat(100)
        }else if tableView == tblOffers{
            return CGFloat(215)
        }else{
            return UITableViewAutomaticDimension
        }
        
    }
}

extension careInformationsVC:  ExpyTableViewDelegate{
    
    func tableView(_ tableView: ExpyTableView, expyState state: ExpyState, changeForSection section: Int) {
        
        switch state {
        case .willExpand:
            print("WILL EXPAND")
            
        case .willCollapse:
            print("WILL COLLAPSE")
            
        case .didExpand:
            print("DID EXPAND")
            
        case .didCollapse:
            print("DID COLLAPSE")
        }
    }
    
}

extension careInformationsVC{
    func numberOfSections(in tableView: UITableView) -> Int {
        if tableView == servicesExpandableTbl{
            return arrCareServices.count
        }else{
            return 1
        }
    }
}
