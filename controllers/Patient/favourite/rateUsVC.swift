//
//  rateUsVC.swift
//  HealthyPlus
//
//  Created by apple on 7/22/18.
//  Copyright © 2018 Kirollos. All rights reserved.
//

import UIKit
import Alamofire
import Lottie

class rateUsVC: UIViewController {
    
    var rate = 0.0
    var careID = 0
    
    var lott: LOTAnimationView = LOTAnimationView(name: "Boat_Loader")
    
    @IBOutlet weak var fav1: UIButton!
    @IBOutlet weak var fav2: UIButton!
    @IBOutlet weak var fav3: UIButton!
    @IBOutlet weak var fav4: UIButton!
    @IBOutlet weak var fav5: UIButton!
    
    @IBOutlet weak var rateView: UIView!
    @IBOutlet weak var rateTxt: UITextField!
    @IBOutlet weak var sendOutlet: UIButton!
    @IBOutlet weak var cancelOutlet: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        showAnimate()
        self.view.backgroundColor = UIColor.black.withAlphaComponent(0.8)
        
        customView(custom: rateView)
        
        sendOutlet.layer.cornerRadius = 10
        cancelOutlet.layer.cornerRadius = 10
        
        setBtnGradient(btn: sendOutlet, color: colors.appColor, gradColor: colors.gradColor)
        setBtnGradient(btn: cancelOutlet, color: UIColor.gray, gradColor: UIColor.lightGray)
    }
    @IBAction func rateAction(_ sender: UIButton) {
        rate = Double(sender.tag)
        settingFillingRate(rate: rate, btn: [fav1, fav2, fav3, fav4, fav5])
    }
    @IBAction func cancelAction(_ sender: Any) {
        removeAnimation()
    }
    @IBAction func sendAction(_ sender: Any) {
        sendRate(url: urls.healthyCarURL)
    }
    func showAnimate() {
        self.view.transform = CGAffineTransform(scaleX: 1.3 , y: 1.3)
        self.view.alpha = 0.0;
        UIView.animate(withDuration: 0.25) {
            self.view.alpha = 1.0
            self.view.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
        }
    }
    func removeAnimation() {
        UIView.animate(withDuration: 0.25, animations: {
            self.view.transform = CGAffineTransform(scaleX: 1.3 , y: 1.3)
            self.view.alpha = 0.0;
        }) { (finished : Bool) in
            if finished {
                self.view.removeFromSuperview()
            }
        }
    }
    func sendRate(url: String){
        lott = playAnimation(vc: self)
        let param = [
            "method": "rate",
            "stars": "\(rate)",
            "comment": "\(rateTxt.text ?? " ")",
            "care_id": "\(careID)",
            "patient_id": "\(sharedHandler.getUserID())"
        ]
        Alamofire.request(url, method: .post, parameters: param, encoding: JSONEncoding(options: []), headers: nil).responseJSON { (response) in
            switch response.result {
                
            case .success:
                if let value = response.result.value as? NSDictionary {
                    if value["state"] as? Int == 101{
                        self.removeAnimation()
                        self.perform(#selector(self.refreshProfileView), with: nil, afterDelay: 3.2)
                    }else{
                        if let status = value["state"] as? Int{
                            self.otherStatus(Number: status)
                        }
                    }
                    self.stopAnimation(Lot: self.lott)
                }
            case .failure:
                self.errorInConnection()
                self.stopAnimation(Lot: self.lott)
            }
        }
    }
    
    @objc func refreshProfileView() {
        NotificationCenter.default.post(name: NSNotification.Name("Refresh"), object: nil)
    }
}
