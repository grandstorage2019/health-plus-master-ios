//
//  bookingTimeVC.swift
//  HealthyPlus
//
//  Created by apple on 7/23/18.
//  Copyright © 2018 Kirollos. All rights reserved.
//

import UIKit
import Alamofire
import Lottie

class bookingTimeVC: UIViewController {
    
    var doctorID = 0
    var patientID = 0
    var isDoctor = false
    var lott: LOTAnimationView = LOTAnimationView(name: "Boat_Loader")

    var times  = Times(day: " ", from_time: " ", to_time: " ", waiting_time: " ", date: " ", time_type: " ")
    var arrBookingTime = Array<bookingTime>()
    var data = healthyFavourite(id: 0, doctor_id: 0, profile_image: " ", is_favourite: false, rate: 0, views: 0, raters: 0, fullName: " ", clinic_name: " ", about: " ", price: 0, waitingTime: " ", Location: " ", available_from: " ", is_life_time: " ", spec_id: " ", area_name: " ", flat_number: " ", clinic_number: " ", type: " ", specializations: " ", main_specialization_name: " ", level: " ")
    
    @IBOutlet weak var colBookinTime: UICollectionView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setNavigationTitle(Title: "Dr. Times")
        colBookinTime.delegate = self
        colBookinTime.dataSource = self
    }
    override func viewDidAppear(_ animated: Bool) {
        getBookingTime(url: urls.healthyPlusURL)
    }
    override func viewWillDisappear(_ animated: Bool) {
        arrBookingTime.removeAll()
        colBookinTime.reloadData()
    }
    func getBookingTime(url: String){
        lott = playAnimation(vc: self)

        let param = [
            "method": "booking_times",
            "language": "\(sharedHandler.getLanguage())",
            "doctor_id": "\(doctorID)",
            "from": "\(times.from_time)",
            "to": "\(times.to_time)",
            "waiting_time": "\(times.waiting_time)",
            "date": "\(times.date)"
        ]
        Alamofire.request(url, method: .post, parameters: param, encoding: JSONEncoding(options: []), headers: nil).responseJSON { (response) in
            switch response.result {
                
            case .success:
                if let value = response.result.value as? NSDictionary {
                    if value["state"] as? Int == 101{
                        if let arr = value["booking_times"] as? NSArray{
                            for data in arr{
                                
                                var time = " "
                                var isBook = false
                                
                                if let object = data as? Dictionary <String, Any>{
                                    if let value = object["time"] as? String{
                                        time = value
                                    }
                                    if let value = object["is_booked"] as? Bool{
                                        isBook = value
                                    }
                                }
                                self.arrBookingTime.append(bookingTime(time: time, isBooked: isBook))
                            }
                            self.colBookinTime.reloadData()
                        }
                    }else{
                        if let status = value["state"] as? Int{
                            self.otherStatus(Number: status)
                        }
                    }
                    self.stopAnimation(Lot: self.lott)
                }
            case .failure:
                self.errorInConnection()
                self.stopAnimation(Lot: self.lott)

            }
        }
    }
}
extension bookingTimeVC: UICollectionViewDelegate, UICollectionViewDataSource{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return arrBookingTime.count
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = colBookinTime.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! servicesCell
        if arrBookingTime[indexPath.row].isBooked{
            cell.backgroundColor = colors.gradColor
        }else{
            cell.layer.backgroundColor = colors.appColor.cgColor
        }
        cell.servicesName.text = arrBookingTime[indexPath.row].time
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if !isDoctor{
            if !arrBookingTime[indexPath.row].isBooked{
                let vc = UIStoryboard.init(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "confirmBookingVC") as! confirmBookingVC
                vc.time = arrBookingTime[indexPath.row].time
                vc.datee = times.date
                vc.day = times.day
                vc.data = data
                self.navigationController?.pushViewController(vc, animated: true)
            }
        }else if isDoctor && !arrBookingTime[indexPath.row].isBooked{
            let vc = UIStoryboard.init(name: "doctor", bundle: nil).instantiateViewController(withIdentifier: "addNewPatientFromResevationVC") as! addNewPatientFromResevationVC
            vc.ID   = String(patientID)
            vc.Name = " "
            vc.datee = times.date
            vc.time = arrBookingTime[indexPath.row].time
            vc.isDoctor = true
            vc.patientId = patientID
            self.addChildViewController(vc)
            vc.view.frame = self.view.frame
            self.view.addSubview(vc.view)
            vc.didMove(toParentViewController: self)
        }
    }
}
