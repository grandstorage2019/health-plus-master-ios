//
//  MoreVC.swift
//  HealthyPlus
//
//  Created by apple on 7/15/18.
//  Copyright © 2018 Kirollos. All rights reserved.
//



import UIKit
import Photos
import BSImagePicker

class MoreVC: UIViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate {

    var change = false
    var image = UIImage()
    var array = Array<String>()
    var selectedImage = UIImagePickerController()
    
    @IBOutlet weak var tblMore: UITableView!
    
    
    func changeImage(){
        let status = PHPhotoLibrary.authorizationStatus()
        if (status == PHAuthorizationStatus.authorized) {
            selectedImage.delegate = self
            selectedImage.sourceType = UIImagePickerControllerSourceType.photoLibrary
            selectedImage.allowsEditing = false
            self.present(selectedImage, animated: true)
        } else if (status == PHAuthorizationStatus.denied) {
        } else if (status == PHAuthorizationStatus.notDetermined) {
            PHPhotoLibrary.requestAuthorization({ (newStatus) in

                if (newStatus == PHAuthorizationStatus.authorized) {
                    self.selectedImage.delegate = self
                    self.selectedImage.sourceType = UIImagePickerControllerSourceType.photoLibrary
                    self.selectedImage.allowsEditing = false
                    self.present(self.selectedImage, animated: true)
                }
            })
        }
    }
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        if picker == selectedImage{
            if let originalImage = info[UIImagePickerControllerOriginalImage] as? UIImage{
                let vc = UIStoryboard.init(name: "doctor", bundle: nil).instantiateViewController(withIdentifier: "acceptionVC") as! acceptionVC
                vc.selecImage = originalImage
                vc.uploadMethod = "upload_profile_image"
                vc.url = urls.healthyPlusURL
                vc.uploadMethod = "upload_image"
                self.addChildViewController(vc)
                vc.view.frame = self.view.frame
                self.view.addSubview(vc.view)
                self.change = true
                self.image = originalImage
                self.tblMore.reloadRows(at: [IndexPath(row: 0, section: 0)], with: UITableViewRowAnimation.fade)
            }
            self.dismiss(animated: true, completion: nil)
        }
    }
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        self.dismiss(animated: true, completion: nil)
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        pushBarButtonLeft()

        tblMore.delegate = self
        tblMore.dataSource = self

        setNavigationTitle(Title: "More")
//        "Go To Health Plus";
//        "Go To Health Care"
        if !sharedHandler.isLogged(){
            if sharedHandler.getpatientType() == 1{
                array = ["Notifications".localized, "Settings".localized, "Contact US".localized, "About Health Plus".localized, "Go To Health Care".localized, "Share App".localized, "Sign In".localized]
            }else{
                array = ["Notifications".localized, "Settings".localized, "Contact US".localized, "About Health Plus".localized, "Go To Health Plus".localized, "Share App".localized, "Sign In".localized]
            }
            
        }else{
            if sharedHandler.getpatientType() == 1{
                array = ["Notifications".localized, "Settings".localized, "Contact US".localized, "About Health Plus".localized, "Go To Health Care".localized, "Share App".localized, "Sign Out".localized]
            }else{
                array = ["Notifications".localized, "Settings".localized, "Contact US".localized, "About Health Plus".localized, "Go To Health Plus".localized, "Share App".localized, "Sign Out".localized]
            }
            
        }
    }
}
extension MoreVC: UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return array.count + 1
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.row == 0{
            let cell = tblMore.dequeueReusableCell(withIdentifier: "personCell") as! personCell
            cell.delegate = self
            cell.personID.text = "\(sharedHandler.getUserID())"
            cell.personName.text = sharedHandler.getUserName()
            if change{
                cell.personImage.image = image
            }else{
                print(sharedHandler.getimage())
                setImage(url: sharedHandler.getimage(), Image: cell.personImage)
            }
            return cell
        }else{
            let cell = tblMore.dequeueReusableCell(withIdentifier: "cell") as! moreCell
            cell.moreTxt.text = array[indexPath.row - 1]
            return cell
        }
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        switch indexPath.row {
        case 1:
            let vc = UIStoryboard.init(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "notificationVC") as! notificationVC
            self.navigationController?.pushViewController(vc, animated: true)
        case 2:
            let vc = UIStoryboard.init(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "settingsVC") as! settingsVC
            self.navigationController?.pushViewController(vc, animated: true)
        case 3:
            let vc = UIStoryboard.init(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "contactUsVC") as! contactUsVC
            self.navigationController?.pushViewController(vc, animated: true)
        case 4:
            let vc = UIStoryboard.init(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "aboutUsVC") as! aboutUsVC
            self.navigationController?.pushViewController(vc, animated: true)
        case 5:
            if sharedHandler.getpatientType() == 1{
                UserDefaults.standard.set(2, forKey: "patientType")
            }else{
                UserDefaults.standard.set(1, forKey: "patientType")
            }
            pushHome()
        case 6:
            shareApp()
        case 7:
            if sharedHandler.isLogged(){
                UserDefaults.standard.removeObject(forKey: "userType")
                UserDefaults.standard.removeObject(forKey: "patientType")
                UserDefaults.standard.removeObject(forKey: "id")
                UserDefaults.standard.removeObject(forKey: "loginStatus")
                UserDefaults.standard.removeObject(forKey: "image")
                UserDefaults.standard.removeObject(forKey: "CountryID")
                UserDefaults.standard.removeObject(forKey: "birthDate")
                UserDefaults.standard.removeObject(forKey: "gender")
                UserDefaults.standard.removeObject(forKey: "enLname")
                UserDefaults.standard.removeObject(forKey: "arLname")
                UserDefaults.standard.removeObject(forKey: "arFname")
                UserDefaults.standard.removeObject(forKey: "phone")
                UserDefaults.standard.removeObject(forKey: "email")
                UserDefaults.standard.removeObject(forKey: "userName")
                UserDefaults.standard.removeObject(forKey: "isRemember")
                UserDefaults.standard.removeObject(forKey: "SpecilizationID")
                let vc = UIStoryboard.init(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "userTypeVC") as! userTypeVC
                present(vc, animated: true, completion: nil)
            }else{
                let vc = UIStoryboard.init(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "signInVC") as! signInVC
                present(vc, animated: true, completion: nil)
            }
        default:
            return
        }
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if !sharedHandler.isLogged() && indexPath.row == 0{
            return CGFloat(0)
        }else{
            return UITableViewAutomaticDimension
        }
    }
}
