//
//  contactUsVC.swift
//  HealthyPlus
//
//  Created by Donna on 7/20/18.
//  Copyright © 2018 Kirollos. All rights reserved.
//

import UIKit
import Alamofire
import KMPopUp
import Lottie


class contactUsVC: UIViewController, UITextViewDelegate {
    
    var lott: LOTAnimationView = LOTAnimationView(name: "Boat_Loader")

    @IBOutlet weak var commentsTxt: UITextView!
    @IBOutlet weak var sendCommentAction: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        commentsTxt.delegate = self
        
        commentsTxt.layer.cornerRadius = 8
        commentsTxt.layer.borderWidth = 0.5
        commentsTxt.layer.shadowOffset = CGSize(width: 1, height: 2   )
        commentsTxt.layer.shadowColor = UIColor.gray.cgColor
        commentsTxt.layer.shadowOpacity = 0.5
        commentsTxt.layer.shadowRadius = 4
        commentsTxt.layer.borderColor = UIColor.lightGray.cgColor
        
        customView(custom: commentsTxt)
        sendCommentAction.layer.cornerRadius = 8
        setNavigationTitle(Title: "Contact Us")
        setBtnGradient(btn: sendCommentAction, color: colors.appColor, gradColor: colors.gradColor)
    }
    @IBAction func callAction(_ sender: Any) {
        let url : NSURL = URL(string : "TEL://\(+96595505014))")! as NSURL
        UIApplication.shared.open(url as URL, options: [:], completionHandler: nil)
    }
    @IBAction func sendAction(_ sender: Any) {
        if commentsTxt.text!.isEmpty == false{
            contactUs(url: urls.healthyCarURL)
        }
    }
    func textViewDidBeginEditing(_ textView: UITextView) {
        if commentsTxt.text == "Add Your Message Here".localized {
            commentsTxt.text = ""
            commentsTxt.textColor = UIColor.black
        }
    }
    func textViewDidEndEditing(_ textView: UITextView) {
        if commentsTxt.text.isEmpty {
            commentsTxt.text = "Add Your Message Here".localized 
            commentsTxt.textColor = UIColor.lightGray
        }
    }
    func contactUs(url: String){
        lott = playAnimation(vc: self)

        let param = [
            "method": "send_message",
            "message": "\(commentsTxt.text!)",
        ]
        Alamofire.request(url, method: .post, parameters: param, encoding: JSONEncoding(options: []), headers: nil).responseJSON { (response) in
            switch response.result {
                
            case .success:
                if let value = response.result.value as? NSDictionary {
                    if value["state"] as? Int == 101{
                        KMPoUp.ShowMessage(controller: self, message: "Send Success".localized, image: #imageLiteral(resourceName: "success"))
                        self.navigationController?.popViewController(animated: true)
                    }
                    else{
                        if let status = value["state"] as? Int{
                            self.otherStatus(Number: status)
                        }
                    }
                    self.stopAnimation(Lot: self.lott)

                }
            case .failure:
                self.errorInConnection()
                self.stopAnimation(Lot: self.lott)

            }
        }
    }
}
