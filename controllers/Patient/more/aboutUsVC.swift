//
//  aboutUsVC.swift
//  HealthyPlus
//
//  Created by Donna on 7/20/18.
//  Copyright © 2018 Kirollos. All rights reserved.
//

import UIKit
import Alamofire
import Lottie

class aboutUsVC: UIViewController {
    
    
    var lott: LOTAnimationView = LOTAnimationView(name: "Boat_Loader")
    
    @IBOutlet weak var aboutview: UIView!
    @IBOutlet weak var userCanLBL: UILabel!
    @IBOutlet weak var userDetailsLBL: UILabel!
    @IBOutlet weak var aboutHealthLBL: UILabel!
    @IBOutlet weak var healthDetails: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        aboutview.layer.cornerRadius = 8
        aboutview.layer.borderWidth = 0.5
        aboutview.layer.shadowOffset = CGSize(width: 1, height: 2   )
        aboutview.layer.shadowColor = UIColor.gray.cgColor
        aboutview.layer.shadowOpacity = 0.5
        aboutview.layer.shadowRadius = 4
        aboutview.layer.borderColor = UIColor.lightGray.cgColor
        
        getAbout(url: urls.healthyCarURL)
        
        setNavigationTitle(Title: "About Health Plus")
    }
    func getAbout(url: String){
        lott = playAnimation(vc: self)

        let param = [
            "method": "about_us",
            "language": "\(sharedHandler.getLanguage())"
        ]
        Alamofire.request(url, method: .post, parameters: param, encoding: JSONEncoding(options: []), headers: nil).responseJSON { (response) in
            switch response.result {
                
            case .success:
                if let value = response.result.value as? NSDictionary {
                    if value["state"] as? Int == 101{
                        if let arr = value["about_us"] as? NSArray{
                            if let object = arr[0] as? Dictionary <String, Any>{
                                if let value = object["name"] as? String{
                                    self.userCanLBL.text = value
                                }
                                if let value = object["details"] as? String{
                                    self.userDetailsLBL.text = value
                                }
                            }
                            if let object = arr[1] as? Dictionary <String, Any>{
                                if let value = object["name"] as? String{
                                    self.aboutHealthLBL.text = value
                                }
                                if let value = object["details"] as? String{
                                    self.healthDetails.text = value
                                }
                            }
                        }
                    }else{
                        if let status = value["state"] as? Int{
                            self.otherStatus(Number: status)
                        }
                    }
                    self.stopAnimation(Lot: self.lott)

                }
            case .failure:
                self.errorInConnection()
                self.stopAnimation(Lot: self.lott)

            }
        }
    }
}
