//
//  singleNotificationVC.swift
//  HealthyPlus
//
//  Created by apple on 8/14/18.
//  Copyright © 2018 Kirollos. All rights reserved.
//

import UIKit

class singleNotificationVC: UIViewController {
    
    var url = " "
    var NAme = " "
    
    @IBOutlet weak var imageNotification: UIImageView!
    @IBOutlet weak var notificationName: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        notificationName.text = NAme
        setImage(url: url, Image: imageNotification)
    }
}
