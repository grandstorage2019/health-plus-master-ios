//
//  notificationVC.swift
//  HealthyPlus
//
//  Created by apple on 7/16/18.
//  Copyright © 2018 Kirollos. All rights reserved.
//

import UIKit
import Alamofire
import Lottie

class notificationVC: UIViewController {
    
    var lott: LOTAnimationView = LOTAnimationView(name: "Boat_Loader")

    @IBOutlet weak var tblNotification: UITableView!
    var arrNotification = Array<notification>()
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setNavigationTitle(Title: "Notifications")
        if sharedHandler.getpatientType() == 1{
            getNotification(url: urls.healthyPlusURL)
        }else{
            getNotification(url: urls.healthyCarURL)
        }
        
        tblNotification.delegate = self
        tblNotification.dataSource = self
    }
    func getNotification(url: String){
        lott = playAnimation(vc: self)

        let param = [
            "method": "notifications"
        ]
        Alamofire.request(url, method: .post, parameters: param, encoding: JSONEncoding(options: []), headers: nil).responseJSON { (response) in
            switch response.result {
                
            case .success:
                if let value = response.result.value as? NSDictionary {
                    if value["state"] as? Int == 101{
                        if let arr = value["notifications"] as? NSArray{
                            for data in arr{
                                
                                var msg = " "
                                var created = " "
                                var img = " "
                                
                                if let object = data as? Dictionary <String, Any>{
                                    if let value = object["message"] as? String{
                                        msg = value
                                    }
                                    if let value = object["created_at"] as? String{
                                        created = value
                                    }
                                    if let value = object["image"] as? String{
                                        img = urls.notifiactionImages + value
                                    }
                                }
                                self.arrNotification.append(notification(msg: msg, image: img, created: created))
                            }
                            self.tblNotification.reloadData()
                        }
                    }else{
                        if let status = value["state"] as? Int{
                            self.otherStatus(Number: status)
                        }
                    }
                    self.stopAnimation(Lot: self.lott)

                }
            case .failure:
                self.errorInConnection()
                self.stopAnimation(Lot: self.lott)

            }
        }
    }
    
}
extension notificationVC: UITableViewDataSource, UITableViewDelegate{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrNotification.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tblNotification.dequeueReusableCell(withIdentifier: "cell") as! notificationCell
        cell.settingData(date: arrNotification[indexPath.row].created_at, message: arrNotification[indexPath.row].message, Image: arrNotification[indexPath.row].image)
        circleImage(image: cell.imageNotification)
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        let vc = UIStoryboard.init(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "singleNotificationVC") as! singleNotificationVC
        vc.NAme = arrNotification[indexPath.row].message
        vc.url = arrNotification[indexPath.row].image
        self.navigationController?.pushViewController(vc, animated: true)
    }
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
}
