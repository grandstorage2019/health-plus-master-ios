//
//  settingsVC.swift
//  HealthyPlus
//
//  Created by apple on 8/2/18.
//  Copyright © 2018 Kirollos. All rights reserved.
//

import UIKit
import MOLH

class settingsVC: UIViewController {
    
    @IBOutlet weak var changeLangView: UIView!
    @IBOutlet weak var editProfileView: UIView!
    
    @IBOutlet weak var arrowLanguage: UIImageView!
    @IBOutlet weak var arBtn: UIButton!
    @IBOutlet weak var enBtn: UIButton!
    @IBOutlet weak var langView: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        designing()
        setNavigationTitle(Title: "Settings")
        
        if MOLHLanguage.currentAppleLanguage() == "en"{
            enBtn.setImage(#imageLiteral(resourceName: "select"), for: .normal)
        }else{
            arBtn.setImage(#imageLiteral(resourceName: "select"), for: .normal)
        }
    }
    
    func designing(){
        
        customView(custom: changeLangView)
        customView(custom: editProfileView)
        
        if !sharedHandler.isLogged(){
            editProfileView.alpha = 0
        }
    }
    @IBAction func editProfileAction(_ sender: Any) {
        let vc = UIStoryboard.init(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "signuUpVc") as! signuUpVc
        vc.isUpdate = true
        present(vc, animated: true, completion: nil)
    }
    
    @IBAction func changLanguageAction(_ sender: Any) {
        
        if langView.alpha == 0 {
            langView.alpha = 1
        }else{
            langView.alpha = 0
        }
        
    }
    @IBAction func changeLanguage(_ sender: UIButton) {
        if sender.tag == 0 && sharedHandler.getLanguage() != "ar"{
            arBtn.setImage(#imageLiteral(resourceName: "select"), for: .normal)
            enBtn.setImage(#imageLiteral(resourceName: "unSelect"), for: .normal)
            MOLH.setLanguageTo(MOLHLanguage.currentAppleLanguage() == "en" ? "ar" : "en")
            MOLH.reset()
        }else if sender.tag == 1 && sharedHandler.getLanguage() != "en"{
            arBtn.setImage(#imageLiteral(resourceName: "unSelect"), for: .normal)
            enBtn.setImage(#imageLiteral(resourceName: "select"), for: .normal)
            MOLH.setLanguageTo(MOLHLanguage.currentAppleLanguage() == "en" ? "ar" : "en")
            MOLH.reset()
        }
    }
}

