//
//  Colors.swift
//  HealthyPlus
//
//  Created by apple on 7/12/18.
//  Copyright © 2018 Kirollos. All rights reserved.
//

import Foundation
import UIKit

class colors{
    static let appColor = UIColor(displayP3Red: 75/255, green: 102/255, blue: 235/255, alpha: 1)
    static let gradColor = UIColor(displayP3Red: 130/255, green: 160/255, blue: 246/255, alpha: 1)
}
