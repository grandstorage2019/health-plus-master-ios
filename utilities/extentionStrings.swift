//
//  extentionStrings.swift
//  HealthyPlus
//
//  Created by apple on 9/12/18.
//  Copyright © 2018 Kirollos. All rights reserved.
//

import Foundation
import MOLH
extension String{
    var localized: String {
        // ar.lproj
        var lang = "en"
        if MOLHLanguage.isRTLLanguage(){
            lang = "ar"
        }
        
        let path = Bundle.main.path(forResource: lang, ofType: "lproj")
        let bundle = Bundle(path: path!)
        return NSLocalizedString(self, tableName: nil, bundle: bundle!, value: "", comment: "")
        
    }
}
