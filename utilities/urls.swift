//
//  urls.swift
//  HealthyPlus
//
//  Created by apple on 7/15/18.
//  Copyright © 2018 Kirollos. All rights reserved.
//       

import Foundation
class urls {
    
    static let healthyPlusURL = "http://health.my-staff.net/public/web_service/go.php"
    static let healthyCarURL = "http://health.my-staff.net/public/web_service/go_health_care.php"
    static let healthyPlusCareImageURL = "http://health.my-staff.net/public/images/care_adverting/"
    static let healthyPlusHomeImageURL = "http://health.my-staff.net/public/images/doctor_adverting/"
    
    static let healthyPlusReservationImageURL = "http://health.my-staff.net/public/images/doctor_profile/"
    static let healthyCarReservationImageURL = "http://health.my-staff.net/public/images/care/"
    
    static let healthyCareOffersImage = "http://health.my-staff.net/public/images/health_cares/"
    
    static let notifiactionImages = "http://health.my-staff.net/public/images/notification_care/"
    
    static let filterInsurance = "http://health.my-staff.net/public/web_service/go_doctor.php"
    static let clinicImages = "http://health.my-staff.net/public/images/Clinic/"
    static let profileImage = "http://health.my-staff.net/public/images/doctor_profile/"
    static let licensePicture = "http://health.my-staff.net/public/images/license/"
    static let patientImage = "http://health.my-staff.net/public/images/patient_profile/"
    
    static let careServicesImageURL = "http://health.my-staff.net/public/images/care_services/"
    static let carSubServicesImageURL = "http://health.my-staff.net/public/images/CareSubService/"
    static let ad_link_Image = "http://health.my-staff.net/public/images/setting/"
}
