//
//  sharedHandler.swift
//  HealthyPlus
//
//  Created by apple on 7/12/18.
//  Copyright © 2018 Kirollos. All rights reserved.
//

import Foundation
import UIKit
import MOLH
class sharedHandler{
    
    static func isValidAction(textFeilds: [UITextField]) -> Bool {
        
        for myTextFeild in textFeilds {
            if (myTextFeild.text!.isEmpty) {
                return false
            }
        }
        return true
    }
    
    static func getUserType() -> Int {
        let y = UserDefaults.standard.integer(forKey: "userType")
        return y
    }
    static func getpatientType() -> Int{
        let y = UserDefaults.standard.integer(forKey: "patientType")
        return y
    }
    static func getUserID() -> Int{
        let y = UserDefaults.standard.integer(forKey: "id")
        return y
    }
    static func isLogged() -> Bool{
        let y = UserDefaults.standard.object(forKey: "loginStatus") as? Bool ?? false
        return y
    }
    static func specialization() -> Int{
        let y = UserDefaults.standard.object(forKey: "SpecilizationID") as? Int ?? -1
        return y
    }
    static func getimage() -> String{
        let y = UserDefaults.standard.object(forKey: "image") as? String ?? " "
        return y
    }
    static func getCountryID() -> Int{
        let y = UserDefaults.standard.object(forKey: "CountryID") as? Int ?? 0
        return y
    }
    static func getbirthDate() -> String{
        let y = UserDefaults.standard.object(forKey: "birthDate") as? String ?? " "
        return y
    }
    static func getGender() -> String{
        let y = UserDefaults.standard.object(forKey: "gender") as? String ?? " "
        return y
    }
    static func getEnLName() -> String{
        let y = UserDefaults.standard.object(forKey: "enLname") as? String ?? " "
        return y
    }
    static func getArLName() -> String{
        let y = UserDefaults.standard.object(forKey: "arLname") as? String ?? " "
        return y
    }
    static func getArFName() -> String{
        let y = UserDefaults.standard.object(forKey: "arFname") as? String ?? " "
        return y
    }
    static func getPhone() -> String{
        let y = UserDefaults.standard.object(forKey: "phone") as? String ?? " "
        return y
    }
    static func getEmail() -> String{
        let y = UserDefaults.standard.object(forKey: "email") as? String ?? " "
        return y
    }
    static func getUserName() -> String{
        let y = UserDefaults.standard.object(forKey: "userName") as? String ?? " "
        return y
//        if MOLHLanguage.currentAppleLanguage() == "en"{
//            let y = UserDefaults.standard.object(forKey: "userName") as? String ?? " "
//            return y
//        }else{
//            let y = (UserDefaults.standard.object(forKey: "arFname") as? String ?? " ") + " " + (UserDefaults.standard.object(forKey: "arLname") as? String ?? " ")
//            return y
//        }
    }
    static func getLanguage() -> String{
        return MOLHLanguage.currentAppleLanguage()
    }
    static func googleID() -> String{
        //        let y = UserDefaults.standard.integer(forKey: "googleID")
        return "-1"
    }
    static func getDeviceID() -> String{
        return UIDevice.current.identifierForVendor!.uuidString
    }
    static func isRemember() -> Bool{
        let y = UserDefaults.standard.object(forKey: "isRemember") as? Bool ?? false
        return y
    }
}
