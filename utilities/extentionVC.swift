//
//  extentionVC.swift
//  HealthyPlus
//
//  Created by apple on 7/12/18.
//  Copyright © 2018 Kirollos. All rights reserved.
//

import UIKit
import Lottie

import KMPopUp
import Alamofire

extension UIViewController {
    
    func LeadData(id: Int, userName: String, email: String, phone: String, gender: String, birthDate: String, CountryID: Int, image: String, enFname: String,  arFname: String, enLname: String, arLname: String, isNewDoc: Bool, SpecilizationID: Int){
        
        let defaults = UserDefaults.standard
        defaults.set(id, forKey: "id")
        defaults.set(userName, forKey: "userName")
        defaults.set(email, forKey: "email")
        defaults.set(phone, forKey: "phone")
        defaults.set(gender, forKey: "gender")
        defaults.set(birthDate, forKey: "birthDate")
        defaults.set(CountryID, forKey: "CountryID")
        defaults.set(image, forKey: "image")
        defaults.set(true, forKey: "loginStatus")
        defaults.set(enFname, forKey: "enFname")
        defaults.set(enLname, forKey: "enLname")
        defaults.set(arLname, forKey: "arLname")
        defaults.set(arFname, forKey: "arFname")
        defaults.set(SpecilizationID, forKey: "SpecilizationID")
        if isNewDoc{
            performSegue(withIdentifier: "plans", sender: nil)
        }else{
            pushHome()
        }
    }
    
    func showAttentionMessage(msg: String){
        KMPoUp.ShowMessage(controller: self, message: msg.localized, image: #imageLiteral(resourceName: "Attention"))
    }
    
    func pushHome(){
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let jobDetailViewController = storyboard.instantiateViewController(withIdentifier: "healthyPlusTabBar") as! healthyPlusTabBar
        present(jobDetailViewController, animated: true, completion: nil)
    }
    
    func selectUserType(){
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "userTypeVC") as! userTypeVC
        present(vc, animated: true, completion: nil)
    }
    
    func selectPatientType(){
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "pateintType") as! pateintType
        present(vc, animated: true, completion: nil)
    }
    
    func setBtnGradient(btn: UIButton, color: UIColor, gradColor: UIColor){
        let gradientLayer = CAGradientLayer()
        
        btn.layer.cornerRadius = 8
        gradientLayer.colors = [color.cgColor, gradColor.cgColor]
        gradientLayer.startPoint = CGPoint(x: 0, y: 1)
        gradientLayer.endPoint = CGPoint(x: 1, y: 0)
        gradientLayer.frame = btn.bounds
        btn.layer.addSublayer(gradientLayer)
        btn.layer.masksToBounds = true
    }
    
    func setCustomViewGradient(customView: UIView, color: UIColor, gradColor: UIColor){
        let gradientLayer = CAGradientLayer()
        
        gradientLayer.colors = [color.cgColor, gradColor.cgColor]
        gradientLayer.startPoint = CGPoint(x: 0, y: 1)
        gradientLayer.endPoint = CGPoint(x: 1, y: 0)
        gradientLayer.frame = customView.bounds
        customView.layer.insertSublayer(gradientLayer, at: 1)
        
    }
    func errorInConnection(){
        KMPoUp.ShowMessage(controller: self, message: "Error In Connection".localized, image: #imageLiteral(resourceName: "Attention"))
    }
    func otherStatus(Number: Int){
        
        if Number == 102{
//            KMPoUp.ShowMessage(controller: self, message: "Empty Data".localized, image: #imageLiteral(resourceName: "Attention"))
        }else if Number == 103{
            KMPoUp.ShowMessage(controller: self, message: "email is wrong".localized, image: #imageLiteral(resourceName: "Attention"))
        }else if Number == 104{
            errorInConnection()
        }else if Number == 105{
            KMPoUp.ShowMessage(controller: self, message: "email is used before".localized, image: #imageLiteral(resourceName: "Attention"))
        }else if Number == 106{
            KMPoUp.ShowMessage(controller: self, message: "Password Is Wrong".localized, image: #imageLiteral(resourceName: "Attention"))
        }
    }
    func setImage(url: String, image: UIImageView) {
        DispatchQueue.global().async {
            image.sd_setImage(with: URL(string: url))
        }
    }
    func circleImage(image: UIImageView){
        //        image.layer.borderWidth = 1
        image.layer.masksToBounds = false
        //        image.layer.borderColor = colors.appColor.cgColor
        image.layer.cornerRadius = image.frame.height/2
        image.clipsToBounds = true
    }
    
    func settingFillingRate(rate: Double, btn: [UIButton]){
        var i = 1.0
        var index = 0
        
        while i <= rate{
            
            btn[index].setImage(UIImage(named: "starSelection"), for: .normal)
            i += 1
            index += 1
        }
        if i == rate + 1{
            while i <= 5{
                btn[index].setImage(UIImage(named: "star"), for: .normal)
                i += 1
                index += 1
            }
        }
    }
    func setImage(url: String, Image: UIImageView) {
        DispatchQueue.global().async {
            if url != " "{
                Image.sd_setImage(with: URL(string: url))
            }
        }
    }
    func customView(custom: UIView){
        custom.clipsToBounds = false
        custom.layer.cornerRadius = 8
        custom.layer.borderWidth = 0.5
        custom.layer.shadowOffset = CGSize(width: 1, height: 2)
        custom.layer.shadowColor = UIColor.gray.cgColor
        custom.layer.shadowOpacity = 0.5
        custom.layer.shadowRadius = 4
        custom.layer.borderColor = UIColor.lightGray.cgColor
    }
    func circleViewForImage(custom: UIView){
        
        custom.clipsToBounds = false
        custom.layer.borderWidth = 1
        custom.layer.shadowOffset = CGSize(width: 2, height: 4)
        custom.layer.shadowColor = colors.gradColor.cgColor
        custom.layer.shadowOpacity = 0.5
        custom.layer.borderColor = colors.appColor.cgColor
        custom.layer.cornerRadius = custom.layer.frame.size.width / 2
        custom.layer.shadowRadius = custom.layer.frame.size.width / 2
        custom.layer.shadowPath = UIBezierPath(roundedRect: custom.bounds, cornerRadius: custom.layer.frame.size.width / 2).cgPath
    }
    func playAnimation(vc: UIViewController) -> LOTAnimationView{
        let Lot: LOTAnimationView = LOTAnimationView(name: "Boat_Loader")
        Lot.alpha = 1
        vc.view.addSubview(Lot)
        Lot.frame = CGRect(x:0, y: view.bounds.midX, width: vc.view.frame.width , height: 300)
        Lot.loopAnimation = true
        Lot.play()
        
        return Lot
    }
    
    func setNavigationTitle(Title: String){
        self.navigationItem.title = Title.localized
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor.white]
    }
    
    func stopAnimation(Lot: LOTAnimationView){
        Lot.alpha = 0
    }
    func cycleLogin(url: String, param: Parameters, isRemember: Bool, isUpdate: Bool, isNewDoc: Bool){
        
        var lott: LOTAnimationView = LOTAnimationView(name: "Boat_Loader")
        lott = playAnimation(vc: self)

        
        Alamofire.request(url, method: .post, parameters: param, encoding: JSONEncoding(options: []), headers: nil).responseJSON { (response) in
            switch response.result {
                
            case .success:
                if let value = response.result.value as? NSDictionary {
                    if value["state"] as? Int == 101{
                        if let arr = value["user"] as? NSArray{
                            for data in arr{
                                var id = 0
                                var enFName    = " "
                                var userName   = " "
                                var enLname    = " "
                                var arFName    = " "
                                var arLname    = " "
                                var email      = " "
                                var phone      = " "
                                var gender     = " "
                                var birth_day  = " "
                                var country_id = 0
                                var image      = " "
                                var specilizationID = 0
                                
                                if let object = data as? Dictionary <String, Any>{
                                    if let value = object["id"] as? String{
                                        id = Int(value)!
                                    }
                                    if let value = object["id"] as? Int{
                                        id = value
                                    }
                                    if let value = object["username"] as? String{
                                        userName = value
                                    }
                                    if let value = object["fname_en"] as? String{
                                        enFName = value
                                    }
                                    if let value = object["lname_en"] as? String{
                                        enLname = value
                                    }
                                    if let value = object["fname_ar"] as? String{
                                        arFName = value
                                    }
                                    if let value = object["lname_ar"] as? String{
                                        arLname = value
                                    }
                                    if let value = object["email"] as? String{
                                        email = value
                                    }
                                    if let value = object["phone"] as? String{
                                        phone = value
                                    }
                                    if let value = object["gender"] as? String{
                                        gender = value
                                    }
                                    if let value = object["birth_day"] as? String{
                                        birth_day = value
                                    }
                                    if let value = object["country_id"] as? Int{
                                        country_id = value
                                    }
                                    if let value = object["image"] as? String{
                                        image = value
                                    }
                                    if let value = object["specialization_id"] as? Int{
                                        specilizationID = value
                                    }
                                }
                                UserDefaults.standard.set(isRemember, forKey: "isRemember")
                                self.LeadData(id: id, userName: userName, email: email, phone: phone, gender: gender, birthDate: birth_day, CountryID: country_id, image: image, enFname: enFName, arFname: arFName, enLname: enLname, arLname: arLname, isNewDoc: isNewDoc, SpecilizationID: specilizationID)
                            }
                            
                        }
                        if isUpdate{
                            self.dismiss(animated: true, completion: nil)
                        }
                    }
                    else{
                        if let status = value["state"] as? Int{
                            self.otherStatus(Number: status)
                        }
                    }
                    self.stopAnimation(Lot: lott)
                }
            case .failure:
                self.errorInConnection()
                self.stopAnimation(Lot: lott)

            }
        }
    }
    
    func dashedView(dashView: UIView){
        var yourViewBorder = CAShapeLayer()
        yourViewBorder.strokeColor = UIColor.black.cgColor
        yourViewBorder.lineDashPattern = [2, 2]
        yourViewBorder.frame = dashView.bounds
        yourViewBorder.fillColor = nil
        yourViewBorder.path = UIBezierPath(rect: dashView.bounds).cgPath
        dashView.layer.addSublayer(yourViewBorder)
    }
    
    func addNewInDoctor(param: Parameters){
        var lott: LOTAnimationView = LOTAnimationView(name: "Boat_Loader")
        lott = playAnimation(vc: self)
        
        Alamofire.request(urls.filterInsurance, method: .post, parameters: param, encoding: JSONEncoding(options: []), headers: nil).responseJSON { (response) in
            switch response.result {
                
            case .success:
                if let value = response.result.value as? NSDictionary {
                    if value["state"] as? Int == 101{
                        self.navigationController?.popViewController(animated: true)
                    }else{
                        if let status = value["state"] as? Int{
                            self.otherStatus(Number: status)
                        }
                    }
                    self.stopAnimation(Lot: lott)
                }
            case .failure:
                self.errorInConnection()
                self.stopAnimation(Lot: lott)
            }
        }
    }
    
    func shareApp(){
        let text = "https://itunes.apple.com/us/app/healthy-plus/id1434561250?ls=1&mt=8"
        let textShare = [ text ]
        let activityViewController = UIActivityViewController(activityItems: textShare , applicationActivities: nil)
        activityViewController.popoverPresentationController?.sourceView = self.view
        self.present(activityViewController, animated: true, completion: nil)
    }
    
    func pushBarButtonLeft(){
        if sharedHandler.isLogged(){
            let barButton = UIBarButtonItem(image: UIImage(named: "msg"), landscapeImagePhone: nil, style: .done, target: self, action: #selector(revealBackClicked))
            self.navigationItem.leftBarButtonItem = barButton
            self.navigationItem.hidesBackButton = true
        }
    }
    
    @objc func revealBackClicked() {
        let vc = UIStoryboard(name: "doctor", bundle: nil).instantiateViewController(withIdentifier: "messagesVC") as! messagesVC
        vc.method = "chatters"
        vc.isDoctor = false
        vc.url = urls.healthyCarURL
        vc.parameterID = "patient_id"
        navigationController?.pushViewController(vc, animated: true)
    }
}
