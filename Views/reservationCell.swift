//
//  reservationCell.swift
//  HealthyPlus
//
//  Created by apple on 7/15/18.
//  Copyright © 2018 Kirollos. All rights reserved.
//

import UIKit

class reservationCell: UITableViewCell {

    @IBOutlet weak var reservationNameOrDate: UILabel!
    @IBOutlet weak var MainTitle: UILabel!
    @IBOutlet weak var subTitle: UILabel!
    @IBOutlet weak var address: UILabel!
    
    @IBOutlet weak var callBtn: UIButton!
    @IBOutlet weak var mapBtn: UIButton!
    @IBOutlet weak var cancelBtn: UIButton!
    @IBOutlet weak var custView: UIView!
    @IBOutlet weak var cust: UIView!
    
    @IBOutlet weak var resImage: UIImageView!
    
    
    func settingData(NameOrDate: String, title: String, subtitle: String, Address: String, Url: String){
        reservationNameOrDate.text = NameOrDate
        MainTitle.text = title
        subTitle.text = subtitle
        address.text = Address
        setImage(url: Url)
    }
    func setImage(url: String) {
        DispatchQueue.global().async {
            self.resImage.sd_setImage(with: URL(string: url))
        }
    }
    override func awakeFromNib() {
        custView.layer.cornerRadius = 8
    }
}
