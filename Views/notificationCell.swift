//
//  notificationCell.swift
//  HealthyPlus
//
//  Created by apple on 7/16/18.
//  Copyright © 2018 Kirollos. All rights reserved.
//

import UIKit

class notificationCell: UITableViewCell {

    @IBOutlet weak var imageNotification: UIImageView!
    @IBOutlet weak var dat: UILabel!
    @IBOutlet weak var msg: UILabel!
    @IBOutlet weak var shadowView: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        shadowView.layer.cornerRadius = 8
        shadowView.layer.borderWidth = 0.5
        shadowView.layer.shadowOffset = CGSize(width: 1, height: 2   )
        shadowView.layer.shadowColor = UIColor.gray.cgColor
        shadowView.layer.shadowOpacity = 0.5
        shadowView.layer.shadowRadius = 4
        shadowView.layer.borderColor = UIColor.lightGray.cgColor
        
    }

    func  settingData(date: String, message: String, Image: String) {
        dat.text = date
        msg.text = message
        setImage(url: Image)
    }
    func setImage(url: String) {
        DispatchQueue.global().async {
            self.imageNotification.sd_setImage(with: URL(string: url))
        }
    }
}
