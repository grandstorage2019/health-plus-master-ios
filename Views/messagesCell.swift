//
//  messagesCell.swift
//  HealthyPlus
//
//  Created by apple on 8/16/18.
//  Copyright © 2018 Kirollos. All rights reserved.
//

import UIKit

class messagesCell: UITableViewCell {

    @IBOutlet weak var personImage: UIImageView!
    @IBOutlet weak var personName: UILabel!
    @IBOutlet weak var message: UILabel!
    @IBOutlet weak var messageView: UIView!
    
    func settingData(imageURL: String, Name: String, msg: String){
        personName.text = Name
        message.text = msg
        if imageURL != " "{
            setImage(url: imageURL)
        }
    }
    func setImage(url: String) {
        DispatchQueue.global().async {
            self.personImage.sd_setImage(with: URL(string: url))
        }
    }
}
