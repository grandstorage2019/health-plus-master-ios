//
//  rateCell.swift
//  HealthyPlus
//
//  Created by Donna on 7/20/18.
//  Copyright © 2018 Kirollos. All rights reserved.
//

import UIKit

class rateCell: UITableViewCell {

    @IBOutlet weak var cellView: UIView!
    
    @IBOutlet weak var NameLbl: UILabel!
    @IBOutlet weak var comment: UILabel!
    @IBOutlet weak var fav1: UIButton!
    @IBOutlet weak var fav2: UIButton!
    @IBOutlet weak var fav3: UIButton!
    @IBOutlet weak var fav4: UIButton!
    @IBOutlet weak var fav5: UIButton!
    
    func SettingData(name: String, commenttxt: String){
        NameLbl.text = name
        comment.text = commenttxt
    }
    
}
