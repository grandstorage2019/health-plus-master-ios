//
//  profileCell.swift
//  HealthyPlus
//
//  Created by apple on 8/5/18.
//  Copyright © 2018 Kirollos. All rights reserved.
//

import UIKit

class profileCell: UITableViewCell {

    @IBOutlet weak var cellView: UIView!
    @IBOutlet weak var cellImage: UIImageView!
    @IBOutlet weak var lblName: UILabel!
    
}
