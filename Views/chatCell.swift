//
//  chatCell.swift
//  HealthyPlus
//
//  Created by apple on 8/16/18.
//  Copyright © 2018 Kirollos. All rights reserved.
//

import UIKit

class chatCell: UITableViewCell {

    @IBOutlet weak var personImage: UIImageView!
    @IBOutlet weak var message: UILabel!
    @IBOutlet weak var chatView: UIView!
    
}
