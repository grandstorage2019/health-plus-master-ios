//
//  PatientCell.swift
//  HealthyPlus
//
//  Created by apple on 8/8/18.
//  Copyright © 2018 Kirollos. All rights reserved.
//

import UIKit

class PatientCell: UITableViewCell {

    @IBOutlet weak var PatientNameLbl: UILabel!
    @IBOutlet weak var patientIDLbl: UILabel!
    @IBOutlet weak var patientMobileLbl: UILabel!
    @IBOutlet weak var PatientGenderLbl: UILabel!
    
    @IBOutlet weak var patientView: UIView!
    
    func settingData(name: String, id: String, mobile: String, gender: String){
        
        PatientNameLbl.text = name
        patientIDLbl.text = id
        patientMobileLbl.text = mobile
        PatientGenderLbl.text = gender
        
    }
}
