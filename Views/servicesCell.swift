
//
//   servicesCell.swift
//  HealthyPlus
//
//  Created by apple on 7/18/18.
//  Copyright © 2018 Kirollos. All rights reserved.
//

import UIKit

class servicesCell: UICollectionViewCell {
    
    @IBOutlet weak var servicesName: UILabel!
    @IBOutlet weak var custView: UIView!
    
    func designing(name: String){
        self.servicesName.text = name
        custView.layer.cornerRadius = 10
    }
}
