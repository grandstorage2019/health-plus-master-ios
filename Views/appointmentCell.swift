//
//  appointmentCell.swift
//  HealthyPlusDate
//
//  Created by apple on 8/12/18.
//  Copyright © 2018 Kirollos. All ri8ghts8 reserved.
//

import UIKit

class appointmentCell: UITableViewCell {

    @IBOutlet weak var dayNameLbl: UILabel!
    @IBOutlet weak var numOfReservation: UILabel!
    @IBOutlet weak var dateLbl: UILabel!
    @IBOutlet weak var appointmentView: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    func setData(day: String, num: Int, date: String){
        dayNameLbl.text = day
        dateLbl.text = date
        
        if num == 0{
            numOfReservation.text = "No Reservation".localized
        }else{
            numOfReservation.text = "\(num) " + "Reservataions".localized
        }
    }
}
