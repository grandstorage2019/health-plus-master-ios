//
//  headerTableViewCell.swift
//  HealthyPlus
//
//  Created by apple on 8/13/18.
//  Copyright © 2018 Kirollos. All rights reserved.
//

import UIKit
import ExpyTableView

class headerTableViewCell: UITableViewCell,ExpyTableViewDelegate {

    @IBOutlet weak var dayName: UILabel!
    @IBOutlet weak var isSwitch: UISwitch!
    

    func changeState(_ state: ExpyState, cellReuseStatus cellReuse: Bool) {
        
        switch state {
        case .willExpand:
            print("WILL EXPAND")
            
        case .willCollapse:
            print("WILL COLLAPSE")
            
        case .didExpand:
            print("DID EXPAND")
            
        case .didCollapse:
            print("DID COLLAPSE")
        }
    }
}


extension UITableViewCell {
    
    func showSeparator() {
        DispatchQueue.main.async {
            self.separatorInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        }
    }
    
    func hideSeparator() {
        DispatchQueue.main.async {
            self.separatorInset = UIEdgeInsets(top: 0, left: self.bounds.size.width, bottom: 0, right: 0)
        }
    }
}

