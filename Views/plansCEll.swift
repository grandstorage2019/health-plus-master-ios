//
//  plansCEll.swift
//  HealthyPlus
//
//  Created by apple on 8/16/18.
//  Copyright © 2018 Kirollos. All rights reserved.
//

import UIKit

class plansCEll: UITableViewCell {

    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var details: UILabel!
    @IBOutlet weak var gradView: UIView!
    
    func settingData(Name: String, Details: String){
        self.name.text = Name
        self.details.text = Details
    }
}
