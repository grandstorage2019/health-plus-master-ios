//
//  imagesCell.swift
//  HealthyPlus
//
//  Created by Donna on 7/20/18.
//  Copyright © 2018 Kirollos. All rights reserved.
//

import UIKit

class imagesCell: UICollectionViewCell {
    
    @IBOutlet weak var picture: UIImageView!
    
    func setImage(url: String) {
        DispatchQueue.global().async {
            self.picture.sd_setImage(with: URL(string: url))
        }
    }
}
