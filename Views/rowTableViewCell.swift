//
//  rowTableViewCell.swift
//  HealthyPlus
//
//  Created by apple on 8/13/18.
//  Copyright © 2018 Kirollos. All rights reserved.
//

import UIKit
import ExpyTableView

class rowTableViewCell: UITableViewCell,ExpyTableViewDelegate {
    
    @IBOutlet weak var fromDate: UITextField!
    @IBOutlet weak var toDate: UITextField!
    @IBOutlet weak var reservationType: UITextField!
    @IBOutlet weak var waitingTime: UITextField!
    
    func settingData(from: String, to: String, type: String, waiting: String){
        if from != " "{
            fromDate.text = from
        }
        if to != " "{
            toDate.text = to
        }
        if type != " "{
            reservationType.text = type
        }
        if waiting != " "{
            waitingTime.text = waiting
        }
    }
    func changeState(_ state: ExpyState, cellReuseStatus cellReuse: Bool) {
        
        switch state {
        case .willExpand:
            print("WILL EXPAND")
            
        case .willCollapse:
            print("WILL COLLAPSE")
            
        case .didExpand:
            print("DID EXPAND")
            
        case .didCollapse:
            print("DID COLLAPSE")
        }
    }
}

