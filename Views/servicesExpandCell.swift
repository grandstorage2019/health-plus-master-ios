//
//  servicesExpandCell.swift
//  HealthyPlus
//
//  Created by apple on 8/14/18.
//  Copyright © 2018 Kirollos. All rights reserved.
//

import UIKit

class servicesExpandCell: UITableViewCell {

    @IBOutlet weak var addBtn: UIButton!
    @IBOutlet weak var moneyLbl: UILabel!
    @IBOutlet weak var hourLbl: UILabel!
    @IBOutlet weak var nameLbl: UILabel!
    @IBOutlet weak var imageServices: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
