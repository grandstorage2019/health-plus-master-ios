//
//  favouriteCell.swift
//  HealthyPlus
//
//  Created by apple on 7/17/18.
//  Copyright © 2018 Kirollos. All rights reserved.
//

import UIKit

class careFavouriteCell: UITableViewCell {

    @IBOutlet weak var namelbl: UILabel!
    @IBOutlet weak var viewsLbl: UILabel!
    @IBOutlet weak var locationLbl: UILabel!
    @IBOutlet weak var phone: UILabel!
    @IBOutlet weak var datelbl: UILabel!
    @IBOutlet weak var descriptionlbl: UILabel!
    @IBOutlet weak var favouriteBtm: UIButton!
    @IBOutlet weak var ImageFavourite: UIImageView!
    @IBOutlet weak var cust: UIView!
    
    @IBOutlet weak var fav1: UIButton!
    @IBOutlet weak var fav2: UIButton!
    @IBOutlet weak var fav3: UIButton!
    @IBOutlet weak var fav4: UIButton!
    @IBOutlet weak var fav5: UIButton!
    
    func settingData(Name: String, views: String, location: String, Phone: String, date: String, des: String, imageURLL: String, isFav: Bool){
        namelbl.text = Name
        viewsLbl.text = "(\(views) " + "view".localized + " )"
        locationLbl.text = location
        phone.text = Phone
        datelbl.text = date
        descriptionlbl.text = des
        setImage(url: imageURLL)
        if isFav ==  true{
            favouriteBtm.setImage(#imageLiteral(resourceName: "heartSelection"), for: UIControlState.normal)
        }
    }
    func setImage(url: String) {
        DispatchQueue.global().async {
            self.ImageFavourite.sd_setImage(with: URL(string: url))
        }
    }
}
