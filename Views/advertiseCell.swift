//
//  advertiseCell.swift
//  HealthyPlus
//
//  Created by apple on 7/15/18.
//  Copyright © 2018 Kirollos. All rights reserved.
//

import UIKit
import SDWebImage
class advertiseCell: UICollectionViewCell {
    
    @IBOutlet weak var adImage: UIImageView!
    @IBOutlet weak var adName: UILabel!
    @IBOutlet weak var adShow: UIButton!
    
    func settingData(URL: String, adNamee: String){
        setImage(url: URL)
        adName.text = adNamee
    }
    func setImage(url: String) {
        DispatchQueue.global().async {
            self.adImage.sd_setImage(with: URL(string: url))
        }
    }
    override func awakeFromNib() {
        
        self.contentView.layer.cornerRadius = 2.0
        self.contentView.layer.borderWidth = 1.0
        self.contentView.layer.borderColor = UIColor.clear.cgColor
        self.contentView.layer.masksToBounds = true
        
        self.layer.shadowColor = UIColor.black.cgColor
        self.layer.shadowOffset = CGSize(width: 0, height: 2.0)
        self.layer.shadowRadius = 2.0
        self.layer.shadowOpacity = 0.5
        self.layer.masksToBounds = false
        self.layer.shadowPath = UIBezierPath(roundedRect: self.bounds, cornerRadius: self.contentView.layer.cornerRadius).cgPath
        
        self.layer.backgroundColor = UIColor.white.cgColor
    }
}
