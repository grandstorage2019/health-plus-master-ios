//
//  timesCell.swift
//  HealthyPlus
//
//  Created by apple on 7/18/18.
//  Copyright © 2018 Kirollos. All rights reserved.
//

import UIKit

class bookCell: UICollectionViewCell {
    
    @IBOutlet weak var day: UILabel!
    @IBOutlet weak var date: UILabel!
    @IBOutlet weak var from: UILabel!
    @IBOutlet weak var to: UILabel!
    
    func settingdata(Day: String, Date: String, From: String, To: String){
        day.text = Day
        date.text = Date
        from.text = From
        to.text = To
    }
    override func awakeFromNib() {
        self.layer.cornerRadius = 8
        self.layoutSubviews()
    }
}
