//
//  personCell.swift
//  HealthyPlus
//
//  Created by apple on 10/18/18.
//  Copyright © 2018 Kirollos. All rights reserved.
//

import UIKit

class personCell: UITableViewCell {

    var delegate: MoreVC?
    @IBOutlet weak var personImage: UIImageView!
    @IBOutlet weak var personName: UILabel!
    @IBOutlet weak var personID: UILabel!

    @IBAction func changeImageAction(_ sender: Any) {
        delegate?.changeImage()
    }

}
class moreCell: UITableViewCell {
    
    @IBOutlet weak var moreTxt: UILabel!
}
