//
//  healthyFavouriteCell.swift
//  HealthyPlus
//
//  Created by apple on 7/17/18.
//  Copyright © 2018 Kirollos. All rights reserved.
//

import UIKit

class healthyFavouriteCell: UITableViewCell {

    @IBOutlet weak var healthyInage: UIImageView!
    @IBOutlet weak var fullNameLbl: UILabel!
    @IBOutlet weak var clinicLbl: UILabel!
    @IBOutlet weak var ViewsLbl: UILabel!
    @IBOutlet weak var aboutLbl: UILabel!
    @IBOutlet weak var priceLbl: UILabel!
    @IBOutlet weak var waitingTime: UILabel!
    @IBOutlet weak var Loction: UILabel!
    @IBOutlet weak var availableFrom: UILabel!
    @IBOutlet weak var cust: UIView!
    
    @IBOutlet weak var fav1: UIButton!
    @IBOutlet weak var fav2: UIButton!
    @IBOutlet weak var fav3: UIButton!
    @IBOutlet weak var fav4: UIButton!
    @IBOutlet weak var fav5: UIButton!
    
    @IBOutlet weak var favouriteBtn: UIButton!
    @IBOutlet weak var bookNow: UIButton!
    @IBOutlet weak var btnBookView: UIButton!
    @IBOutlet weak var specilization: UILabel!
    
    func settingData(image: String, fullName: String, Clinic: String, Views: String, About: String, Price: String, Waiting: String, Location: String, available: String, isFav: Bool, specialization: String){
        setImage(url: image)
        fullNameLbl.text = fullName
        clinicLbl.text = Clinic
        ViewsLbl.text = "( \(Views) " + "People visited DR.".localized + " )"
        aboutLbl.text = About
        priceLbl.text = "Detection Price".localized + ": \(Price)"
        specilization.text = specialization
        if Waiting == "Book First".localized{
            waitingTime.text = "\(Waiting)"
        }else{
            waitingTime.text = "wait".localized + ": \(Waiting)"
        }
        Loction.text = Location
        availableFrom.text = available
        if isFav ==  true{
            favouriteBtn.setImage(#imageLiteral(resourceName: "heartSelection"), for: UIControlState.normal)
        }
    }
    func setImage(url: String) {
        DispatchQueue.global().async {
            self.healthyInage.sd_setImage(with: URL(string: url))
        }
    }
    override func awakeFromNib() {
        btnBookView.layer.cornerRadius = 10
        self.contentView.layer.masksToBounds = true
    }
}
