//
//  patientReservationCell.swift
//  HealthyPlus
//
//  Created by apple on 8/13/18.
//  Copyright © 2018 Kirollos. All rights reserved.
//

import UIKit

class patientReservationCell: UITableViewCell {

    @IBOutlet weak var reservationView: UIView!
    @IBOutlet weak var patientNameLbl: UILabel!
    @IBOutlet weak var patientIDLbl: UILabel!
    @IBOutlet weak var mobileLbl: UILabel!
    @IBOutlet weak var genderLbl: UILabel!
    @IBOutlet weak var timeLbl: UILabel!
    @IBOutlet weak var dateLbl: UILabel!
    @IBOutlet weak var addPatientLbl: UIButton!
    
    func settingData(patientName: String, patientID: String, mobile: String, gender: String, time: String, date: String, btnTag: Int){
        
        patientNameLbl.text = patientName
        patientIDLbl.text   = patientID
        mobileLbl.text      = mobile
        timeLbl.text        = time
        dateLbl.text        = date
        addPatientLbl.tag   = btnTag
        genderLbl.text      = gender
        
        if Int(patientID) == sharedHandler.getUserID(){
            addPatientLbl.alpha = 0
            
        }
    }
}
